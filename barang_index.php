<?php 
  require('layouts/header.php');
  require('config/connection.php');

  if (!isset($_SESSION['date'])) {
    $_SESSION['notif'] = "date-null"; 
    header('location:index.php'); die;
  }

  $id = $_GET['id'];
  $data = mysqli_query($conn, "SELECT * FROM barang WHERE id_barang='$id'");

  $barang = [];
  while ($x = mysqli_fetch_assoc($data)) {
    $barang[] = $x;
  }

  // jika keranjang belum di init
  if (!isset($_SESSION['keranjang_belanja'])) {
    $_SESSION['keranjang_belanja'] = [];
    $_SESSION['id_barang'] = [];
  }

  $sql_merek = mysqli_query($conn, "SELECT COUNT(id_barang), merek FROM barang GROUP BY merek");
  $mereks = [];
  while ($merek = mysqli_fetch_assoc($sql_merek)) {
    $mereks[] = $merek;
  }

?>

  <div class="jumbotron title-barang-detail" style="background:url('images/background.jpg') no-repeat;background-size:cover;background-position:center;">
    <div class="container text-center text-white">
      <h2><?= $barang[0]['nama_barang'] ?></h2>
    </div>
  </div>

  <div class="container">

    <div class="row">
      <!-- awal kategori barang -->
    <div class="col-md-3 mb-4">
      <div class="list-group">
        <?php foreach ($mereks as $m) : ?>
        <a href="./?merek=<?= $m['merek'] ?>" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
          <?= $m['merek'] ?>
          <span class="badge badge-primary badge-pill"><?= $m['COUNT(id_barang)'] ?></span>
        </a>
        <?php endforeach; ?>
      </div>
    </div>
    <!-- akhir kategori barang -->
      <!-- awal detail barang -->
      <div class="col-md-9">
        <div class="row">
          <!-- awal gambar barang -->
          <div class="col-md-6">
            <?php 
              if (isset($_SESSION['notif'])) {
                if ($_SESSION['notif'] == 'barang-tambah'){
                  echo '
                    <div class="alert alert-warning alert-dismissible fade show text-left" role="alert">
                      <strong>Barang berhasil ditambah!</strong>
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>';
                }
                if ($_SESSION['notif'] == 'barang-ada'){
                  echo '
                    <div class="alert alert-danger alert-dismissible fade show text-left" role="alert">
                      <strong>Barang sudah ada di keranjang!</strong>
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>';
                }
                unset($_SESSION['notif']);
              }
            ?>
            <div class="card card-detail-barang mb-3">
              <div class="text-center">
                <img class="center-cropped" src="images/<?= $barang[0]['foto_barang'] ?>" class="card-img-top" alt="<?= $barang[0]['nama_barang'] ?>">
              </div>
            </div>
          </div>
          <!-- akhir gambar barang -->
          <!-- awal detail -->
          <div class="col-md-6">
            <div class="card card-detail-barang px-3 mb-5">
              <div class="row">
                <div class="col-6">
                  <h5>STOK TERSISA</h5>
                  <h6 class="text-muted"><?= $barang[0]['stok_awal']-$barang[0]['jmlh_rusak']-$barang[0]['jmlh_diambil'] ?></h6>
                </div>
                <div class="col-6 text-right">
                  <h4 class="card-title">Rp. <?= $barang[0]['harga_sewa'] ?></h4>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col text-justify"><?= $barang[0]['deskripsi'] ?></div>
              </div>
              <hr>
              <form action="keranjang_tambah.php" method="post">
                <div class="row">
                  <div class="col-12">
                    <table width="100%" cellpadding="10">
                      <input type="hidden" name="id_barang" class="form-control" value="<?= $barang[0]['id_barang'] ?>">
                      <tr>
                        <th class="text-right">Jumlah</th>
                        <td width="60%"><input type="number" name="jmlh_sewa" id="jmlh_sewa" min="1" max="<?= $barang[0]['stok_awal']-$barang[0]['jmlh_rusak']-$barang[0]['jmlh_diambil'] ?>" class="form-control" required></td>
                      </tr>
                    </table>
                  </div>
                </div>
                <div class="row mt-4">
                  <div class="col-12 text-right">
                    <button type="submit" class="btn btn-primary" style="width:100%;">Sewa</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <!-- akhir detail -->
        </div>

        <?php 
          $queryReview = mysqli_query($conn, "SELECT r.rate, r.komentar, m.nama FROM review r INNER JOIN detail_peminjaman d ON r.detail_peminjaman_id=d.id INNER JOIN peminjaman p ON d.id_peminjaman=p.id_peminjaman INNER JOIN member m ON p.id_member=m.id_member WHERE d.id_barang='$id'");
          $reviews = [];
          while ($rvw = mysqli_fetch_assoc($queryReview)) {
            $reviews[] = $rvw;
          }

          $queryAvgRate = mysqli_query($conn, "SELECT AVG(r.rate) AS avg_rate, COUNT(*) AS jml FROM review r INNER JOIN detail_peminjaman d ON r.detail_peminjaman_id=d.id WHERE d.id_barang='$id'");
          $avgRate = mysqli_fetch_assoc($queryAvgRate);
        ?>

        <div class="row">
          <div class="col-12 mb-5">
            <div class="card py-3">
              <div class="container">
                <h4 style="display: inline;">Testimoni</h4> &nbsp;
                <?php $ksng = 5 - number_format($avgRate['avg_rate']) ?>
                <?php for ($c = 0; $c < number_format($avgRate['avg_rate']); $c++) : ?>
                  <i class="fas fa-star text-warning" style="font-size: 1.5rem;"></i>
                <?php endfor ?>
                <?php for ($d = 0; $d < $ksng; $d++) : ?>
                  <i class="far fa-star text-warning" style="font-size: 1.5rem;"></i>
                <?php endfor ?>
                <p class="mt-2"><?= number_format($avgRate['avg_rate']) ?> Bintang rata-rata dari <?= $avgRate['jml'] ?> User</p>
                
                <div class="row mt-4">
                  <?php foreach ($reviews as $rw) : ?>
                    <div class="col-12">
                      <hr>
                      <h6><?= $rw['nama'] ?></h6>
                      <?php $kosong = 5 - $rw['rate'] ?>
                      <?php for ($a = 0; $a < $rw['rate']; $a++) : ?>
                        <i class="fas fa-star text-warning" style="font-size: 0.8rem;"></i>
                      <?php endfor ?>
                      <?php for ($b = 0; $b < $kosong; $b++) : ?>
                        <i class="far fa-star text-warning" style="font-size: 0.8rem;"></i>
                      <?php endfor ?>
                      <p style="font-size: 0.9rem;" class="text-muted mt-2"><?= $rw['komentar'] ?></p>
                    </div>
                  <?php endforeach ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- akhir detail barang -->

    </div>
  </div>

<?php require('layouts/footer.php') ?>