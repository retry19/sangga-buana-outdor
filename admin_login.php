<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Sangga Buana Outdor</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<body id="body" style="background:url('images/background.jpg') no-repeat;background-size:cover;">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-5 mb-5">
        <div class="card align-middle login-page mb-5">
          <div class="card-body text-center">
            <h5 class="card-title">LOGIN ADMIN</h5>
            <a href="./index.php"><h6 class="text-muted">Sangga Buana Outdor</h6></a>
            <hr class="mb-4">

            <?php 
              if (isset($_GET['notif'])) { 
                if ($_GET['notif'] == 'error-account'){
                  echo '
                    <div class="alert alert-danger alert-dismissible fade show mx-3 text-left" role="alert">
                      <strong>Login gagal!</strong> email atau password salah!
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>';
                }
                if ($_GET['notif'] == 'logout'){
                  echo '
                    <div class="alert alert-success alert-dismissible fade show mx-3 text-left" role="alert">
                      <strong>Logout berhasil!</strong> anda telah keluar dari akun!
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>';
                }
                if ($_GET['notif'] == 'error-login'){
                  echo '
                    <div class="alert alert-danger alert-dismissible fade show mx-3 text-left" role="alert">
                      <strong>Anda harus login!</strong> silahkan login terlebih dahulu untuk melanjutkan!
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>';
                }
              }
            ?>

            <form method="POST" class="text-left px-4 mb-4" action="admin_login_submit.php">
              <div class="form-group">
                <label for="username">Username</label>
                <input type="text" name="username" class="form-control" id="username" placeholder="Masukan username" required>
              </div>
              <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
              </div>
              <br>
              <div class="row">
                <div class="col-md-6">
                  <button type="reset" class="btn btn-outline-secondary" style="width:100%;">Batal</button>
                </div>
                <div class="col-md-6">
                  <button type="submit" class="btn btn-primary" style="width:100%;">Login</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
 
  <!-- awal footer -->
  <div class="jumbotron footer mt-4 mb-0">
    <div class="container text-center">
      <p>2019 &copy; Sangga Buana Outdor</p>
    </div>
  </div>
  <!-- akhir footer -->

  <script src="js/jquery.slim.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</body>
</html>