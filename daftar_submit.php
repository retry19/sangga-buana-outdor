<?php
  require('config/connection.php');
  extract($_POST);

  // cek apakah email sudah terdaftar
  $cek_email = mysqli_query($conn, "SELECT * FROM member WHERE email='$email'");
  if (mysqli_num_rows($cek_email)) {
    header('location:daftar_index.php?notif=email-error');die;
  }

  // untuk file upload
  $ekstensi = ['png', 'jpeg', 'jpg', 'bmp'];
  $nama_file = $_FILES['foto_identitas']['name'];
  $x = explode('.', $nama_file);
  $ekstensi_file = strtolower(end($x));
  $ukuran_file = $_FILES['foto_identitas']['size'];
  $temp_file = $_FILES['foto_identitas']['tmp_name'];

  $namaFile = $nama.'_'.$nama_file;

  if (in_array($ekstensi_file, $ekstensi) === true ) {
    if ($ukuran_file <= 5000000) {
      move_uploaded_file($temp_file, 'file/foto-identitas/'.$namaFile);
      $sql = mysqli_query($conn, "INSERT INTO member values('','$nama','$namaFile','$status','$no_hp','$email','$alamat','$password')");
      
      if (!$sql) {
        header('location:daftar_index.php?notif=error');
      }
      header('location:daftar_index.php?notif=success');
    
    } else {
      header('location:daftar_index.php?notif=size-file');
    }
  } else {
    header('location:daftar_index.php?notif=ekstensi-file');
  }
?>