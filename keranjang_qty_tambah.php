<?php 
  session_start();

  $id_barang = $_GET['id'];
  $max_qty = $_GET['max'];

  foreach ($_SESSION['keranjang_belanja'] as $key => $value) {
    if ($_SESSION['keranjang_belanja'][$key]['id_barang'] == $id_barang) {
      if ($_SESSION['keranjang_belanja'][$key]['qty'] != $max_qty) {
        $_SESSION['keranjang_belanja'][$key]['qty']++; 
      } else {
        $_SESSION['notif'] = 'qty-max';
        header('location:keranjang_index.php');
      }
    }

    header('location:keranjang_index.php');
  }
?>