<?php 
  session_start();

  // apakah sudah login
  if (!isset($_SESSION['id_member'])) {
    header('location:login_index.php?notif=error-login');die;
  }

  require('config/connection.php');

  $id_barang = $_POST['id_barang'];
  $jmlh_sewa = $_POST['jmlh_sewa'];

  $keranjang_belanja = $_SESSION['keranjang_belanja'];
  $ket_barang = [
    'id_barang' => $id_barang,
    'qty' => $jmlh_sewa,
  ];

  // apakah produk sudah di keranjang
  if (!in_array($id_barang, $_SESSION['id_barang'])) {
    array_push($_SESSION['keranjang_belanja'], $ket_barang);
    array_push($_SESSION['id_barang'], $id_barang);
    $_SESSION['notif'] = 'barang-tambah';
  } else {
    $_SESSION['notif'] = 'barang-ada';
  }

  header('location:barang_index.php?id='.$id_barang);
?>