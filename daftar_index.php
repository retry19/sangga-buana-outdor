<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Sangga Buana Outdor</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<body id="body" style="background:url('images/background.jpg') no-repeat;background-size:cover;">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-5 mb-5">
        <div class="card align-middle daftar-page mb-5">
          <div class="card-body text-center">
            <h5 class="card-title">DAFTAR</h5>
            <a href="./index.php"><h6 class="text-muted">Sangga Buana Outdor</h6></a>
            <hr class="mb-4">
            
            <?php 
              if (isset($_GET['notif'])) { 
                switch ($_GET['notif']) {
                  case 'success':
                    echo '
                      <div class="alert alert-warning alert-dismissible fade show mx-3 text-left" role="alert">
                        <strong>Pendaftaran berhasil!</strong> silahkan login untuk melanjutkan penyewaan alat.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>';
                    break;
                  case 'error':
                    echo '
                      <div class="alert alert-danger alert-dismissible fade show mx-3 text-left" role="alert">
                        <strong>Pendaftaran gagal!</strong> silahkan ulangi proses pendaftaran.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>';
                    break;
                  case 'size-file':
                    echo '
                      <div class="alert alert-danger alert-dismissible fade show mx-3 text-left" role="alert">
                        <strong>Pendaftaran gagal!</strong> ukuran file foto identitas terlalu besar (Max. 5 MB).
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>';
                    break;
                  case 'ekstensi-file':
                    echo '
                      <div class="alert alert-danger alert-dismissible fade show mx-3 text-left" role="alert">
                        <strong>Pendaftaran gagal!</strong> ekstensi file foto identitas tidak sesuai (JPG, PNG, JPEG, BMP).
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>';
                    break;
                  case 'email-error':
                    echo '
                      <div class="alert alert-danger alert-dismissible fade show mx-3 text-left" role="alert">
                        <strong>Pendaftaran gagal!</strong> email sudah terdaftar.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>';
                    break;
                }
              }
            ?>

            <form method="POST" class="text-left px-4 mb-4" action="daftar_submit.php" enctype="multipart/form-data">
              <input type="hidden" name="status" value="member">
              <div class="form-group">
                <label for="nama">Nama Lengkap</label>
                <input type="text" name="nama" class="form-control" id="nama" placeholder="masukan nama lengkap anda" required>
              </div>
              <div class="form-group">
                <label for="email">Email</label>
                <input type="email" name="email" class="form-control" id="email" placeholder="nama@contoh.com" required>
              </div>
              <div class="form-group">
                <label for="no_hp">No. Hp</label>
                <input type="number" name="no_hp" class="form-control" id="no_hp" placeholder="089751XXXX" required>
              </div>
              <div class="form-group">
                <label for="alamat">Alamat</label>
                <textarea class="form-control" name="alamat" id="alamat" rows="3" required></textarea>
              </div>
              <div class="form-group">
                <label for="foto_identitas">Foto identitas (KTP)</label>
                <input type="file" name="foto_identitas" class="form-control-file" id="foto_identitas" required>
              </div>
              <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" class="form-control" id="password" placeholder="password" required>
              </div>
              <br>
              <div class="row">
                <div class="col-md-6">
                  <button type="reset" class="btn btn-outline-secondary" style="width:100%;">Batal</button>
                </div>
                <div class="col-md-6">
                  <button type="submit" class="btn btn-primary" style="width:100%;">Daftar</button>
                </div>
              </div>
              <div class="text-center mt-3">
                <span class="text-muted">Sudah memiliki akun? <a href="./login_index.php">klik disini!</a></span>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
 
  <!-- awal footer -->
  <div class="jumbotron footer mt-4 mb-0">
    <div class="container text-center">
      <p>2019 &copy; Sangga Buana Outdor</p>
    </div>
  </div>
  <!-- akhir footer -->

  <script src="js/jquery.slim.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</body>
</html>