<?php 
  require('../layouts/header_admin.php');
  require('../config/connection.php');
  
  $select_denda = mysqli_query($conn, "SELECT pb.id_pengembalian, pb.id_peminjaman, pb.denda_telat, pb.denda_rusak ,                                                m.nama
                                        FROM pengembalian pb
                                        INNER JOIN peminjaman p ON pb.id_peminjaman=p.id_peminjaman
                                        INNER JOIN member m ON p.id_member=m.id_member");
  $dendas = [];
  while ($denda = mysqli_fetch_array($select_denda)) {
    $dendas[] = $denda;
  }
?>

<!-- heading content -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Daftar Denda</h1>
</div>

<div class="row">
  <div class="col-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Pengembalian</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-hover" id="myTable">
            <thead>
              <tr>
                <th>Id Pengembalian</th>
                <th>Id Peminjaman</th>
                <th>Nama Member</th>
                <th>Denda Telat</th>
                <th>Denda Rusak</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($dendas as $d) : ?>
              <tr>
                <td><?= $d['id_pengembalian'] ?></td>
                <td><?= $d['id_peminjaman'] ?></td>
                <td><?= $d['nama'] ?></td>
                <td><?= $d['denda_telat'] ?></td>
                <td><?= $d['denda_rusak'] ?></td>
              </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require('../layouts/footer_admin.php') ?>