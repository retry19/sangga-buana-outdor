<?php 
  require('../layouts/header_admin.php');
  require('../config/connection.php');

  $i = 1;

  $select_member = mysqli_query($conn, "SELECT * FROM member");
  $members = [];
  while ($member = mysqli_fetch_array($select_member)) {
    $members[] = $member;
  }
?>

<!-- heading content -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Daftar Member</h1>
  <a href="./report/member.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Download Report</a>
</div>

<!-- awal tabel member -->
<div class="row">
  <div class="col-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Daftar Member</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="myTable">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Lengkap</th>
                <th>Email</th>
                <th>Alamat</th>
                <th>No. HP</th>
                <th>Foto Identitas</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($members as $u) : ?>
                <tr>
                  <td><?= $i++ ?></td>
                  <td><?= $u['nama'] ?></td>
                  <td><?= $u['email'] ?></td>
                  <td><?= $u['alamat'] ?></td>
                  <td><?= $u['no_hp'] ?></td>
                  <td class="text-center"><button type="button" class="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#fotoIdentitas<?=$u['id_member']?>">Lihat foto</button></td>
                </tr>

                <!-- modal foto identitas -->
                <div class="modal fade" id="fotoIdentitas<?=$u['id_member']?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><?= $u['nama'] ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <img class="img-thumbnail mx-auto d-block" src="../file/foto-identitas/<?= $u['foto_identitas'] ?>" width="70%">
                      </div>
                    </div>
                  </div>
                </div>
                <!-- modal foto identitas -->
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- akhir tabel member -->

<?php require('../layouts/footer_admin.php') ?>