<?php 
  require('../layouts/header_admin.php');
  require('../config/connection.php');

  $select_transaksi = mysqli_query($conn, "SELECT * FROM transaksi t INNER JOIN peminjaman p ON t.id_peminjaman=p.id_peminjaman 
                                            INNER JOIN member m ON p.id_member=m.id_member ORDER BY t.status_pembayaran ASC");
  $transaksis = [];
  while ($transaksi = mysqli_fetch_array($select_transaksi)) {
    $transaksis[] = $transaksi;
  }
?>

<!-- heading content -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Daftar Transaksi</h1>
</div>

<?php 
  if (isset($_SESSION['notif'])) {
    if ($_SESSION['notif']=='transaksi-valid') {
      echo '
      <div class="row"><div class="col-12">  
        <div class="alert alert-success alert-dismissible fade show mx-3 text-left" role="alert">
          <strong>Selamat!</strong> Pembayaran telah berhasil dilakukan.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div></div>';
    } 
    unset($_SESSION['notif']);
  }
?>

<div class="row">
  <div class="col-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Transaksi</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="table-keuangan">
            <thead>
              <tr>
                <th rowspan="2" class="align-middle">Id Transaksi</th>
                <th rowspan="2" class="align-middle">Id Peminjaman</th>
                <th rowspan="2" class="align-middle">Nama Member</th>
                <th rowspan="2" class="align-middle">Id User</th>
                <th rowspan="2" class="align-middle">Metode Pembayaran</th>
                <th colspan="2" class="align-middle text-center">Transfer</th>
                <th rowspan="2" class="align-middle">Bukti Pembayaran</th>
                <th rowspan="2" class="align-middle">Tanggal Pembayaran</th>
                <th rowspan="2" class="align-middle">Total Pembayaran</th>
                <th rowspan="2" class="align-middle">Total Harga</th>
                <th rowspan="2" class="align-middle">Status Pembayaran</th>
                <th rowspan="2" class="align-middle">Opsi</th>
              </tr>
              <tr>
                <th>Atas Nama</th>
                <th>Nama Bank</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($transaksis as $t) : ?>
                <tr>
                  <td class="align-middle"><?= $t['id_transaksi'] ?></td>
                  <td class="align-middle"><?= $t['id_peminjaman'] ?></td>
                  <td class="align-middle"><?= $t['nama'] ?></td>
                  <td class="align-middle"><?= $t['id_user'] ?></td>
                  <td class="align-middle"><?= $t['metode_pembayaran'] ?></td>
                  <td class="align-middle"><?= $t['transfer_an'] ?></td>
                  <td class="align-middle"><?= $t['transfer_bank'] ?></td>
                  <td class="align-middle">
                    <?php if ($t['metode_pembayaran'] == "Transfer") : ?>
                      <button type="button" class="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#bukti<?=$t['id_transaksi']?>">Lihat foto..</button>
                    <?php endif ?>
                  </td>
                  <td class="align-middle"><?= $t['tgl_pembayaran'] ?></td>
                  <td class="align-middle"><?= $t['total_pembayaran'] ?></td>
                  <td class="align-middle"><?= $t['total_harga'] ?></td>
                  <td class="align-middle">
                    <?php 
                      if ($t['status_pembayaran'] == 0) {
                        if ($t['metode_pembayaran'] == "Tunai") {
                          echo "Belum bayar";
                        } else {
                          echo "Belum dikonfirmasi";
                        }
                      } else {
                        echo "Sudah valid";
                      }
                    ?>
                  </td>
                  <td class="align-middle">
                    <?php if ($t['status_pembayaran'] == 0) : ?>
                      <a href="./transaksi_konfirmasi.php?id=<?=$t['id_transaksi']?>" class="btn btn-outline-primary">Konfirmasi</a>
                      <?php else : ?>
                      <a href="./transaksi_konfirmasi.php?id=<?=$t['id_transaksi']?>" class="btn btn-outline-success disabled">Konfirmasi</a>
                      <!-- <a href="./transaksi_invoice.php?id=<?=$t['id_transaksi']?>" class="btn btn-outline-success">Cetak Invoice</a> -->
                    <?php endif ?>
                  </td>
                </tr>
                <!-- modal bukti pembayaran -->
                <div class="modal fade" id="bukti<?=$t['id_transaksi']?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><?= $t['id_transaksi'] ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <img class="img-thumbnail mx-auto d-block" src="../file/bukti-pembayaran/<?= $t['bukti_pembayaran'] ?>" width="70%">
                      </div>
                    </div>
                  </div>
                </div>
                <!-- modal konfirmasi
                <div class="modal fade" id="konfirmasi<?=$t['id_transaksi']?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">

                      </div>
                    </div>
                  </div>
                </div> -->
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require('../layouts/footer_admin.php') ?>