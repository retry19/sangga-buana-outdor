<?php 
  require('../layouts/header_admin.php');
  require('../config/connection.php');

  $select_peminjaman = mysqli_query($conn, "SELECT * FROM peminjaman JOIN member ON peminjaman.id_member=member.id_member");
  $peminjamans = [];
  while ($peminjaman = mysqli_fetch_array($select_peminjaman)) {
    $peminjamans[] = $peminjaman;
  }
  
  $i = 1;
?>

<!-- heading content -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Daftar Peminjaman</h1>
  <button data-toggle="modal" data-target="#modalReport" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Download Report</button>
</div>

<!-- jika range tgl kosong -->
<?php 
  if (isset($_SESSION['notif'])) {
    if ($_SESSION['notif']=='tgl-error') {
      echo '
      <div class="row"><div class="col-12">  
        <div class="alert alert-danger alert-dismissible fade show mx-3 text-left" role="alert">
          <strong>Gagal!</strong> Tanggal tidak boleh kosong.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div></div>';
    }
    unset($_SESSION['notif']); 
  }
?>

<!-- awal table peminjaman -->
<div class="row">
  <div class="col-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Peminjaman</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table id="dataTable" class="table table-bordered">
            <thead>
              <tr>
                <th>Id Peminjaman</th>
                <th>Id Member</th>
                <th>Nama Member</th>
                <th>Metode Bayar</th>
                <th>Total Harga</th>
                <th>Tgl Sewa</th>
                <th>Lama Sewa</th>
                <th>Jaminan</th>
                <th>Status Barang</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($peminjamans as $p) : ?>
                <tr>
                  <td><?= $p['id_peminjaman'] ?></td>
                  <td><?= $p['id_member'] ?></td>
                  <td><?= $p['nama'] ?></td>
                  <td><?= $p['metode_bayar'] ?></td>
                  <td>Rp. <?= $p['total_harga'] ?></td>
                  <td><?= $p['tgl_sewa'] ?></td>
                  <td><?= $p['lama_sewa'] ?> hari</td>
                  <td><button type="button" class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#fotoJaminan<?=$i?>">Lihat foto</button></td>
                  <td>
                    <?php 
                      if ($p['status_barang'] == 0) {
                        echo "Belum diambil";
                      } elseif ($p['status_barang'] == 1 && $p['status_kembali'] == 1){
                        echo "Sudah dikembalikan";
                      } else {
                        echo "Sudah diambil";
                      }
                    ?>
                  </td>
                </tr>

                <!-- modal foto identitas -->
                <div class="modal fade" id="fotoJaminan<?=$i?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><?= $p['id_member'] ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <img class="img-thumbnail mx-auto d-block" src="../file/foto-identitas/<?= $p['jaminan'] ?>" width="70%">
                      </div>
                    </div>
                  </div>
                </div>
                <?php $i++ ?>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- akhir table peminjaman -->

<!-- modal report -->
<div class="modal fade" id="modalReport" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Download Report</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="./report/peminjaman.php" method="post">
        <div class="modal-body">
          <div class="form-check">
            <input class="form-check-input" type="radio" name="jenisReport" id="jenisReport1" value="all" checked>
            <label class="form-check-label" for="jenisReport1">
              Semua data
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="jenisReport" id="jenisReport2" value="date">
            <label class="form-check-label" for="jenisReport2">
              Sesuai Tanggal
            </label>
          </div>
          <div class="tanggalReport" style="display:none;">
            <br>
            <div class="form-group">
              <label for="tgl_awal">Tanggal Awal</label>
              <input type="date" name="tgl_awal" id="tgl_awal" class="form-control">
            </div>
            <div class="form-group">
              <label for="tgl_akhir">Tanggal Akhir</label>
              <input type="date" name="tgl_akhir" id="tgl_akhir" class="form-control">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Download</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- akhir modal report -->

<?php require('../layouts/footer_admin.php') ?>