<?php 
  require('../layouts/header_admin.php');
  require('../config/connection.php');
  
  $select_id_supplier = mysqli_query($conn, "SELECT id_supplier FROM supplier");
  $id_suppliers = [];
  while ($id_supplier = mysqli_fetch_array($select_id_supplier)) {
    $id_suppliers[] = $id_supplier;
  }
?>

<!-- heading content -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Tambah Barang</h1>
</div>

<?php 
  if (isset($_SESSION['notif'])) {
    if ($_SESSION['notif']=='size-file') {
      echo '
      <div class="row"><div class="col-12">  
        <div class="alert alert-danger alert-dismissible fade show mx-3 text-left" role="alert">
          <strong>Gagal!</strong> Foto barang terlalu besar, max = 5 MB.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div></div>';
    }
    elseif ($_SESSION['notif']=='ekstensi-file') {
      echo '
      <div class="row"><div class="col-12">  
        <div class="alert alert-danger alert-dismissible fade show mx-3 text-left" role="alert">
          <strong>Gagal!</strong> Foto barang hanya boleh JPG, PNG, JPEG, BMP.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div></div>';
    }
    unset($_SESSION['notif']);
  }
?>

<!-- form tambah alat -->
<div class="row">
  <div class="col-md-7">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Barang</h6>
      </div>
      <div class="card-body">
        <form action="./barang_insert.php" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <label for="id_supplier">Id Supplier</label>
            <select name="id_supplier" id="id_supplier" class="form-control" required>
              <?php foreach ($id_suppliers as $id) : ?>
                <option value="<?= $id['id_supplier'] ?>"><?= $id['id_supplier'] ?></option>
              <?php endforeach ?>
            </select>
          </div>
          <div class="form-group">
            <label for="nama_barang">Nama Barang</label>
            <input type="text" name="nama_barang" id="nama_barang" class="form-control" required>
          </div>
          <div class="form-group">
            <label for="merek">Merek</label>
            <input type="text" name="merek" id="merek" class="form-control" required>
          </div>
          <div class="form-group">
            <label for="jenis">Jenis</label>
            <input type="text" name="jenis" id="jenis" class="form-control" required>
          </div>
          <div class="form-group">
            <label for="harga_barang">Harga Barang</label>
            <input type="number" name="harga_barang" id="harga_barang" class="form-control" required>
          </div>
          <div class="form-group">
            <label for="harga_sewa">Harga Sewa</label>
            <input type="number" name="harga_sewa" id="harga_sewa" class="form-control" required>
          </div>
          <div class="form-group">
            <label for="stok_awal">Stok Awal</label>
            <input type="number" name="stok_awal" id="stok_awal" class="form-control" min="0" required>
          </div>
          <div class="form-group">
            <label for="tgl_barang_masuk">Tanggal Masuk</label>
            <input type="date" name="tgl_barang_masuk" id="tgl_barang_masuk" class="form-control" required>
          </div>
          <div class="form-group">
            <label for="deskripsi">Deskripsi</label>
            <textarea name="deskripsi" id="deskripsi" class="form-control" cols="30" rows="3" required></textarea>
          </div>
          <div class="form-group">
            <label for="foto_barang">Foto barang</label>
            <input type="file" name="foto_barang" id="foto_barang" class="form-control-file">
          </div>
          <div class="text-right">
            <button type="reset" class="btn btn-white">Reset</button>
            <button type="submit" class="btn btn-primary">Tambah</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<?php require('../layouts/footer_admin.php') ?>