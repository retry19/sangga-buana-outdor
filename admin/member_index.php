<?php 
  require('../layouts/header_admin.php');
  require('../config/connection.php');

  $i = 1;

  $select_member = mysqli_query($conn, "SELECT * FROM member");
  $members = [];
  while ($member = mysqli_fetch_array($select_member)) {
    $members[] = $member;
  }
?>

<!-- heading content -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Daftar Member</h1>
</div>

<!-- awal tabel member -->
<div class="row">
  <div class="col-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Daftar Member</h6>
      </div>
      <div class="card-body">
        <?php
          if (isset($_SESSION['notif'])) {
            if ($_SESSION['notif'] == 'member-delete') {
              echo '
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                  <strong>Berhasil!</strong> Data member berhasil dihapus.
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              ';
            }
            elseif ($_SESSION['notif'] == 'member-update') {
              echo '
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                  <strong>Berhasil!</strong> Data member berhasil diubah.
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              ';
            }
            elseif ($_SESSION['notif'] == 'member-insert') {
              echo '
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                  <strong>Berhasil!</strong> Data member berhasil ditambahkan.
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              ';
            }
            unset($_SESSION['notif']);
          } 
        ?>
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Lengkap</th>
                <th>Email</th>
                <th>Alamat</th>
                <th>No. HP</th>
                <th>Foto Identitas</th>
                <th>Opsi</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($members as $u) : ?>
                <tr>
                  <td><?= $i++ ?></td>
                  <td><?= $u['nama'] ?></td>
                  <td><?= $u['email'] ?></td>
                  <td><?= $u['alamat'] ?></td>
                  <td><?= $u['no_hp'] ?></td>
                  <td class="text-center"><button type="button" class="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#fotoIdentitas<?=$u['id_member']?>">Lihat foto</button></td>
                  <td>
                    <button class="btn btn-sm btn-outline-warning" data-toggle="modal" data-target="#editMember<?=$u['id_member']?>">Edit</button> &nbsp;
                    <a href="./member_hapus.php?id=<?=$u['id_member']?>" class="btn btn-sm btn-outline-danger" onclick="deleteConfirm(event)">Hapus</a>
                  </td>
                </tr>

                <!-- modal foto identitas -->
                <div class="modal fade" id="fotoIdentitas<?=$u['id_member']?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><?= $u['nama'] ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <img class="img-thumbnail mx-auto d-block" src="../file/foto-identitas/<?= $u['foto_identitas'] ?>" width="70%">
                      </div>
                    </div>
                  </div>
                </div>
                <!-- modal foto identitas -->
                <!-- modal edit member -->
                <div class="modal fade" id="editMember<?=$u['id_member']?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-scrollable">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <form action="./member_update.php?id=<?=$u['id_member']?>" method="post" enctype="multipart/form-data">
                        <div class="modal-body">
                          <div class="form-group row">
                            <label for="nama" class="col-sm-4 col-form-label text-right">Nama Lengkap</label>
                            <div class="col-sm-8">
                              <input type="text" name="nama" id="nama" class="form-control" value="<?=$u['nama']?>" required>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-right">Email</label>
                            <div class="col-sm-8">
                              <input type="email" name="email" id="email" class="form-control" value="<?=$u['email']?>" required>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="no_hp" class="col-sm-4 col-form-label text-right">No. HP</label>
                            <div class="col-sm-8">
                              <input type="number" name="no_hp" id="no_hp" class="form-control" value="<?=$u['no_hp']?>" required>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="alamat" class="col-sm-4 col-form-label text-right">Alamat</label>
                            <div class="col-sm-8">
                              <textarea name="alamat" id="alamat" cols="30" rows="3" class="form-control"><?= $u['alamat'] ?></textarea>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="foto_identitas" class="col-sm-4 col-form-label text-right">Foto Identitas (KTP)</label>
                            <div class="col-sm-2">
                              <img class="img-thumbnail mx-auto" src="../file/foto-identitas/<?= $u['foto_identitas'] ?>">
                            </div>
                            <div class="col-sm-6 align-middle">
                              <input type="file" name="foto_identitas" id="foto_identitas" class="form-control-file">
                              <input type="hidden" name="foto_identitas_lama" value="<?=$u['foto_identitas']?>">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="password" class="col-sm-4 col-form-label text-right">Password</label>
                            <div class="col-sm-8">
                              <input type="password" name="password" id="password" class="form-control" value="<?=$u['password']?>" required>
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                          <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                <!-- modal edit member -->
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- akhir tabel member -->

<?php require('../layouts/footer_admin.php') ?>