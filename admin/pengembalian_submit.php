<?php 
  require('../config/connection.php');
  session_start();

  $id_peminjaman = $_POST['id_peminjaman'];
  $jml_barang = $_POST['jml_barang'];
  $denda_telat = $_POST['denda_telat'];
  $denda_rusak = $_POST['denda_rusak'];
  
  // jika kondisi barang tidak semua di isi
  if (COUNT($_SESSION['barang_rusak']) != $jml_barang) {
    $_SESSION['notif'] = "kondisi-error";
    unset($_SESSION['barang_rusak']);
    unset($_SESSION['id_barang']);
 
    header('location:pengembalian_konfirmasi.php?id='.$id_peminjaman);die;
  }

  ksort($_SESSION['barang_rusak']);

  foreach ($_SESSION['barang_rusak'] as $br) {
    if (COUNT($br) >= 2) {
      $qty_rusak = 0;
      $kondisiArray = [];

      foreach ($br as $b) {
        $id_barang = substr(substr($b, 0, -2), -1);
        $k = substr($b, -1);
        
        // menentukan kondisi
        switch ($k) {
          case '0':
            $kondisi = 'B'; break; 
          case '1':
            $kondisi = 'RR'; 
            $qty_rusak += 1; break; 
          case '2':
            $kondisi = 'RB';
            $qty_rusak += 1; break; 
          case '3':
            $kondisi = 'HL';
            $qty_rusak += 1; break; 
        }
        
        array_push($kondisiArray, $kondisi);
      }
      
      $kondisiJson = json_encode($kondisiArray);

      // untuk mengambil qty setiap barang di detail peminjaman
      $select_barang = mysqli_query($conn, "SELECT qty FROM detail_peminjaman WHERE id_peminjaman='$id_peminjaman' AND id_barang='$id_barang'");
      $brg = mysqli_fetch_array($select_barang);
      $qty_barang = $brg['qty'];

      try {
        // update jmlh_diambil && jmlh_rusak di table barang
        mysqli_query($conn, "UPDATE barang SET jmlh_diambil=jmlh_diambil-'$qty_barang', jmlh_rusak=jmlh_rusak+'$qty_rusak' 
                              WHERE id_barang='$id_barang'");
  
        // update kondisi di detail peminjaman
        mysqli_query($conn, "UPDATE detail_peminjaman SET kondisi='$kondisiJson' WHERE id_peminjaman='$id_peminjaman' AND id_barang='$id_barang'");
      } catch (\Exception $e) {
        echo $e;
      }
    } else {
      $qty_rusak = 0;
      $kondisiArray = [];

      $id_barang = substr(substr($br[0], 0, -2), -1);
      $k = substr($br[0], -1);
      
      // menentukan kondisi
      switch ($k) {
        case '0':
          $kondisi = 'B'; break; 
        case '1':
          $kondisi = 'RR'; 
          $qty_rusak += 1; break; 
        case '2':
          $kondisi = 'RB';
          $qty_rusak += 1; break; 
        case '3':
          $kondisi = 'HL';
          $qty_rusak += 1; break; 
      }
      
      array_push($kondisiArray, $kondisi);
      
      $kondisiJson = json_encode($kondisiArray);

      // untuk mengambil qty setiap barang di detail peminjaman
      $select_barang = mysqli_query($conn, "SELECT qty FROM detail_peminjaman WHERE id_peminjaman='$id_peminjaman' AND id_barang='$id_barang'");
      $brg = mysqli_fetch_array($select_barang);
      $qty_barang = $brg['qty'];

      try {
        // update jmlh_diambil && jmlh_rusak di table barang
        mysqli_query($conn, "UPDATE barang SET jmlh_diambil=jmlh_diambil-'$qty_barang', jmlh_rusak=jmlh_rusak+'$qty_rusak' 
                              WHERE id_barang='$id_barang'");
  
        // update kondisi di detail peminjaman
        mysqli_query($conn, "UPDATE detail_peminjaman SET kondisi='$kondisiJson' WHERE id_peminjaman='$id_peminjaman' AND id_barang='$id_barang'");
      } catch (\Exception $e) {
        echo $e;
      }
    }
  }
  
  try {
    // update status_kembali di table peminjaman
    mysqli_query($conn, "UPDATE peminjaman SET status_kembali='1', denda_rusak='$denda_rusak'+'$denda_telat' WHERE id_peminjaman='$id_peminjaman'");

    // insert data ke table pengembalian
    mysqli_query($conn, "INSERT INTO pengembalian VALUES('','$id_peminjaman','$denda_telat','$denda_rusak')");
  } catch (\Exception $e) {
    echo $e;
  }

  $_SESSION['notif'] = "pengembalian-success";
  header('location:pengembalian_index.php');
?>
