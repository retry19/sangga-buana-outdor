<?php 
  require('../config/connection.php');
  session_start();

  extract($_POST);

  // untuk file upload
  $ekstensi = ['png', 'jpeg', 'jpg', 'bmp'];
  $nama_file = $_FILES['foto_barang']['name'];
  $x = explode('.', $nama_file);
  $ekstensi_file = strtolower(end($x));
  $ukuran_file = $_FILES['foto_barang']['size'];
  $temp_file = $_FILES['foto_barang']['tmp_name'];

  $namaFile = $nama.'_'.$nama_file;

  $date = date('Y-m-d', strtotime($tgl_barang_masuk));

  if (in_array($ekstensi_file, $ekstensi)) {
    if ($ukuran_file <= 5000000) {
      move_uploaded_file($temp_file, '../images/'.$namaFile);
      try {
        mysqli_query($conn, "INSERT INTO barang 
                              VALUES('','$id_supplier','$nama_barang','$namaFile', '$jenis', '$stok_awal', '$date', '$deskripsi', '0', '$merek', '$harga_sewa', '0', '$harga_barang')");
      } catch (\Exception $e) {
        echo $e;
      }
    } else {
      $_SESSION['notif'] = "size-file";
      header('location:barang_tambah.php');die;
    }
  } else {
    $_SESSION['notif'] = "ekstensi-file";
    header('location:barang_tambah.php');die;
  }

  $_SESSION['notif'] = "barang-insert";
  header('location:barang_index.php');
?>