<?php 
  require('../layouts/header_admin.php'); 
  require('../config/connection.php');

  $select_supplier = mysqli_query($conn, "SELECT * FROM supplier");
  $suppliers = [];
  while ($supplier = mysqli_fetch_array($select_supplier)) {
    $suppliers[] = $supplier;
  }

  $i = 1;
?>

<!-- heading content -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Daftar Supplier</h1>
</div>

<!-- awal tabel supplier -->
<div class="row">
  <div class="col-12">
    <?php 
      if (isset($_SESSION['notif'])) {
        if ($_SESSION['notif'] == 'supplier-edit') {
          echo '
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Selamat!</strong> Data Supplier berhasil diedit.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          ';
        } 
        elseif ($_SESSION['notif'] == 'supplier-hapus') {
          echo '
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Berhasil!</strong> Data Supplier telah terhapus.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          ';
        }
        elseif ($_SESSION['notif'] == 'supplier-add') {
          echo '
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <strong>Berhasil!</strong> Data Supplier telah ditambahkan.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          ';
        }
        unset($_SESSION['notif']); 
      } 
    ?>

    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Supplier</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="myTable">
            <thead>
              <tr>
                <th>No</th>
                <th>Id Supplier</th>
                <th>Nama Lengkap</th>
                <th>Alamat</th>
                <th>No. HP</th>
                <th>Opsi</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($suppliers as $s) : ?>
              <tr>
                <td><?= $i++ ?></td>
                <td><?= $s['id_supplier'] ?></td>
                <td><?= $s['nama_supplier'] ?></td>
                <td><?= $s['alamat'] ?></td>
                <td><?= $s['no_hp'] ?></td>
                <td>
                  <button type="button" class="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#ubahSupplier<?=$s['id_supplier']?>">Edit</button>&nbsp;
                  <a href="./supplier_hapus.php?id=<?=$s['id_supplier']?>" class="btn btn-sm btn-outline-danger" onclick="deleteConfirm(event)">Hapus</a>
                </td>
              </tr>
              <!-- modal gudang -->
              <div class="modal fade" id="ubahSupplier<?=$s['id_supplier']?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit data</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form action="supplier_edit.php" method="post">
                        <input type="hidden" name="id_supplier" value="<?=$s['id_supplier']?>">
                        <div class="form-group">
                          <label for="nama_supplier">Nama Lengkap</label>
                          <input type="text" class="form-control" name="nama_supplier" id="nama_supplier" value="<?=$s['nama_supplier']?>" required>
                        </div>
                        <div class="form-group">
                          <label for="no_hp">No. HP</label>
                          <input type="number" class="form-control" name="no_hp" id="no_hp" value="<?=$s['no_hp']?>" required>
                        </div>
                        <div class="form-group">
                          <label for="alamat">Alamat</label>
                          <textarea name="alamat" id="alamat" cols="30" rows="3" class="form-control" required><?= $s['alamat'] ?></textarea>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-white" data-dismiss="modal">Batal</button>
                          <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- akhir tabel supplier -->

<?php require('../layouts/footer_admin.php') ?>