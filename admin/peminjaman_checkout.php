<?php 
  require('../config/connection.php');
  session_start();

  extract($_POST);

  if (isset($member_baru)) {
    // cek apakah email sudah terdaftar
    $cek_email = mysqli_query($conn, "SELECT * FROM member WHERE email='$email'");
    if (mysqli_num_rows($cek_email)) {
      $_SESSION['notif'] = "email-error";
      header('location:peminjaman_tagihan.php');die;
    }
  }

  $id_transaksi = 'T'.substr($id_peminjaman, 1, 7);
  $tglSewa = date('Y-m-d', strtotime($tgl_sewa));

  // apakah member baru?
  if (!isset($member_baru)) {
    $select_member = mysqli_query($conn, "SELECT foto_identitas FROM member WHERE id_member='$id_member'");
    $member = mysqli_fetch_array($select_member);
    $jaminan = $member['foto_identitas'];

    // memasukan data peminjaman ke db peminjaman
    $add_peminjaman = mysqli_query($conn, "INSERT INTO peminjaman 
                                            VALUES('$id_peminjaman','$id_member','Tunai','','$total_harga','$tglSewa','$lama_sewa','$jaminan','',NULL)");  
    $add_transaksi = mysqli_query($conn, "INSERT INTO transaksi 
                                          VALUES ('$id_transaksi','$id_peminjaman',NULL,NULL,NULL,NULL,'$tglSewa',NULL,'Tunai',NULL,'')");
  } else {
    // untuk file upload
    $ekstensi = ['png', 'jpeg', 'jpg', 'bmp'];
    $nama_file = $_FILES['foto_identitas']['name'];
    $x = explode('.', $nama_file);
    $ekstensi_file = strtolower(end($x));
    $ukuran_file = $_FILES['foto_identitas']['size'];
    $temp_file = $_FILES['foto_identitas']['tmp_name'];

    $namaFile = $nama.'_'.$nama_file;

    if (in_array($ekstensi_file, $ekstensi) === true) {
      if ($ukuran_file <= 5000000) {
        move_uploaded_file($temp_file, '../file/foto-identitas/'.$namaFile);
        $in_member = mysqli_query($conn, "INSERT INTO member 
                                          VALUES('','$nama','$namaFile','member','$no_hp','$email','$alamat','$password')");
      } else {
        $_SESSION['notif'] = "size-file";
        header('location:peminjaman_tagihan.php');die;
      }
    } else {
      $_SESSION['notif'] = "ekstensi-file";
      header('location:peminjaman_tagihan.php');die;
    }

    $select_member = mysqli_query($conn, "SELECT * FROM member WHERE id_member=(SELECT MAX(id_member) FROM member)");
    $member = mysqli_fetch_array($select_member);
    $member_id = $member['id_member'];
    $member_jaminan = $member['foto_identitas'];

    // memasukan data peminjaman ke db peminjaman
    $add_peminjaman = mysqli_query($conn, "INSERT INTO peminjaman 
                                            VALUES('$id_peminjaman','$member_id','Tunai','','$total_harga','$tglSewa','$lama_sewa','$member_jaminan','',NULL)");  
    $add_transaksi = mysqli_query($conn, "INSERT INTO transaksi 
                                          VALUES ('$id_transaksi','$id_peminjaman',NULL,NULL,NULL,NULL,'$tglSewa',NULL,'Tunai',NULL,'')");
  }

  // memasukan barang ke detail_peminjaman
  foreach ($_SESSION['keranjang_belanja'] as $key => $value) {
    $id_barang = $_SESSION['keranjang_belanja'][$key]['id_barang'];
    $qty = $_SESSION['keranjang_belanja'][$key]['qty'];

    // memanggil detail barang sesuai id barang yang di sewa
    $query = mysqli_query($conn, "SELECT * FROM barang WHERE id_barang='$id_barang'");
    $barang = [];
    while ($b = mysqli_fetch_assoc($query)) {
      $barang[] = $b;
    }

    $harga_barang = $barang[0]['harga_sewa']*$_SESSION['keranjang_belanja'][$key]['qty'];

    try {
      $add_detail_peminjaman = mysqli_query($conn, "INSERT INTO detail_peminjaman 
                                                    VALUES('','$id_peminjaman','$id_barang','$qty','$harga_barang', NULL)");
      $up_barang = mysqli_query($conn, "UPDATE barang 
                                        SET jmlh_diambil=jmlh_diambil+'$qty' 
                                        WHERE id_barang='$id_barang'");
    } catch (\Exception $e) {
      echo $e;
    }
  }

  unset($_SESSION['keranjang_belanja']);
  unset($_SESSION['id_barang']);

  $_SESSION['notif'] = 'peminjaman-add';
  header('location:peminjaman_index.php');
?>