<?php 
  require('../layouts/header_admin.php');
  require('../config/connection.php');

  $select_barang = mysqli_query($conn, "SELECT * FROM barang");
  $barangs = [];
  while ($barang = mysqli_fetch_array($select_barang)) {
    $barangs[] = $barang;
  }

  $select_id_supplier = mysqli_query($conn, "SELECT id_supplier FROM supplier");
  $id_suppliers = [];
  while ($id_supplier = mysqli_fetch_array($select_id_supplier)) {
    $id_suppliers[] = $id_supplier;
  }

  $i = 1;
?>

<!-- heading content -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Daftar Barang</h1>
</div>

<?php 
  if (isset($_SESSION['notif'])) {
    if ($_SESSION['notif']=='barang-update') {
      echo '
      <div class="row"><div class="col-12">  
        <div class="alert alert-success alert-dismissible fade show mx-3 text-left" role="alert">
          <strong>Berhasil!</strong> Data Barang telah diupdate.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div></div>';
    } 
    elseif ($_SESSION['notif']=='size-file') {
      echo '
      <div class="row"><div class="col-12">  
        <div class="alert alert-danger alert-dismissible fade show mx-3 text-left" role="alert">
          <strong>Gagal!</strong> Foto Barang terlalu besar, max = 5 MB.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div></div>';
    }
    elseif ($_SESSION['notif']=='ekstensi-file') {
      echo '
      <div class="row"><div class="col-12">  
        <div class="alert alert-danger alert-dismissible fade show mx-3 text-left" role="alert">
          <strong>Gagal!</strong> Foto Barang hanya boleh JPG, PNG, JPEG, BMP.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div></div>';
    }
    elseif ($_SESSION['notif']=='barang-delete') {
      echo '
      <div class="row"><div class="col-12">  
        <div class="alert alert-success alert-dismissible fade show mx-3 text-left" role="alert">
          <strong>Berhasil!</strong> Data Barang telah dihapus.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div></div>';
    }
    elseif ($_SESSION['notif']=='barang-insert') {
      echo '
      <div class="row"><div class="col-12">  
        <div class="alert alert-success alert-dismissible fade show mx-3 text-left" role="alert">
          <strong>Berhasil!</strong> Data Barang baru telah ditambahkan.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div></div>';
    }
    unset($_SESSION['notif']);
  }
?>

<!-- notif barang habis -->
<?php foreach ($barangs as $br) : ?>
<?php if (($br['stok_awal']-$br['jmlh_diambil']-$br['jmlh_rusak']) == 0) : ?>
  <div class="row"><div class="col-12">  
    <div class="alert alert-danger alert-dismissible fade show mx-3 text-left" role="alert">
      <strong>Stok barang habis!</strong> <?= $br['nama_barang'] ?> stok <?= $br['stok_awal']-$br['jmlh_diambil']-$br['jmlh_rusak'] ?>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  </div></div>
<?php elseif (($br['stok_awal']-$br['jmlh_diambil']-$br['jmlh_rusak']) <= 5) : ?>
  <div class="row"><div class="col-12">  
    <div class="alert alert-danger alert-dismissible fade show mx-3 text-left" role="alert">
      <strong>Stok barang hampir habis!</strong> <?= $br['nama_barang'] ?> hanya tersisa <?= $br['stok_awal']-$br['jmlh_diambil']-$br['jmlh_rusak'] ?>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  </div></div>
<?php endif ?>
<?php endforeach ?>

<!-- awal tabel Barang -->
<div class="row">
  <div class="col-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Barang Outdor</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered nowrap" id="dataTable">
            <thead>
              <tr>
                <th rowspan="2" class="align-middle">No</th>
                <th rowspan="2" class="align-middle">Id Supplier</th>
                <th rowspan="2" class="align-middle">Nama Barang</th>
                <th rowspan="2" class="align-middle">Foto</th>
                <th rowspan="2" class="align-middle">Deskripsi</th>
                <th rowspan="2" class="align-middle">Merek</th>
                <th rowspan="2" class="align-middle">Jenis</th>
                <th rowspan="2" class="align-middle">Harga Sewa</th>
                <th colspan="3" class="text-center">Stok</th>
                <th rowspan="2" class="align-middle">Tanggal Masuk</th>
                <th rowspan="2" class="align-middle">Harga Barang</th>
                <th rowspan="2" class="align-middle">Opsi</th>
              </tr>
              <tr>
                <th>Awal</th>
                <th>Di Ambil</th>
                <th>Rusak</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($barangs as $b) : ?>
              <tr>
                <td><?= $i++ ?></td>
                <td class="text-center"><?= $b['id_supplier'] ?></td>
                <td><?= $b['nama_barang'] ?></td>
                <td><button class="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#fotoBarang<?=$b['id_barang']?>">Lihat foto...</button></td>
                <td><?= $b['deskripsi'] ?></td>
                <td><?= $b['merek'] ?></td>
                <td><?= $b['jenis'] ?></td>
                <td><?= $b['harga_sewa'] ?></td>
                <td><?= $b['stok_awal'] ?></td>
                <td><?= $b['jmlh_diambil'] ?></td>
                <td><?= $b['jmlh_rusak'] ?></td>
                <td><?= $b['tgl_barang_masuk'] ?></td>
                <td>Rp. <?= $b['harga_barang'] ?></td>
                <td>
                  <button class="btn btn-sm btn-warning" data-toggle="modal" data-target="#editBarang<?=$b['id_barang']?>">Edit</button> &nbsp;
                  <a href="./barang_hapus.php?id=<?=$b['id_barang']?>" class="btn btn-sm btn-danger" onclick="deleteConfirm(event)">Hapus</a>
                </td>
              </tr>

              <!-- modal foto barang -->
              <div class="modal fade" id="fotoBarang<?=$b['id_barang']?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel"><?= $b['nama_barang'] ?></h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <img class="mx-auto d-block" src="../images/<?= $b['foto_barang'] ?>" width="70%">
                    </div>
                  </div>
                </div>
              </div>
              <!-- modal foto barang -->
              <!-- modal edit barang -->
              <div class="modal fade" id="editBarang<?=$b['id_barang']?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <form action="./barang_update.php?id=<?=$b['id_barang']?>" method="post" enctype="multipart/form-data">
                      <div class="modal-body">
                        <div class="form-group row">
                          <label for="id_supplier" class="col-sm-4 col-form-label text-right">Id Supplier</label>
                          <div class="col-sm-8">
                            <select name="id_supplier" id="id_supplier" class="form-control">
                              <?php foreach ($id_suppliers as $id) : ?>
                                <?php if ($id['id_supplier'] == $b['id_supplier']) : ?>
                                  <option value="<?=$id['id_supplier']?>" selected><?=$id['id_supplier']?></option>
                                <?php else : ?>
                                  <option value="<?=$id['id_supplier']?>"><?=$id['id_supplier']?></option>
                                <?php endif ?>
                              <?php endforeach ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="nama_barang" class="col-sm-4 col-form-label text-right">Nama Alat</label>
                          <div class="col-sm-8">
                            <input type="text" name="nama_barang" id="nama_barang" class="form-control" value="<?=$b['nama_barang']?>" required>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="merek" class="col-sm-4 col-form-label text-right">Merek</label>
                          <div class="col-sm-8">
                            <input type="text" name="merek" id="merek" class="form-control" value="<?=$b['merek']?>" required>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="jenis" class="col-sm-4 col-form-label text-right">Jenis</label>
                          <div class="col-sm-8">
                            <input type="text" name="jenis" id="jenis" class="form-control" value="<?=$b['jenis']?>" required>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="harga_barang" class="col-sm-4 col-form-label text-right">Harga Barang</label>
                          <div class="col-sm-8">
                            <input type="number" name="harga_barang" id="harga_barang" class="form-control" value="<?=$b['harga_barang']?>" required>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="harga_sewa" class="col-sm-4 col-form-label text-right">Harga Sewa</label>
                          <div class="col-sm-8">
                            <input type="number" name="harga_sewa" id="harga_sewa" class="form-control" value="<?=$b['harga_sewa']?>" required>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="stok_awal" class="col-sm-4 col-form-label text-right">Stok Awal</label>
                          <div class="col-sm-8">
                            <input type="number" name="stok_awal" id="stok_awal" class="form-control" min="0" value="<?=$b['stok_awal']?>" required>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="jmlh_rusak" class="col-sm-4 col-form-label text-right">Jumlah Rusak</label>
                          <div class="col-sm-8">
                            <input type="number" name="jmlh_rusak" id="jmlh_rusak" class="form-control" min="0" value="<?=$b['jmlh_rusak']?>" required>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="jmlh_diambil" class="col-sm-4 col-form-label text-right">Jumlah Diambil</label>
                          <div class="col-sm-8">
                            <input type="number" name="jmlh_diambil" id="jmlh_diambil" class="form-control" min="0" value="<?=$b['jmlh_diambil']?>" required>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="tgl_barang_masuk" class="col-sm-4 col-form-label text-right">Tanggal Masuk</label>
                          <div class="col-sm-8">
                            <input type="date" name="tgl_barang_masuk" id="tgl_barang_masuk" class="form-control" value="<?=$b['tgl_barang_masuk']?>" required>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="deskripsi" class="col-sm-4 col-form-label text-right">Deskripsi</label>
                          <div class="col-sm-8">
                            <textarea name="deskripsi" id="deskripsi" class="form-control" cols="30" rows="3"><?= $b['deskripsi'] ?></textarea>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="foto_barang" class="col-sm-4 col-form-label text-right">Foto barang</label>
                          <div class="col-sm-2">
                            <img class="img-thumbnail mx-auto" src="../images/<?= $b['foto_barang'] ?>">
                          </div>
                          <div class="col-sm-6 align-middle">
                            <input type="file" name="foto_barang" id="foto_barang" class="form-control-file">
                            <input type="hidden" name="foto_barang_lama" value="<?=$b['foto_barang']?>">
                          </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <!-- modal edit barang -->
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- akhir tabel alat -->

<?php require('../layouts/footer_admin.php') ?>