<?php 
  require('../layouts/header_admin.php');
  require('../config/connection.php');

  $select_transaksi = mysqli_query($conn, "SELECT * FROM transaksi t INNER JOIN peminjaman p ON t.id_peminjaman=p.id_peminjaman 
                                            INNER JOIN member m ON p.id_member=m.id_member ORDER BY t.status_pembayaran ASC");
  $transaksis = [];
  while ($transaksi = mysqli_fetch_array($select_transaksi)) {
    $transaksis[] = $transaksi;
  }
?>

<!-- heading content -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Daftar Transaksi</h1>
  <button data-toggle="modal" data-target="#modalReport" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Download Report</button>
</div>

<!-- jika range tgl kosong -->
<?php 
  if (isset($_SESSION['notif'])) {
    if ($_SESSION['notif']=='tgl-error') {
      echo '
      <div class="row"><div class="col-12">  
        <div class="alert alert-danger alert-dismissible fade show mx-3 text-left" role="alert">
          <strong>Gagal!</strong> Tanggal tidak boleh kosong.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div></div>';
    }
    unset($_SESSION['notif']); 
  }
?>

<div class="row">
  <div class="col-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Transaksi</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="table-keuangan">
            <thead>
              <tr>
                <th rowspan="2" class="align-middle">Id Transaksi</th>
                <th rowspan="2" class="align-middle">Id Peminjaman</th>
                <th rowspan="2" class="align-middle">Nama Member</th>
                <th rowspan="2" class="align-middle">Id User</th>
                <th rowspan="2" class="align-middle">Metode Pembayaran</th>
                <th colspan="2" class="align-middle text-center">Transfer</th>
                <th rowspan="2" class="align-middle">Bukti Pembayaran</th>
                <th rowspan="2" class="align-middle">Tanggal Pembayaran</th>
                <th rowspan="2" class="align-middle">Total Pembayaran</th>
                <th rowspan="2" class="align-middle">Jumlah DP</th>
                <th rowspan="2" class="align-middle">Total Harga</th>
                <th rowspan="2" class="align-middle">Status Pembayaran</th>
              </tr>
              <tr>
                <th>Atas Nama</th>
                <th>Nama Bank</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($transaksis as $t) : ?>
                <tr>
                  <td class="align-middle"><?= $t['id_transaksi'] ?></td>
                  <td class="align-middle"><?= $t['id_peminjaman'] ?></td>
                  <td class="align-middle"><?= $t['nama'] ?></td>
                  <td class="align-middle"><?= $t['id_user'] ?></td>
                  <td class="align-middle"><?= $t['metode_pembayaran'] ?></td>
                  <td class="align-middle"><?= $t['transfer_an'] ?></td>
                  <td class="align-middle"><?= $t['transfer_bank'] ?></td>
                  <td class="align-middle">
                    <?php if ($t['metode_pembayaran'] == "Transfer") : ?>
                      <button type="button" class="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#bukti<?=$t['id_transaksi']?>">Lihat foto..</button>
                    <?php endif ?>
                  </td>
                  <td class="align-middle"><?= $t['tgl_pembayaran'] ?></td>
                  <td class="align-middle"><?= $t['total_pembayaran'] ?></td>
                  <td class="align-middle"><?= $t['jumlah_dp'] ?></td>
                  <td class="align-middle"><?= $t['total_harga'] ?></td>
                  <td class="align-middle">
                    <?php 
                      if ($t['status_pembayaran'] == 0) {
                        if ($t['metode_pembayaran'] == "Tunai") {
                          echo "Belum bayar";
                        } else {
                          echo "Belum dikonfirmasi";
                        }
                      } else {
                        echo "Sudah valid";
                      }
                    ?>
                  </td>
                </tr>
                <!-- modal bukti pembayaran -->
                <div class="modal fade" id="bukti<?=$t['id_transaksi']?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><?= $t['id_transaksi'] ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <img class="img-thumbnail mx-auto d-block" src="../file/bukti-pembayaran/<?= $t['bukti_pembayaran'] ?>" width="70%">
                      </div>
                    </div>
                  </div>
                </div>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- modal report -->
<div class="modal fade" id="modalReport" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Download Report</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="./report/transaksi.php" method="post">
        <div class="modal-body">
          <div class="form-check">
            <input class="form-check-input" type="radio" name="jenisReport" id="jenisReport1" value="all" checked>
            <label class="form-check-label" for="jenisReport1">
              Semua data
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="jenisReport" id="jenisReport2" value="date">
            <label class="form-check-label" for="jenisReport2">
              Sesuai Tanggal
            </label>
          </div>
          <div class="tanggalReport" style="display:none;">
            <br>
            <div class="form-group">
              <label for="tgl_awal">Tanggal Awal</label>
              <input type="date" name="tgl_awal" id="tgl_awal" class="form-control">
            </div>
            <div class="form-group">
              <label for="tgl_akhir">Tanggal Akhir</label>
              <input type="date" name="tgl_akhir" id="tgl_akhir" class="form-control">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Download</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- akhir modal report -->

<?php require('../layouts/footer_admin.php') ?>