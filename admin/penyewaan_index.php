<?php 
  require('../layouts/header_admin.php');
  require('../config/connection.php');

  $select_peminjaman = mysqli_query($conn, "SELECT p.id_peminjaman, p.id_member, m.nama, p.metode_bayar, p.total_harga, p.tgl_sewa, p.lama_sewa,                                                    p.status_barang, p.status_kembali  
                                            FROM peminjaman p INNER JOIN member m ON p.id_member=m.id_member 
                                            INNER JOIN transaksi t ON t.id_peminjaman=p.id_peminjaman
                                            WHERE t.status_pembayaran='1'");
  $peminjamans = [];
  while ($peminjaman = mysqli_fetch_array($select_peminjaman)) {
    $peminjamans[] = $peminjaman;
  }
?>

<!-- heading content -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Daftar Penyewaan</h1>
</div>

<?php 
  if (isset($_SESSION['notif'])) {
    if ($_SESSION['notif']=='penyewaan-update') {
      echo '
      <div class="row"><div class="col-12">  
        <div class="alert alert-success alert-dismissible fade show mx-3 text-left" role="alert">
          <strong>Berhasil!</strong> Penyewaan telah selesai dilakukan.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div></div>';
    }
    unset($_SESSION['notif']);
  }
?>

<!-- awal penyewaan -->
<div class="row">
  <div class="col-md-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Penyewaan</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="table-gudang">
            <thead>
              <tr>
                <th>Id Peminjaman</th>
                <th>Id Member</th>
                <th>Nama Member</th>
                <th>Metode Bayar</th>
                <th>Total Harga</th>
                <th>Tanggal Sewa</th>
                <th>Lama Sewa</th>
                <th>Status</th>
                <th>Opsi</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($peminjamans as $p) : ?>
              <tr>
                <td class="align-middle"><?= $p['id_peminjaman'] ?></td>
                <td class="align-middle"><?= $p['id_member'] ?></td>
                <td class="align-middle"><?= $p['nama'] ?></td>
                <td class="align-middle"><?= $p['metode_bayar'] ?></td>
                <td class="align-middle"><?= $p['total_harga'] ?></td>
                <td class="align-middle"><?= $p['tgl_sewa'] ?></td>
                <td class="align-middle"><?= $p['lama_sewa'] ?></td>
                <td class="align-middle">
                  <?php if ($p['status_barang'] == 1) : ?>
                    <?php if ($p['status_kembali'] == 1) : ?>
                      Sudah dikembalikan
                    <?php else : ?>
                      Sudah diambil
                    <?php endif ?>
                  <?php else : ?>
                    Belum diambil
                  <?php endif ?>
                </td>
                <td class="align-middle">
                  <a href="./penyewaan_detail.php?id=<?= $p['id_peminjaman'] ?>" class="btn btn-sm btn-outline-primary">Lihat Detail</a>
                </td>
              </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- akhir penyewaan -->

<?php require('../layouts/footer_admin.php') ?>