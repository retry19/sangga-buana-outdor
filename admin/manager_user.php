<?php 
  require('../layouts/header_admin.php'); 
  require('../config/connection.php');
  $h = 1;

  $select_user = mysqli_query($conn, "SELECT * FROM user");
  $users = [];
  while ($user = mysqli_fetch_assoc($select_user)) {
    $users[] = $user;
  }
?>

<!-- heading content -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Daftar User</h1>
  <button data-toggle="modal" data-target="#modalReport" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Download Report</button>
</div>

<!-- awal tabel user -->
<div class="row">
  <div class="col-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">User</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="myTable">
            <thead>
              <tr>
                <th class="align-middle">No</th>
                <th class="align-middle">Nama Lengkap</th>
                <th class="align-middle" width="30%">Alamat</th>
                <th class="align-middle">Level</th>
                <th class="align-middle">Username</th>
                <th class="align-middle">Password</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($users as $u) : ?>
                <tr>
                  <td class="align-middle"><?= $h++ ?></td>
                  <td class="align-middle"><?= $u['nama_user'] ?></td>
                  <td class="align-middle"><?= $u['alamat'] ?></td>
                  <td class="align-middle"><?= $u['level'] ?></td>
                  <td class="align-middle"><?= $u['username'] ?></td>
                  <td class="align-middle"><?= $u['password'] ?></td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- akhir tabel user -->

<!-- modal report -->
<div class="modal fade" id="modalReport" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Download Report</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="./report/user.php" method="post">
        <div class="modal-body">
          <div class="form-check">
            <input class="form-check-input" type="radio" name="jenisReport" id="jenisReport1" value="all">
            <label class="form-check-label" for="jenisReport1">
              Semua user
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="jenisReport" id="jenisReport2" value="admin">
            <label class="form-check-label" for="jenisReport2">
              Hanya admin 
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="jenisReport" id="jenisReport3" value="gudang">
            <label class="form-check-label" for="jenisReport3">
              Hanya bagian gudang 
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="jenisReport" id="jenisReport4" value="keuangan">
            <label class="form-check-label" for="jenisReport4">
              Hanya bagian keuangan 
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="jenisReport" id="jenisReport5" value="manager">
            <label class="form-check-label" for="jenisReport5">
              Hanya manager 
            </label>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Download</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- akhir modal report -->

<?php require('../layouts/footer_admin.php') ?>