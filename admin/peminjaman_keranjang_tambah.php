<?php 
  require('../config/connection.php');
  session_start();

  $id_barang = $_GET['id'];

  $ket_barang = [
    'id_barang' => $id_barang,
    'qty' => 1
  ];

  // apakah barang di sudah berada di keranjang?
  if (!in_array($id_barang, $_SESSION['id_barang'])) {
    array_push($_SESSION['keranjang_belanja'], $ket_barang);
    array_push($_SESSION['id_barang'], $id_barang);
    $_SESSION['notif'] = 'barang-tambah';
  } else {
    $_SESSION['notif'] = 'barang-ada';
  }

  header('location:peminjaman_tambah.php');
?>