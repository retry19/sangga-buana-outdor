<?php 
  require('../layouts/header_admin.php');
  require('../config/connection.php');

  $select_member = mysqli_query($conn, "SELECT id_member,nama FROM member");
  $members = [];
  while ($member = mysqli_fetch_assoc($select_member)) {
    $members[] = $member;
  }

  $select_id_p = mysqli_query($conn, "SELECT RIGHT(MAX(id_peminjaman),3) AS last_id FROM peminjaman");
  $last_id = mysqli_fetch_array($select_id_p);
  
  if (mysqli_num_rows($select_id_p)) {
    $id_peminjaman = 'P'.date('ym').sprintf("%03s",$last_id['last_id']+1);
  } else {
    $id_peminjaman = 'P'.date('ym').sprintf("%03s","001");
  }

  $total_harga = 0;
  // perulangan untuk mendapatkan total harga dari keranjang
  foreach ($_SESSION['keranjang_belanja'] as $key => $value) {
    $id_barang = $_SESSION['keranjang_belanja'][$key]['id_barang'];
    $qty = $_SESSION['keranjang_belanja'][$key]['qty'];

    // memanggil detail barang sesuai id barang yang di sewa
    $query = mysqli_query($conn, "SELECT * FROM barang WHERE id_barang='$id_barang'");
    $barang = [];
    while ($b = mysqli_fetch_assoc($query)) {
      $barang[] = $b;
    }

    $harga_barang = $barang[0]['harga_sewa']*$_SESSION['keranjang_belanja'][$key]['qty'];
    $total_harga += $harga_barang;
  }
?>

<!-- heading content -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Detail Tagihan</h1>
</div>

<?php 
  if (isset($_SESSION['notif'])) {
    if ($_SESSION['notif']=='email-error') {
      echo '
      <div class="row"><div class="col-12">  
        <div class="alert alert-danger alert-dismissible fade show mx-3 text-left" role="alert">
          <strong>Email sudah terdaftar!</strong> silahkan masukan email lain.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div></div>';
    } elseif ($_SESSION['notif']=='size-file') {
      echo '
      <div class="row"><div class="col-12">  
        <div class="alert alert-danger alert-dismissible fade show mx-3 text-left" role="alert">
          <strong>Size foto identitas maksimal 5 MB!</strong>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div></div>';
    } elseif ($_SESSION['notif']=='ekstensi-file') {
      echo '
      <div class="row"><div class="col-12">  
        <div class="alert alert-danger alert-dismissible fade show mx-3 text-left" role="alert">
          <strong>Ekstensi file tidak valid!</strong> hanya boleh PNG, JPG, JPEG, BMP.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div></div>';
    }
    unset($_SESSION['notif']);
  }
?>

<form action="./peminjaman_checkout.php" method="post" enctype="multipart/form-data">
  <div class="row mb-3">
    <div class="col-6"><a href="./peminjaman_tambah.php" class="btn btn-outline-primary"><i class="fas fa-chevron-left"></i>&nbsp; Keranjang Belanja</a></div>
    <div class="col-6 text-right"><button type="submit" class="btn btn-primary">Checkout &nbsp;<i class="fas fa-angle-right"></i></button></div>
  </div>

  <div class="row">
    <!-- awal tagihan -->
    <div class="col-md-7">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Tagihan</h6>
        </div>
        <div class="card-body">
          <table class="table table-borderless">
            <tbody>
              <tr>
                <th class="align-middle">Id Peminjaman</th>
                <td><input type="text" name="id_peminjaman" class="form-control border-0" value="<?=$id_peminjaman?>" readonly></td>
              </tr>
              <tr>
                <th class="align-middle">Id Member</th>
                <td>
                  <select name="id_member" id="id_member" class="form-control">
                    <option value="" selected>Pilih...</option>
                    <?php foreach ($members as $m) : ?>
                      <option value="<?= $m['id_member'] ?>"><?= $m['id_member'] ?> (<?= $m['nama'] ?>)</option>
                    <?php endforeach ?>
                  </select>
                </td>
              </tr>
              <tr>
                <th></th>
                <td class="pl-5">
                  <input type="checkbox" name="member_baru" id="member-baru" class="form-check-input">
                  <label for="member-baru" class="form-check-label">Buat member baru</label>
                </td>
              </tr>
              <tr>
                <th class="align-middle">Tanggal Sewa</th>
                <td><input type="text" name="tgl_sewa" class="form-control border-0" value="<?= date('d-m-Y') ?>" readonly></td>
              </tr>
              <tr>
                <th class="align-middle">Lama Sewa (hari)</th>
                <td>
                  <div class="input-group w-50">
                    <div class="input-group-prepend">
                      <button type="button" id="btn-minus-hari" class="btn btn-sm btn-outline-primary btn-inc"><i class="fas fa-minus"></i></button>
                    </div>
                    <input type="text" name="lama_sewa" id="tagihan-lama-sewa" class="form-control-plaintext border text-center" value="1" readonly>
                    <div class="input-group-append">
                      <button type="button" id="btn-plus-hari" class="btn btn-sm btn-primary btn-inc"><i class="fas fa-plus"></i></button>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <th class="align-middle">Total (Rp)</th>
                <td class="align-middle">
                  <input type="text" name="total_harga" id="tagihan-total-harga" class="form-control border-0" value="<?=$total_harga?>" readonly>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- akhir tagihan -->
    <!-- awal data member -->
    <div class="col-md-5" id="data-member" style="display: none;">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Data Member</h6>
        </div>
        <div class="card-body">
          <table cellpadding="10" class="table-table-borderless w-100">
            <tbody>
              <tr>
                <th width="35%" class="text-right">Nama Lengkap</th>
                <td><input type="text" name="nama" id="nama" class="form-control"></td>
              </tr>
              <tr>
                <th class="text-right">Email</th>
                <td><input type="email" name="email" id="email" class="form-control"></td>
              </tr>
              <tr>
                <th class="text-right">No. HP</th>
                <td><input type="number" name="no_hp" id="no_hp" class="form-control"></td>
              </tr>
              <tr>
                <th class="text-right">Alamat</th>
                <td><textarea name="alamat" id="alamat" cols="30" rows="3" class="form-control"></textarea></td>
              </tr>
              <tr>
                <th class="text-right">Foto Identitas (KTP)</th>
                <td><input type="file" class="form-control-file" name="foto_identitas"></td>
              </tr>
              <tr>
                <th class="text-right">Password</th>
                <td><input type="password" name="password" id="password" class="form-control"></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- akhir data member -->
  </div>
</form>

<?php require('../layouts/footer_admin.php') ?>