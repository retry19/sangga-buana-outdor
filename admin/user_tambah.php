<?php require('../layouts/header_admin.php'); require('../config/connection.php'); ?>

<!-- heading content -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Tambah User</h1>
</div>

<?php 
  if (isset($_SESSION['notif'])) {
    if ($_SESSION['notif']=='username-error') {
      echo '
      <div class="row"><div class="col-12">  
        <div class="alert alert-danger alert-dismissible fade show mx-3 text-left" role="alert">
          <strong>Gagal!</strong> Username telah terdaftar.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div></div>';
    } 
    unset($_SESSION['notif']);
  }
?>

<!-- awal form tambah user -->
<div class="row">
  <div class="col-md-6">
    <div class="card shadow mb-4">
      <div class="card-body"> 
        <form action="user_insert.php" method="post">
          <div class="form-group">
            <label for="level">Level</label>
            <select name="level" id="level" class="form-control bg-warning" required>
              <option value="" disabled selected>Pilih...</option>
              <option value="gudang">Gudang</option>
              <option value="keuangan">Keuangan</option>
              <option value="admin">Admin</option>
              <option value="manager">Manager</option>
            </select>
          </div>
          <div class="form-group">
            <label for="nama_user">Nama Lengkap</label>
            <input type="text" name="nama_user" id="nama_user" class="form-control" required>
          </div>
          <div class="form-group">
            <label for="alamat">Alamat</label>
            <textarea name="alamat" id="alamat" cols="30" rows="3" class="form-control" required></textarea>
          </div>
          <div class="form-group">
            <label for="username">Username</label>
            <input type="text" name="username" id="username" class="form-control" required>
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input type="password" name="password" id="password" class="form-control" required>
          </div>
          <div class="text-right">
            <button type="reset" class="btn btn-white">Reset</button>
            <button type="submit" class="btn btn-primary">Tambah</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- akhir form tambah user -->

<?php require('../layouts/footer_admin.php') ?>