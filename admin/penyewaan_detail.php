<?php
  require('../layouts/header_admin.php');
  require('../config/connection.php');

  $id_peminjaman = $_GET['id'];

  $select_detail = mysqli_query($conn, "SELECT detail_peminjaman.id_barang, detail_peminjaman.qty, barang.nama_barang, barang.jenis, barang.merek                                  
                                        FROM detail_peminjaman 
                                        INNER JOIN barang
                                        ON detail_peminjaman.id_barang=barang.id_barang 
                                        WHERE id_peminjaman='$id_peminjaman'");
  $details = [];
  while ($detail = mysqli_fetch_array($select_detail)) {
    $details[] = $detail;
  }
  $select_status = mysqli_query($conn, "SELECT status_barang FROM peminjaman WHERE id_peminjaman='$id_peminjaman'");
  $status_barang = mysqli_fetch_assoc($select_status);
?>

<!-- heading content -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Detail Peminjaman</h1>
</div>

<div class="row mb-3">
  <div class="col-6"><a href="./penyewaan_index.php" class="btn btn-outline-secondary"><i class="fas fa-chevron-left"></i>&nbsp; Kembali</a></div>
  <div class="col-6 text-right">
    <form action="./penyewaan_submit.php" method="post">
      <input type="hidden" name="id_peminjaman" value="<?= $id_peminjaman ?>">
      <?php if ($status_barang['status_barang'] == 0) : ?>
        <button type="submit" class="btn btn-primary">Selesai &nbsp;<i class="fas fa-angle-right"></i></button>
      <?php endif ?>
    </form>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Barang</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>Id Barang</th>
                <th>Nama Barang</th>
                <th>Merek</th>
                <th>Jenis</th>
                <th>Jumlah Barang</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($details as $d) : ?>
              <tr>
                <td><?= $d['id_barang'] ?></td>
                <td><?= $d['nama_barang'] ?></td>
                <td><?= $d['merek'] ?></td>
                <td><?= $d['jenis'] ?></td>
                <td><?= $d['qty'] ?></td>
              </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require('../layouts/footer_admin.php') ?>