<?php require('../layouts/header_admin.php') ?>

<!-- heading content -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Tambah Member</h1>
</div>

<?php 
  if (isset($_SESSION['notif'])) {
    if ($_SESSION['notif']=='size-file') {
      echo '
      <div class="row"><div class="col-12">  
        <div class="alert alert-danger alert-dismissible fade show mx-3 text-left" role="alert">
          <strong>Gagal!</strong> Foto alat terlalu besar, max = 5 MB.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div></div>';
    }
    elseif ($_SESSION['notif']=='ekstensi-file') {
      echo '
      <div class="row"><div class="col-12">  
        <div class="alert alert-danger alert-dismissible fade show mx-3 text-left" role="alert">
          <strong>Gagal!</strong> Foto alat hanya boleh JPG, PNG, JPEG, BMP.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div></div>';
    }
    unset($_SESSION['notif']);
  }
?>

<div class="row">
  <div class="col-md-7">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Member</h6>
      </div>
      <div class="card-body">
        <form action="member_insert.php" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <label for="nama">Nama Lengkap</label>
            <input type="text" name="nama" id="nama" class="form-control" required>
          </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" name="email" id="email" class="form-control" required>
          </div>
          <div class="form-group">
            <label for="no_hp">No. HP</label>
            <input type="number" name="no_hp" id="no_hp" class="form-control" required>
          </div>
          <div class="form-group">
            <label for="alamat">Alamat</label>
            <textarea name="alamat" id="alamat" cols="30" rows="3" class="form-control" required></textarea>
          </div>
          <div class="form-group">
            <label for="foto_identitas">Foto Identitas (KTP)</label>
            <input type="file" name="foto_identitas" id="foto_identitas" class="form-control-file" required>
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input type="password" name="password" id="password" class="form-control" required>
          </div>
          <div class="text-right">
            <button type="reset" class="btn btn-white">Reset</button>
            <button type="submit" class="btn btn-primary">Tambah</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<?php require('../layouts/footer_admin.php') ?>