<?php 
  require('../config/connection.php');
  session_start();

  extract($_POST);
  $id_barang = $_GET['id'];

  // jika foto barang tidak di ganti
  if ($_FILES['foto_barang']['name'] == null) {
    $up_barang = mysqli_query($conn, "UPDATE barang 
                                      SET id_supplier='$id_supplier', nama_barang='$nama_barang', jenis='$jenis', stok_awal='$stok_awal', tgl_barang_masuk='$tgl_barang_masuk', deskripsi='$deskripsi', jmlh_rusak='$jmlh_rusak', merek='$merek', harga_sewa='$harga_sewa', jmlh_diambil='$jmlh_diambil', harga_barang='$harga_barang' 
                                      WHERE id_barang='$id_barang'");
  } else {
    // untuk file upload
    $ekstensi = ['png', 'jpeg', 'jpg', 'bmp'];
    $nama_file = $_FILES['foto_barang']['name'];
    $x = explode('.', $nama_file);
    $ekstensi_file = strtolower(end($x));
    $ukuran_file = $_FILES['foto_barang']['size'];
    $temp_file = $_FILES['foto_barang']['tmp_name'];

    $namaFile = $nama.'_'.$nama_file;

    $date = date('Y-m-d', strtotime($tgl_barang_masuk));

    if (in_array($ekstensi_file, $ekstensi)) {
      if ($ukuran_file <= 5000000) {
        move_uploaded_file($temp_file, '../images/'.$namaFile);
        mysqli_query($conn, "UPDATE barang 
                              SET id_supplier='$id_supplier', nama_barang='$nama_barang', foto_barang='$namaFile', jenis='$jenis', stok_awal='$stok_awal', tgl_barang_masuk='$date', deskripsi='$deskripsi', jmlh_rusak='$jmlh_rusak', merek='$merek', harga_sewa='$harga_sewa', jmlh_diambil='$jmlh_diambil', harga_barang='$harga_barang' 
                              WHERE id_barang='$id_barang'");
        unlink('../images/'.$foto_barang_lama);
      } else {
        $_SESSION['notif'] = "size-file";
        header('location:barang_index.php');die;
      }
    } else {
      $_SESSION['notif'] = "ekstensi_file";
      header('location:barang_index.php');die;
    }
  }

  $_SESSION['notif'] = "barang-update";
  header('location:barang_index.php');
?>