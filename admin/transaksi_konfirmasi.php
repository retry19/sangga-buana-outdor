<?php 
  require('../layouts/header_admin.php');
  require('../config/connection.php');

  $id = $_GET['id'];

  $select_transaksi = mysqli_query($conn, "SELECT * FROM transaksi t INNER JOIN peminjaman p ON t.id_peminjaman=p.id_peminjaman
                                            INNER JOIN member m ON p.id_member=m.id_member WHERE t.id_transaksi='$id'");
  $t = mysqli_fetch_array($select_transaksi);

  $id_peminjaman = $t['id_peminjaman'];
  $select_detail_barang = mysqli_query($conn, "SELECT dp.qty, dp.harga_sewa, b.nama_barang FROM detail_peminjaman dp INNER JOIN barang b
                                                ON dp.id_barang=b.id_barang WHERE dp.id_peminjaman='$id_peminjaman'");
  $details = [];
  while ($detail = mysqli_fetch_array($select_detail_barang)) {
    $details[] = $detail;
  }
  
?>

<!-- heading content -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Detail Transaksi</h1>
</div>

<div class="row mb-3">
  <div class="col-6">
    <a href="./transaksi_index.php" class="btn btn-outline-secondary"><i class="fas fa-chevron-left"></i>&nbsp; Kembali</a>
  </div>
  <div class="col-6 text-right">
    <form action="./transaksi_submit.php?id=<?=$t['id_transaksi']?>" method="post">
      <input type="hidden" name="t_metode" value="<?= $t['metode_pembayaran'] ?>">
      <input type="hidden" name="t_total_bayar" value="<?= $t['total_harga'] ?>">
      <?php if ( (int)$t['total_harga'] <= (int) $t['total_pembayaran']) : ?>
        <button type="submit" class="btn btn-primary">Selesai &nbsp;<i class="fas fa-angle-right"></i></button>
      <?php else : ?>
        <button type="submit" id="transaksi_submit" class="btn btn-primary" disabled>Selesai &nbsp;<i class="fas fa-angle-right"></i></button>
      <?php endif ?>
    </form>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Detail</h6>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label text-right">Id Transaksi</label>
              <div class="col-sm-8">
                <input type="text" class="form-control-plaintext border pl-2" value="<?=$t['id_transaksi']?>" readonly>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label text-right">Id Peminjaman</label>
              <div class="col-sm-8">
                <input type="text" class="form-control-plaintext border pl-2" value="<?=$t['id_peminjaman']?>" readonly>
              </div>
            </div>
            <div class="form-group row mt-5">
              <label class="col-sm-4 col-form-label text-right">Id Member</label>
              <div class="col-sm-8">
                <input type="text" class="form-control-plaintext border pl-2" value="<?=$t['id_member']?>" readonly>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label text-right">Nama Member</label>
              <div class="col-sm-8">
                <input type="text" class="form-control-plaintext border pl-2" value="<?=$t['nama']?>" readonly>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label text-right">Tanggal Sewa</label>
              <div class="col-sm-8">
                <input type="text" class="form-control-plaintext border pl-2" value="<?=$t['tgl_sewa']?>" readonly>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label text-right">Lama Sewa</label>
              <div class="col-sm-8">
                <input type="text" class="form-control-plaintext border pl-2" value="<?=$t['lama_sewa']?> Hari" readonly>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>Nama barang</th>
                  <th>Jumlah</th>
                  <th>Jumlah Harga</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($details as $dt) : ?>
                  <tr>
                    <td><?= $dt['nama_barang'] ?></td>
                    <td><?= $dt['qty'] ?></td>
                    <td><?= $dt['harga_sewa'] ?></td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
            <div class="form-group row mt-5">
              <label class="col-sm-4 col-form-label text-right">Total Harga (Rp)</label>
              <div class="col-sm-8">
                <input type="text" class="form-control form-control-lg" id="total_harga" value="<?=$t['total_harga']?>" readonly>
              </div>
            </div>
            <div class="form-group row mt-5">
              <label class="col-sm-4 col-form-label text-right">Uang Bayar (Rp)</label>
              <div class="col-sm-8">
                <?php 
                  if ($t['total_pembayaran'] != null) {
                    $total_pembayaran = $t['total_pembayaran'];
                  } 
                ?>
                <input type="number" class="form-control" id="uang_bayar" value="<?=$total_pembayaran?>">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label text-right">Uang Kembali (Rp)</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="uang_kembali" readonly>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require('../layouts/footer_admin.php') ?>