<?php 
  require('../../config/connection.php');
  session_start();

  extract($_POST);

  $date = date('Y-m-d');

  // apakah akan semua data?
  if ($jenisReport == 'all') { 
    $select_transaksi = mysqli_query($conn, "SELECT * FROM transaksi t INNER JOIN peminjaman p ON t.id_peminjaman=p.id_peminjaman 
                                              INNER JOIN member m ON p.id_member=m.id_member
                                              INNER JOIN user u ON t.id_user=u.id_user ORDER BY t.status_pembayaran ASC");
    $transaksis = [];
    while ($transaksi = mysqli_fetch_array($select_transaksi)) {
      $transaksis[] = $transaksi;
    }
  } else {
    if ($tgl_awal == null && $tgl_akhir == null) {
      $_SESSION['notif'] = "tgl-error";
      header('location:../manager_transaksi.php');
    }

    $select_transaksi = mysqli_query($conn, "SELECT * FROM transaksi t INNER JOIN peminjaman p ON t.id_peminjaman=p.id_peminjaman
                                              INNER JOIN member m ON p.id_member=m.id_member 
                                              INNER JOIN user u ON t.id_user=u.id_user 
                                              WHERE t.tgl_pembayaran BETWEEN '$tgl_awal' AND '$tgl_akhir'");
    $transaksis = [];
    while ($transaksi = mysqli_fetch_array($select_transaksi)) {
      $transaksis[] = $transaksi;
    }
  }

  $content = '
      <style>
        table { border-collapse:collapse; page-break-inside:auto; }
        table tr { page-break-inside:avoid; page-break-after:auto; }
        table th { padding:8px 8px; background-color:#f60; color:#fff; }
        table td { padding:8px 8px; }
      </style>
    ';
  $content .= '
    <hr>
    <h2>Daftar Transaksi</h2>
    <h6>Sangga Buana Outdor</h6>
    <hr>
    <table border="1" cellspacing="0" cellpadding="5">
      <tr>
        <th rowspan="2">Id Transaksi</th>
        <th rowspan="2">Id Peminjaman</th>
        <th rowspan="2">Nama Member</th>
        <th rowspan="2">Metode Pembayaran</th>
        <th colspan="2">Transfer</th>
        <th rowspan="2">Tanggal Pembayaran</th>
        <th rowspan="2">Total Pembayaran</th>
        <th rowspan="2">Jumlah DP</th>
        <th rowspan="2">Total Harga</th>
        <th rowspan="2">Status Pembayaran</th>
        <th rowspan="2">Validate By</th>
      </tr>
      <tr>
        <th>Atas nama</th>  
        <th>Nama bank</th>  
      </tr>';

  foreach ($transaksis as $t) {
    if ($t['status_pembayaran'] == 0) {
      $status = "Belum valid";
    } else {
      $status = "Sudah valid";
    }
    $tgl_bayar = date('d F Y', strtotime($t['tgl_pembayaran']));
    $content .= '
      <tr>
        <td>'.$t['id_transaksi'].'</td>
        <td>'.$t['id_peminjaman'].'</td>
        <td>'.$t['nama'].'</td>
        <td>'.$t['metode_pembayaran'].'</td>
        <td>'.$t['transfer_an'].'</td>
        <td>'.$t['transfer_bank'].'</td>
        <td>'.$tgl_bayar.'</td>
        <td>'.$t['total_pembayaran'].'</td>
        <td>'.$t['jumlah_dp'].'</td>
        <td>'.$t['total_harga'].'</td>
        <td>'.$status.'</td>
        <td>'.$t['nama_user'].'</td>
      </tr>
    ';
  }
  $content .= '
    </table>
    <p>Dicetak pada tanggal : '.$date.'</p>
  ';

  require '../../vendor/autoload.php';
  use Spipu\Html2Pdf\Html2Pdf as SpipuHtml2Pdf;

  $html2pdf = new SpipuHtml2Pdf('L', 'A3');
  $html2pdf->writeHTML($content);
  $html2pdf->output($date.'-transaksi.pdf');
?>