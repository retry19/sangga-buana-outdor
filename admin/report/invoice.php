<?php 
  require('../../config/connection.php');
  session_start();

  $id_peminjaman = $_GET['id'];

  $date = date('d-m-Y');

  $select_peminjaman = mysqli_query($conn, "SELECT p.id_peminjaman, p.tgl_sewa, p.lama_sewa, p.metode_bayar, p.status_barang, m.nama 
                                            FROM peminjaman p INNER JOIN member m ON p.id_member=m.id_member 
                                            WHERE id_peminjaman='$id_peminjaman'");
  $peminjaman = mysqli_fetch_array($select_peminjaman);

  $select_status_pembayaran = mysqli_query($conn, "SELECT total_pembayaran FROM transaksi WHERE id_peminjaman='$id_peminjaman'");
  $status_pembayaran = mysqli_fetch_assoc($select_status_pembayaran);

  $select_detail_peminjaman = mysqli_query($conn, "SELECT dp.qty, dp.harga_sewa, b.nama_barang 
                                                    FROM detail_peminjaman dp INNER JOIN barang b ON dp.id_barang=b.id_barang  
                                                    WHERE id_peminjaman='$id_peminjaman'");
  $details = [];
  while ($detail = mysqli_fetch_array($select_detail_peminjaman)) $details[] = $detail;

    if ($peminjaman['metode_bayar'] == 'Transfer' && $status_pembayaran['total_pembayaran'] != null) {
        $metode = 'PAID';
    } elseif ($peminjaman['metode_bayar'] == 'Tunai' && $peminjaman['status_barang'] == 1) {
        $metode = 'PAID';
    } else {
        $metode = 'UNPAID';
    }
  $content = '
      <style>
        .header { position: relative; margin-bottom: 120px; }
        h1, h2 { display: inline-block; }
        .title { margin-bottom: 0; }
        .align-start { position: absolute; right: auto; left: 0; }
        .align-end { position: absolute; right: 0; left: auto; }
        .status-bayar { text-transform: uppercase; }
        table { border-collapse:collapse; page-break-inside:auto; width: 100%; }
        table tr { page-break-inside:avoid; page-break-after:auto; }
        table th { padding:8px 8px; background-color:#f60; color:#fff; }
        table td { padding:8px 8px; }
      </style>
    ';
  $content .= '
    <hr>
    <div class="header">
        <div class="align-start">
            <h2 class="title">INVOICE</h2>
            <h5>Sangga Buana Outdor</h5>
        </div>
        <div class="align-end">
            <h1 class="status-bayar">'.$metode.'</h1>
            <h4>'.$peminjaman['metode_bayar'].'</h4>
        </div>
    </div>
    <hr><br><br>
    <div class="content-peminjaman">
        <table cellspacing="0" cellpadding="5">
            <tr>
                <td width="10%"><strong>Id Peminjaman</strong></td>
                <td width="40%">: '.$peminjaman['id_peminjaman'].'</td>
                <td width="10%"><strong>Tanggal Sewa</strong></td>
                <td>: '.date('d-m-Y', strtotime($peminjaman['tgl_sewa'])).'</td>
            </tr>
            <tr>
                <td width="10%"><strong>Nama</strong></td>
                <td width="40%">: '.$peminjaman['nama'].'</td>
                <td width="10%"><strong>Lama Sewa</strong></td>
                <td>: '.$peminjaman['lama_sewa'].' hari</td>
            </tr>
        </table>
    </div>
    <br>
    <table border="1" cellspacing="0" cellpadding="5">
      <tr>
        <th width="55%">Nama Barang</th>
        <th>Jumlah Barang</th>
        <th>Harga Sewa</th>
      </tr>';
    $total_bayar = 0;
  foreach ($details as $d) {
    $content .= '
      <tr>
        <td>'.$d['nama_barang'].'</td>
        <td align="center">'.$d['qty'].'</td>
        <td align="right">'.$d['harga_sewa'].'</td>
      </tr>
    ';
    $total_bayar += $d['harga_sewa'];
  }
  $content .= '
        <tr>
            <td align="right" colspan="2"><strong>Total (Rp)</strong></td>
            <td align="right"><strong>'.$total_bayar.'</strong></td>
        </tr>
    </table>
    <br>
    <p>Dicetak pada tanggal : '.$date.'</p>
    <br>
    <div style="border-bottom:3px dashed #00f;"></div>
  ';

  require '../../vendor/autoload.php';
  use Spipu\Html2Pdf\Html2Pdf as SpipuHtml2Pdf;

  $html2pdf = new SpipuHtml2Pdf('P', 'A4');
  $html2pdf->writeHTML($content);
  $html2pdf->output($date.'-'.$id_peminjaman.'-invoice.pdf');
?>