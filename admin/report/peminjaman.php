<?php 
  require('../../config/connection.php');
  session_start();

  extract($_POST);

  $date = date('Y-m-d');

  // apakah akan semua data?
  if ($jenisReport == 'all') { 
    $select_peminjaman = mysqli_query($conn, "SELECT * FROM peminjaman p INNER JOIN member m ON p.id_member=m.id_member");
    $peminjamans = [];
    while ($peminjaman = mysqli_fetch_array($select_peminjaman)) {
      $peminjamans[] = $peminjaman;
    }
  } else {
    if ($tgl_awal == null && $tgl_akhir == null) {
      $_SESSION['notif'] = "tgl-error";
      header('location:../manager_peminjaman.php');
    }

    $select_peminjaman = mysqli_query($conn, "SELECT * FROM peminjaman p INNER JOIN member m ON p.id_member=m.id_member 
                                              WHERE p.tgl_sewa BETWEEN '$tgl_awal' AND '$tgl_akhir'");
    $peminjamans = [];
    while ($peminjaman = mysqli_fetch_array($select_peminjaman)) {
      $peminjamans[] = $peminjaman;
    }
  }

  $content = '
      <style>
        table { border-collapse:collapse; }
        table th { padding:8px 8px; background-color:#f60; color:#fff; }
        table td { padding:8px 8px; }
      </style>
    ';
  $content .= '
    <hr>
    <h2>Daftar Peminjaman</h2>
    <h6>Sangga Buana Outdor</h6>
    <hr>
    <table border="1" cellspacing="0" cellpadding="5">
      <tr>
        <th>Id Peminjaman</th>
        <th>Id Member</th>
        <th>Nama Member</th>
        <th>Metode Bayar</th>
        <th>Total Harga</th>
        <th>Tanggal Sewa</th>
        <th>Lama Sewa</th>
        <th>Status Barang</th>
      </tr>';
  $total_peminjaman_harga = 0;
  foreach ($peminjamans as $p) {
    if ($p['status_barang'] == 0) {
      $status = "Belum diambil";
    } elseif ($p['status_barang'] == 1 && $p['status_kembali'] == 1){
      $status = "Sudah dikembalikan";
    } else {
      $status = "Sudah diambil, belum dikembalikan";
    }
    $tgl_sewa = date('d F Y', strtotime($p['tgl_sewa']));
    $content .= '
      <tr>
        <td>'.$p['id_peminjaman'].'</td>
        <td>'.$p['id_member'].'</td>
        <td>'.$p['nama'].'</td>
        <td>'.$p['metode_bayar'].'</td>
        <td>'.$p['total_harga'].'</td>
        <td>'.$tgl_sewa.'</td>
        <td>'.$p['lama_sewa'].' Hari</td>
        <td>'.$status.'</td>
      </tr>
    ';
    $total_peminjaman_harga += $p['total_harga'];
  }
  $content .= '
      <tr>
        <td colspan="7">Total (Rp)</td>
        <td>'.$total_peminjaman_harga.'</td>
      </tr>
    </table>
    <p>Dicetak pada tanggal : '.$date.'</p>
  ';

  require '../../vendor/autoload.php';
  use Spipu\Html2Pdf\Html2Pdf as SpipuHtml2Pdf;

  $html2pdf = new SpipuHtml2Pdf('L');
  $html2pdf->writeHTML($content);
  $html2pdf->output($date.'-peminjaman.pdf');
?>