<?php 
  require('../../config/connection.php');
  session_start();

  extract($_POST);

  $date = date('Y-m-d');

  $select_member = mysqli_query($conn, "SELECT * FROM member");
  $members = [];
  while ($member = mysqli_fetch_array($select_member)) {
    $members[] = $member;
  }

  $content = '
      <style>
        table { border-collapse:collapse; page-break-inside:auto; }
        table tr { page-break-inside:avoid; page-break-after:auto; }
        table th { padding:8px 8px; background-color:#f60; color:#fff; }
        table td { padding:8px 8px; }
      </style>
    ';
  $no = 1;
  $content .= '
    <hr>
    <h2>Daftar Member</h2>
    <h6>Sangga Buana Outdor</h6>
    <hr>
    <table border="1" cellspacing="0" cellpadding="5">
      <tr>
        <th>No</th>
        <th>Nama Lengkap</th>
        <th>Email</th>
        <th>Alamat</th>
        <th>No. HP</th>
      </tr>';

  foreach ($members as $s) {
    $content .= '
      <tr>
        <td>'.$no++.'</td>
        <td>'.$s['nama'].'</td>
        <td>'.$s['email'].'</td>
        <td>'.$s['alamat'].'</td>
        <td>'.$s['no_hp'].'</td>
      </tr>
    ';
  }
  $content .= '
    </table>
    <p>Dicetak pada tanggal : '.$date.'</p>
  ';

  require '../../vendor/autoload.php';
  use Spipu\Html2Pdf\Html2Pdf as SpipuHtml2Pdf;

  $html2pdf = new SpipuHtml2Pdf('P', 'A4');
  $html2pdf->writeHTML($content);
  $html2pdf->output($date.'-member.pdf');
?>