<?php 
  require('../../config/connection.php');
  session_start();

  extract($_POST);

  $date = date('Y-m-d');

  $select_denda = mysqli_query($conn, "SELECT pg.id_pengembalian, pg.id_peminjaman, m.nama, pg.denda_telat, pg.denda_rusak FROM pengembalian pg INNER JOIN peminjaman p ON pg.id_peminjaman=p.id_peminjaman INNER JOIN member m ON p.id_member=m.id_member");
  $dendas = [];
  while ($denda = mysqli_fetch_array($select_denda)) {
    $dendas[] = $denda;
  }

  $content = '
      <style>
        table { border-collapse:collapse; page-break-inside:auto; }
        table tr { page-break-inside:avoid; page-break-after:auto; }
        table th { padding:8px 8px; background-color:#f60; color:#fff; }
        table td { padding:8px 8px; }
      </style>
    ';
  $content .= '
    <hr>
    <h2>Daftar Denda</h2>
    <h6>Sangga Buana Outdor</h6>
    <hr>
    <table border="1" cellspacing="0" cellpadding="5">
      <tr>
        <th>Id Pengembalian</th>
        <th>Id Peminjaman</th>
        <th>Nama Member</th>
        <th>Denda Telat</th>
        <th>Denda Rusak</th>
      </tr>';

  foreach ($dendas as $d) {
    $content .= '
      <tr>
        <td>'.$d['id_pengembalian'].'</td>
        <td>'.$d['id_peminjaman'].'</td>
        <td>'.$d['nama'].'</td>
        <td>'.$d['denda_telat'].'</td>
        <td>'.$d['denda_rusak'].'</td>
      </tr>
    ';
  }
  $content .= '
    </table>
    <p>Dicetak pada tanggal : '.$date.'</p>
  ';

  require '../../vendor/autoload.php';
  use Spipu\Html2Pdf\Html2Pdf as SpipuHtml2Pdf;

  $html2pdf = new SpipuHtml2Pdf('P', 'A4');
  $html2pdf->writeHTML($content);
  $html2pdf->output($date.'-denda.pdf');
?>