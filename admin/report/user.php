<?php 
  require('../../config/connection.php');
  session_start();

  extract($_POST);

  $date = date('Y-m-d');

  if ($jenisReport == 'all') {
    $select_user = mysqli_query($conn, "SELECT * FROM user");
  } else {
    $select_user = mysqli_query($conn, "SELECT * FROM user WHERE level='$jenisReport'");
  }

  $users = [];
  while ($user = mysqli_fetch_array($select_user)) {
    $users[] = $user;
  }

  $content = '
      <style>
        table { border-collapse:collapse; page-break-inside:auto; }
        table tr { page-break-inside:avoid; page-break-after:auto; }
        table th { padding:8px 8px; background-color:#f60; color:#fff; }
        table td { padding:8px 8px; }
      </style>
    ';
  $no = 1;
  $content .= '
    <hr>
    <h2>Daftar User</h2>
    <h6>Sangga Buana Outdor</h6>
    <hr>
    <table border="1" cellspacing="0" cellpadding="5">
      <tr>
        <th>No</th>
        <th>Nama Lengkap</th>
        <th>Alamat</th>
        <th>Level</th>
        <th>Username</th>
        <th>Password</th>
      </tr>';

  foreach ($users as $u) {
    $content .= '
      <tr>
        <td>'.$no++.'</td>
        <td>'.$u['nama_user'].'</td>
        <td>'.$u['alamat'].'</td>
        <td>'.$u['level'].'</td>
        <td>'.$u['username'].'</td>
        <td>'.$u['password'].'</td>
      </tr>
    ';
  }
  $content .= '
    </table>
    <p>Dicetak pada tanggal : '.$date.'</p>
  ';

  require '../../vendor/autoload.php';
  use Spipu\Html2Pdf\Html2Pdf as SpipuHtml2Pdf;

  $html2pdf = new SpipuHtml2Pdf('L', 'A4');
  $html2pdf->writeHTML($content);
  $html2pdf->output($date.'-user.pdf');
?>