<?php 
  require('../../config/connection.php');
  session_start();

  extract($_POST);

  $date = date('Y-m-d');

  // apakah akan semua data?
  if ($jenisReport == 'all') { 
    $select_barang = mysqli_query($conn, "SELECT * FROM barang");
    $barangs = [];
    while ($barang = mysqli_fetch_array($select_barang)) {
      $barangs[] = $barang;
    }
  } else {
    if ($tgl_awal == null && $tgl_akhir == null) {
      $_SESSION['notif'] = "tgl-error";
      header('location:../manager_barang.php');
    }

    $select_barang = mysqli_query($conn, "SELECT * FROM barang WHERE tgl_barang_masuk BETWEEN '$tgl_awal' AND '$tgl_akhir'");
    $barangs = [];
    while ($barang = mysqli_fetch_array($select_barang)) {
      $barangs[] = $barang;
    }
  }

  $content = '
      <style>
        table { border-collapse:collapse; page-break-inside:auto; }
        table tr { page-break-inside:avoid; page-break-after:auto; }
        table th { padding:8px 8px; background-color:#f60; color:#fff; }
        table td { padding:8px 8px; }
      </style>
    ';
  $no = 1;
  $content .= '
    <hr>
    <h2>Daftar Barang</h2>
    <h6>Sangga Buana Outdor</h6>
    <hr>
    <table border="1" cellspacing="0" cellpadding="5">
      <tr>
        <th rowspan="2">No</th>
        <th rowspan="2">Id Supplier</th>
        <th rowspan="2">Nama Barang</th>
        <th rowspan="2">Deskripsi</th>
        <th rowspan="2">Merek</th>
        <th rowspan="2">Jenis</th>
        <th rowspan="2">Harga Barang</th>
        <th rowspan="2">Harga Sewa</th>
        <th colspan="3">Stok</th>
        <th rowspan="2">Tanggal Masuk</th>
      </tr>
      <tr>
        <th>Awal</th>  
        <th>Diambil</th>  
        <th>Rusak</th>  
      </tr>';

  foreach ($barangs as $b) {
    $tgl_barang = date('d F Y', strtotime($b['tgl_barang_masuk']));
    $content .= '
      <tr>
        <td>'.$no++.'</td>
        <td>'.$b['id_supplier'].'</td>
        <td>'.$b['nama_barang'].'</td>
        <td>'.$b['deskripsi'].'</td>
        <td>'.$b['merek'].'</td>
        <td>'.$b['jenis'].'</td>
        <td>'.$b['harga_barang'].'</td>
        <td>'.$b['harga_sewa'].'</td>
        <td>'.$b['stok_awal'].'</td>
        <td>'.$b['jmlh_diambil'].'</td>
        <td>'.$b['jmlh_rusak'].'</td>
        <td>'.$tgl_barang.'</td>
      </tr>
    ';
  }
  $content .= '
    </table>
    <p>Dicetak pada tanggal : '.$date.'</p>
  ';

  require '../../vendor/autoload.php';
  use Spipu\Html2Pdf\Html2Pdf as SpipuHtml2Pdf;

  $html2pdf = new SpipuHtml2Pdf('L', 'A4');
  $html2pdf->writeHTML($content);
  $html2pdf->output($date.'-barang.pdf');
?>