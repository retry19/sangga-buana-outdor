<?php require('../layouts/header_admin.php') ?>
<?php 
  require('../config/connection.php');
  $h = 1;

  $select_user = mysqli_query($conn, "SELECT * FROM user");
  $users = [];
  while ($user = mysqli_fetch_assoc($select_user)) {
    $users[] = $user;
  }

  $level = ['admin','keuangan','gudang','manager'];
?>

<!-- heading content -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Daftar User</h1>
</div>

<?php 
  if (isset($_SESSION['notif'])) {
    if ($_SESSION['notif']=='user-insert') {
      echo '
      <div class="row"><div class="col-12">  
        <div class="alert alert-success alert-dismissible fade show mx-3 text-left" role="alert">
          <strong>Berhasil!</strong> Data user baru telah ditambahkan.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div></div>';
    }
    elseif ($_SESSION['notif']=='user-update') {
      echo '
      <div class="row"><div class="col-12">  
        <div class="alert alert-success alert-dismissible fade show mx-3 text-left" role="alert">
          <strong>Berhasil!</strong> Data user telah diubah.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div></div>';
    }
    elseif ($_SESSION['notif']=='user-delete') {
      echo '
      <div class="row"><div class="col-12">  
        <div class="alert alert-success alert-dismissible fade show mx-3 text-left" role="alert">
          <strong>Berhasil!</strong> Data user telah dihapus.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div></div>';
    }
    unset($_SESSION['notif']);
  }
?>

<!-- awal tabel user -->
<div class="row">
  <div class="col-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">User</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="myTable">
            <thead>
              <tr>
                <th class="align-middle">No</th>
                <th class="align-middle">Nama Lengkap</th>
                <th class="align-middle" width="30%">Alamat</th>
                <th class="align-middle">Level</th>
                <th class="align-middle">Username</th>
                <th class="align-middle">Password</th>
                <th class="align-middle">Opsi</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($users as $u) : ?>
                <tr>
                  <td class="align-middle"><?= $h++ ?></td>
                  <td class="align-middle"><?= $u['nama_user'] ?></td>
                  <td class="align-middle"><?= $u['alamat'] ?></td>
                  <td class="align-middle"><?= $u['level'] ?></td>
                  <td class="align-middle"><?= $u['username'] ?></td>
                  <td class="align-middle"><?= $u['password'] ?></td>
                  <td class="align-middle">
                    <button class="btn btn-sm btn-outline-warning" data-toggle="modal" data-target="#ubahUser<?=$u['id_user']?>">Edit</button>&nbsp;&nbsp;
                    <a href="./user_hapus.php?id=<?=$u['id_user']?>" class="btn btn-sm btn-outline-danger" onclick="deleteConfirm(event)">Hapus</a>
                  </td>
                </tr>
                <!-- modal User -->
                <div class="modal fade" id="ubahUser<?=$u['id_user']?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <form action="./user_update.php?id=<?=$u['id_user']?>" method="post">
                          <div class="form-group">
                            <label for="level">Level</label>
                            <select name="level" id="level" class="form-control bg-warning" required>
                              <?php foreach ($level as $lv) : ?>
                                <?php if ($lv == $u['level']) : ?>
                                <option value="<?=$lv?>" selected><?=$lv?></option>
                                <?php else : ?>
                                <option value="<?=$lv?>"><?=$lv?></option>
                                <?php endif ?>
                              <?php endforeach ?>
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="nama_user">Nama Lengkap</label>
                            <input type="text" class="form-control" name="nama_user" id="nama_user" value="<?=$u['nama_user']?>" required>
                          </div>
                          <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <textarea name="alamat" id="alamat" cols="30" rows="3" class="form-control" required><?= $u['alamat'] ?></textarea>
                          </div>
                          <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" class="form-control" name="username" id="username" value="<?=$u['username']?>" required>
                          </div>
                          <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" id="password" value="<?=$u['password']?>" required>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- akhir tabel user -->

<?php require('../layouts/footer_admin.php') ?>