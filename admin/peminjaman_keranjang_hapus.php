<?php 
  session_start();

  $id_barang = $_GET['id'];

  foreach ($_SESSION['keranjang_belanja'] as $key => $value) {
    if ($_SESSION['keranjang_belanja'][$key]['id_barang'] == $id_barang) {
      unset($_SESSION['keranjang_belanja'][$key]); 

      if (($key_id = array_search($id_barang, $_SESSION['id_barang'])) !== false) {
        unset($_SESSION['id_barang'][$key_id]);
      }

      $_SESSION['notif'] = 'barang-hapus';
    }
    
    header('location:peminjaman_tambah.php');
  }
?>