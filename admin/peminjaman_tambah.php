<?php 
  require('../layouts/header_admin.php'); 
  require('../config/connection.php');

  // untuk pagination
  $halaman = 9;
  $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
  $mulai = ($page > 1) ? ($page * $halaman) - $halaman : 0;
  $query = mysqli_query($conn, "SELECT * FROM barang LIMIT $mulai, $halaman");
  $sql = mysqli_query($conn, "SELECT * FROM barang");
  $total = mysqli_num_rows($sql);
  $pages = ceil($total/$halaman);

  // untuk search
  if (isset($_GET['search'])) {
    $query = mysqli_query($conn, "SELECT * FROM barang WHERE nama_barang LIKE '%".$_GET['search']."%'");
  }
  
  // $query = mysqli_query($conn, "SELECT * FROM barang");
  $barangs = [];
  while ($barang = mysqli_fetch_array($query)) {
    $barangs[] = $barang;
  }

  if (!isset($_SESSION['keranjang_belanja'])) {
    $_SESSION['keranjang_belanja'] = [];
    $_SESSION['id_barang'] = [];
  }
?>

<!-- heading content -->
<div class="d-sm-flex align-items-center justify-content-between mb-3">
  <h1 class="h3 mb-0 text-gray-800">Buat Peminjaman</h1>
  <?php 
    if (isset($_SESSION['notif'])) {
      if ($_SESSION['notif'] == 'barang-tambah'){
        echo '
          <div class="alert alert-warning alert-dismissible fade show text-left" role="alert">
            <strong>Barang berhasil ditambah!</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>';
      }
      if ($_SESSION['notif'] == 'barang-ada'){
        echo '
          <div class="alert alert-danger alert-dismissible fade show text-left" role="alert">
            <strong>Barang sudah ada di keranjang!</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>';
      }
      unset($_SESSION['notif']);
    }
  ?>
</div>

<!-- awal tombol next -->
<div class="row mb-3">
  <div class="col-12 text-right">
    <a href="peminjaman_tagihan.php" class="btn btn-primary">Tagihan &nbsp;<i class="fas fa-angle-right"></i></a>
  </div>
</div>
<!-- akhir tombol next -->

<div class="row">

  <!-- awal table barang -->
  <div class="col-md-6">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Pilih Barang</h6>
      </div>
      <div class="card-body">
        <!-- awal search form -->
        <div class="row mb-3">
          <div class="col">
            <form method="GET" action="" class="form-inline">
              <input class="form-control mr-sm-2" name="search" type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fas fa-search"></i></button>
            </form>
          </div>
        </div>
        <!-- akhir search form -->
        <div class="row">
          <?php foreach ($barangs as $b) : ?>
          <div class="col-4">
            <div class="card mb-3" style="width: 100%;">
              <div class="text-center">
                <img class="center-cropped" src="../images/<?= $b['foto_barang'] ?>" class="card-img-top" alt="<?= $b['nama_barang'] ?>">
              </div>
              <div class="card-body p-3">
                <h6 class="card-subtitle mb-2 text-muted"><?= $b['nama_barang'] ?></h6>
                <h5 class="card-title mb-3">Rp. <?= $b['harga_sewa'] ?></h5>
                <h6 class="card-subtitle mb-3 text-muted">Stok <?= $b['stok_awal']-$b['jmlh_rusak']-$b['jmlh_diambil'] ?></h6>
                
				<?php if (!in_array($b['id_barang'], isset($_SESSION['id_barang']) ? $_SESSION['id_barang'] : [])) : ?>
                  <a href="peminjaman_keranjang_tambah.php?id=<?= $b['id_barang'] ?>" class="btn btn-sm btn-outline-primary w-100">Sewa</a>
                <?php else : ?>
                  <a href="" class="btn btn-sm btn-outline-success w-100 disabled">Sewa</a>
                <?php endif ?>
              </div>
            </div>
          </div>
          <?php endforeach ?>
        </div>

        <!-- awal pagination -->
        <div class="row mt-3">
          <div class="col-12">
            <ul class="pagination justify-content-center">
              <?php if (isset($_GET['page'])) : ?>
                <?php if ($_GET['page'] <= 1) : ?>
                  <li class="page-item disabled"><a class="page-link" href="?page=<?= $_GET['page']-1 ?>">Previous</a></li>
                <?php else : ?>
                  <li class="page-item"><a class="page-link" href="?page=<?= $_GET['page']-1 ?>">Previous</a></li>
                <?php endif; ?>
              <?php endif; ?>
              <?php for ($i=1; $i <= $pages; $i++) : ?>
                <li class="page-item"><a class="page-link" href="?page=<?= $i ?>"><?= $i ?></a></li>
              <?php endfor; ?>
              <?php if (isset($_GET['page'])) : ?>
                <?php if ($_GET['page'] >= $pages) : ?>
                  <li class="page-item disabled"><a class="page-link" href="?page=<?= $_GET['page']+1 ?>">Next</a></li>
                <?php else : ?>
                  <li class="page-item"><a class="page-link" href="?page=<?= $_GET['page']+1 ?>">Next</a></li>
                <?php endif; ?>
              <?php endif; ?>
            </ul>
          </div>
        </div>
        <!-- akhir pagination -->
      </div>
    </div>
  </div>
  <!-- akhir table barang -->

  <!-- awal keranjang -->
  <div class="col-md 6">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Keranjang Belanja</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>Produk</th>
                <th>Jumlah</th>
                <th>Harga/Unit</th>
                <th>Harga</th>
                <th>Batal</th>
              </tr>
            </thead>
            <tbody>
              <?php if (isset($_SESSION['keranjang_belanja'])) : ?>
                <?php if ($_SESSION['keranjang_belanja'] == null) : ?>
                  <tr class="text-center">
                    <td colspan="5">Tidak ada barang!</td>
                  </tr>
                <?php else : ?>
                  <?php 
                    $total_harga = 0;
                    foreach ($_SESSION['keranjang_belanja'] as $kb) : 
                      $id = $kb['id_barang'];
                      $query = mysqli_query($conn, "SELECT * FROM barang WHERE id_barang='$id'");

                      $rows = [];
                      while ($row = mysqli_fetch_assoc($query)) {
                        $rows[] = $row;
                      }
                  ?>
                  <tr class="align-middle">
                    <td class="align-middle"><?= $rows[0]['nama_barang'] ?></td>
                    <td width="20%" class="text-center align-middle">
                      <input type="text" name="jmlh_barang" class="border-0 w-25 text-center" value="<?= $kb['qty'] ?>" readonly><br>
                      <a href="./peminjaman_qty_kurang.php?id=<?= $kb['id_barang'] ?>" class="btn btn-sm btn-outline-primary btn-inc"><i class="fas fa-minus"></i></a>
                      <a href="./peminjaman_qty_tambah.php?id=<?= $kb['id_barang'] ?>&max=<?= $rows[0]['stok_awal']-$rows[0]['jmlh_rusak']-$rows[0]['jmlh_diambil'] ?>" class="btn btn-sm btn-primary btn-inc"><i class="fas fa-plus"></i></a>
                    </td>
                    <td class="align-middle"><?= $rows[0]['harga_sewa'] ?></td>
                    <td class="align-middle"><?= $rows[0]['harga_sewa']*$kb['qty'] ?></td>
                    <td class="align-middle text-center">
                      <a href="./peminjaman_keranjang_hapus.php?id=<?= $kb['id_barang'] ?>" class="btn btn-sm btn-danger">X</a>
                    </td>
                  </tr>
                    <?php $total_harga += $rows[0]['harga_sewa']*$kb['qty']; ?>
                  <?php endforeach ?>
                  <tr>
                    <th colspan="5">
                      <div class="row">
                        <div class="col-6">Total</div>
                        <div class="col-6 text-right">Rp. <?= $total_harga ?></div>
                      </div>
                    </th>
                  </tr>
                <?php endif ?>
              <?php endif ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- akhir keranjang -->

</div>

<?php require('../layouts/footer_admin.php') ?>