<?php 
  require('../layouts/header_admin.php'); 
  require('../config/connection.php');

  $select_supplier = mysqli_query($conn, "SELECT * FROM supplier");
  $suppliers = [];
  while ($supplier = mysqli_fetch_array($select_supplier)) {
    $suppliers[] = $supplier;
  }

  $i = 1;
?>

<!-- heading content -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Daftar Supplier</h1>
  <a href="./report/supplier.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Download Report</a>
</div>

<!-- awal tabel supplier -->
<div class="row">
  <div class="col-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Supplier</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="myTable">
            <thead>
              <tr>
                <th>No</th>
                <th>Id Supplier</th>
                <th>Nama Lengkap</th>
                <th>Alamat</th>
                <th>No. HP</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($suppliers as $s) : ?>
              <tr>
                <td><?= $i++ ?></td>
                <td><?= $s['id_supplier'] ?></td>
                <td><?= $s['nama_supplier'] ?></td>
                <td><?= $s['alamat'] ?></td>
                <td><?= $s['no_hp'] ?></td>
              </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- akhir tabel supplier -->

<?php require('../layouts/footer_admin.php') ?>