<?php 
  require('../layouts/header_admin.php');
  require('../config/connection.php');
  
  $select_peminjaman = mysqli_query($conn, "SELECT * FROM peminjaman WHERE status_barang='1'");
  $peminjamans = [];
  while ($peminjaman = mysqli_fetch_array($select_peminjaman)) {
    $peminjamans[] = $peminjaman;
  }
  
  $i = 1;

  unset($_SESSION['barang_rusak']);
  unset($_SESSION['id_barang']);
?>

<!-- heading content -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Daftar Pengembalian</h1>
</div>

<?php 
  if (isset($_SESSION['notif'])) {
    if ($_SESSION['notif']=='pengembalian-success') {
      echo '
      <div class="row"><div class="col-12">  
        <div class="alert alert-success alert-dismissible fade show mx-3 text-left" role="alert">
          <strong>Sukses!</strong> pengembalian barang telah berhasil dilakukan.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div></div>';
    }
    unset($_SESSION['notif']);
  }
?>

<!-- awal table peminjaman -->
<div class="row">
  <div class="col-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Pengembalian</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table id="table-pengembalian" class="table table-bordered">
            <thead>
              <tr>
                <th>Id Peminjaman</th>
                <th>Id Member</th>
                <th>Metode Bayar</th>
                <th>Total Harga</th>
                <th>Tgl Sewa</th>
                <th>Lama Sewa</th>
                <th>Jaminan</th>
                <th>Status Barang</th>
                <th>Denda Rusak</th>
                <th>Opsi</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($peminjamans as $p) : ?>
                <tr>
                  <td class="align-middle"><?= $p['id_peminjaman'] ?></td>
                  <td class="align-middle"><?= $p['id_member'] ?></td>
                  <td class="align-middle"><?= $p['metode_bayar'] ?></td>
                  <td class="align-middle">Rp. <?= $p['total_harga'] ?></td>
                  <td class="align-middle"><?= $p['tgl_sewa'] ?></td>
                  <td class="align-middle"><?= $p['lama_sewa'] ?></td>
                  <td class="align-middle">
                    <button type="button" class="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#fotoJaminan<?=$i?>">Lihat foto</button>
                  </td>
                  <td class="align-middle">
                    <?php 
                      if ($p['status_kembali'] == 0) {
                        echo "Belum dikembalikan";
                      } else {
                        echo "Sudah dikembalikan";
                      }
                    ?>
                  </td>
                  <td class="align-middle"><?= $p['denda_rusak'] ?></td>
                  <td class="align-middle">
                    <?php if ($p['status_kembali'] == 0) : ?>
                      <a href="./pengembalian_konfirmasi.php?id=<?=$p['id_peminjaman']?>" class="btn btn-outline-primary">Konfirmasi</a>
                    <?php else : ?>
                      <a href="" class="btn btn-outline-success disabled">Konfirmasi</a>
                    <?php endif ?>
                  </td>
                </tr>

                <!-- modal foto identitas -->
                <div class="modal fade" id="fotoJaminan<?=$i?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><?= $p['id_member'] ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <img class="img-thumbnail mx-auto d-block" src="../file/foto-identitas/<?= $p['jaminan'] ?>" width="70%">
                      </div>
                    </div>
                  </div>
                </div>
                <?php $i++ ?>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- akhir table peminjaman -->

<?php require('../layouts/footer_admin.php') ?>