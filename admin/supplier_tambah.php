<?php require('../layouts/header_admin.php') ?>

<!-- heading content -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Tambah Supplier</h1>
  <?php 
    if (isset($_SESSION['notif'])) {
      if ($_SESSION['notif'] == 'supplier-add') {
        echo '
          <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Selamat!</strong> Data Supplier berhasil ditambahkan.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        ';
      }
      unset($_SESSION['notif']); 
    } 
  ?>
</div>

<!-- awal form supplier -->
<div class="row">
  <div class="col-md-6">
    <div class="card shadow mb-4">
      <div class="card-body">
        <form action="supplier_save.php" method="post">
          <div class="form-group">
            <label for="nama_supplier">Nama Lengkap</label>
            <input type="text" name="nama_supplier" id="nama_supplier" class="form-control" required>
          </div>
          <div class="form-group">
            <label for="no_hp">No. HP</label>
            <input type="number" name="no_hp" id="no_hp" class="form-control" required>
          </div>
          <div class="form-group">
            <label for="alamat">Alamat</label>
            <textarea name="alamat" id="alamat" class="form-control" cols="30" rows="3" required></textarea>
          </div>
          <br>
          <div class="text-right">
            <button type="reset" class="btn btn-white">Reset</button>&nbsp;&nbsp;
            <button type="submit" class="btn btn-primary">Tambah</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- akhir form supplier -->

<?php require('../layouts/footer_admin.php') ?>