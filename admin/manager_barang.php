<?php 
  require('../layouts/header_admin.php');
  require('../config/connection.php');

  $select_barang = mysqli_query($conn, "SELECT * FROM barang");
  $barangs = [];
  while ($barang = mysqli_fetch_array($select_barang)) {
    $barangs[] = $barang;
  }

  $i = 1;
?>

<!-- heading content -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Daftar Barang</h1>
  <button data-toggle="modal" data-target="#modalReport" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Download Report</button>
</div>

<!-- jika range tgl kosong -->
<?php 
  if (isset($_SESSION['notif'])) {
    if ($_SESSION['notif']=='tgl-error') {
      echo '
      <div class="row"><div class="col-12">  
        <div class="alert alert-danger alert-dismissible fade show mx-3 text-left" role="alert">
          <strong>Gagal!</strong> Tanggal tidak boleh kosong.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div></div>';
    }
    unset($_SESSION['notif']); 
  }
?>

<!-- awal tabel Barang -->
<div class="row">
  <div class="col-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Barang Outdor</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered nowrap" id="dataTable">
            <thead>
              <tr>
                <th rowspan="2" class="align-middle">No</th>
                <th rowspan="2" class="align-middle">Id Supplier</th>
                <th rowspan="2" class="align-middle">Nama Barang</th>
                <th rowspan="2" class="align-middle">Foto</th>
                <th rowspan="2" class="align-middle">Deskripsi</th>
                <th rowspan="2" class="align-middle">Merek</th>
                <th rowspan="2" class="align-middle">Jenis</th>
                <th rowspan="2" class="align-middle">Harga Barang</th>
                <th rowspan="2" class="align-middle">Harga Sewa</th>
                <th colspan="3" class="text-center">Stok</th>
                <th rowspan="2" class="align-middle">Tanggal Masuk</th>
              </tr>
              <tr>
                <th>Awal</th>
                <th>Di Ambil</th>
                <th>Rusak</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($barangs as $b) : ?>
              <tr>
                <td><?= $i++ ?></td>
                <td class="text-center"><?= $b['id_supplier'] ?></td>
                <td><?= $b['nama_barang'] ?></td>
                <td><button class="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#fotoBarang<?=$b['id_barang']?>">Lihat foto...</button></td>
                <td><?= $b['deskripsi'] ?></td>
                <td><?= $b['merek'] ?></td>
                <td><?= $b['jenis'] ?></td>
                <td><?= $b['harga_barang'] ?></td>
                <td><?= $b['harga_sewa'] ?></td>
                <td><?= $b['stok_awal'] ?></td>
                <td><?= $b['jmlh_diambil'] ?></td>
                <td><?= $b['jmlh_rusak'] ?></td>
                <td><?= $b['tgl_barang_masuk'] ?></td>
              </tr>

              <!-- modal foto barang -->
              <div class="modal fade" id="fotoBarang<?=$b['id_barang']?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel"><?= $b['nama_barang'] ?></h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <img class="mx-auto d-block" src="../images/<?= $b['foto_barang'] ?>" width="70%">
                    </div>
                  </div>
                </div>
              </div>
              <!-- modal foto barang -->
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- akhir tabel alat -->

<!-- modal report -->
<div class="modal fade" id="modalReport" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Download Report</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="./report/barang.php" method="post">
        <div class="modal-body">
          <div class="form-check">
            <input class="form-check-input" type="radio" name="jenisReport" id="jenisReport1" value="all" checked>
            <label class="form-check-label" for="jenisReport1">
              Semua data
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="jenisReport" id="jenisReport2" value="date">
            <label class="form-check-label" for="jenisReport2">
              Sesuai Tanggal
            </label>
          </div>
          <div class="tanggalReport" style="display:none;">
            <br>
            <div class="form-group">
              <label for="tgl_awal">Tanggal Awal</label>
              <input type="date" name="tgl_awal" id="tgl_awal" class="form-control">
            </div>
            <div class="form-group">
              <label for="tgl_akhir">Tanggal Akhir</label>
              <input type="date" name="tgl_akhir" id="tgl_akhir" class="form-control">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Download</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- akhir modal report -->

<?php require('../layouts/footer_admin.php') ?>