<?php 
  require('../layouts/header_admin.php');
  require('../config/connection.php');

  $select_sum_transaksi = mysqli_query($conn, "SELECT SUM(total_pembayaran) AS total_sum FROM transaksi");
  $transaksi = mysqli_fetch_array($select_sum_transaksi);

  $select_count_brg = mysqli_query($conn, "SELECT SUM(jmlh_diambil) AS count_brg, SUM(jmlh_rusak) as count_rusak FROM barang");
  $brg = mysqli_fetch_array($select_count_brg);

  $select_member = mysqli_query($conn, "SELECT COUNT(id_member) as count_member FROM member");
  $mbr = mysqli_fetch_array($select_member);

  $yearNow = date('Y');
  $count_jan = [];
  for ($i = 1; $i <= 12; $i++) {
    $select_january = mysqli_query($conn, "SELECT COUNT(*) as jmlh FROM peminjaman WHERE Year(tgl_sewa)='$yearNow' AND Month(tgl_sewa)='$i'");
    $count_jan[] = mysqli_fetch_assoc($select_january);
  }

  $select_max_count = mysqli_query($conn, "SELECT COUNT(*) AS max_count FROM peminjaman GROUP BY Month(tgl_sewa) ORDER BY COUNT(*) DESC LIMIT 1");
  $max_count = mysqli_fetch_assoc($select_max_count);
?>

<div class="row">
  <!-- Jumlah Barang -->
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-info shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Jumlah Barang Disewa</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $brg['count_brg'] ?></div>
          </div>
          <div class="col-auto">
            <i class="fas fa-shopping-cart fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Jumlah Barang -->
  <!-- Jumlah Barang -->
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-warning shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Jumlah Barang Rusak</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $brg['count_rusak'] ?></div>
          </div>
          <div class="col-auto">
            <i class="fas fa-box fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Jumlah Barang -->
  <!-- Jumlah Member -->
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-secondary shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-secondary text-uppercase mb-1">Jumlah Member</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $mbr['count_member'] ?></div>
          </div>
          <div class="col-auto">
            <i class="fas fa-user fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Jumlah Member -->
  <!-- total pemasukan -->
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Pemasukan</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">Rp. <?= $transaksi['total_sum'] ?></div>
          </div>
          <div class="col-auto">
            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- total pemasukan -->
</div>
<div class="row">
  <div class="col-md-12">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Statistik Peminjaman Perbulan</h6>
      </div>
      <div class="card-body text-center">
        <canvas id="myBarChart" height="100px"></canvas>
      </div>
    </div>
  </div>
</div>

</div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>2019 &copy; Sangga Buana Outdor</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Yakin akan keluar?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Pilih "Logout" untuk keluar dari akun.</div>
        <div class="modal-footer">
          <button class="btn btn-white" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="../admin_logout.php">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="../js/jquery.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../vendor/chart.js/Chart.min.js"></script>
  <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>
  <script src="../vendor/chart.js/Chart.min.js"></script>
  <script src="../js/btn-tagihan.js"></script>
  <script src="../js/script-admin.js"></script>

  <script>
    var ctx = document.getElementById("myBarChart");
    var myBarChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
        datasets: [{
          label: "Peminjaman Barang",
          data: [<?php foreach ($count_jan as $cj) { echo '"' . $cj['jmlh'] . '",';}?>],
          backgroundColor: 'rgba(80, 145, 195, 0.7)',
          hoverBackgroundColor: "rgba(80, 145, 195)",
          borderColor: 'rgba(80, 145, 195)',
        }],
      },
      barValueSpacing: 20,
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              max: <?= $max_count['max_count'] ?>,
            }
          }],
          xAxes: [{
            gridLines: {
              color: '#fff'
            }
          }],
        }
      }
    });
  </script>

</body>

</html>