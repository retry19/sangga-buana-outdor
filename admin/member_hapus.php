<?php 
  require('../config/connection.php');
  session_start();

  $id_member = $_GET['id'];
  
  $select_member = mysqli_query($conn, "SELECT foto_identitas FROM member WHERE id_member='$id_member'");
  $member = mysqli_fetch_array($select_member);

  $select_peminjaman = mysqli_query($conn, "SELECT detail_peminjaman.id_barang, detail_peminjaman.qty 
                                            FROM peminjaman
                                            JOIN detail_peminjaman ON peminjaman.id_peminjaman=detail_peminjaman.id_peminjaman 
                                            WHERE peminjaman.id_member='$id_member' AND peminjaman.status_kembali=0");
  $peminjamans = [];
  while ($peminjaman = mysqli_fetch_array($select_peminjaman)) {
    $peminjamans[] = $peminjaman;
  }

  // jika member masih meminjam barang
  if (mysqli_num_rows($select_peminjaman)) {
    foreach ($peminjamans as $p) {
      $idbarang = $p['id_barang'];
      $qty = $p['qty'];

      try {
        mysqli_query($conn, "UPDATE barang SET jmlh_diambil=jmlh_diambil-'$qty' WHERE id_barang='$idbarang'");
      } catch (\Exception $e) {
        echo $e;
      }
    }
  }

  try {
    mysqli_query($conn, "DELETE FROM member WHERE id_member='$id_member'");
    unlink('../file/foto-identitas/'.$member['foto_identitas']);
  } catch (\Exception $e) {
    echo $e;
  }

  $_SESSION['notif'] = "member-delete";
  header('location:member_index.php');
?>