<?php 
  require('../layouts/header_admin.php');
  require('../config/connection.php');

  $id_peminjaman = $_GET['id'];

  $select_peminjaman = mysqli_query($conn, "SELECT p.tgl_sewa, p.lama_sewa, m.nama FROM peminjaman p
                                            INNER JOIN member m ON p.id_member=m.id_member
                                            WHERE p.id_peminjaman='$id_peminjaman'");
  $p = [];
  while ($peminjaman = mysqli_fetch_array($select_peminjaman)) {
    $p[] = $peminjaman;
  }

  $tgl_sewa = $p[0]['tgl_sewa'];
  $tgl_pengembalian = date('Y-m-d', strtotime('+'.$p[0]['lama_sewa'].' days', strtotime($tgl_sewa)));
  $tgl_now = date('Y-m-d');

  $selisih_tgl = abs(strtotime($tgl_now) - strtotime($tgl_pengembalian)) / (60 * 60 * 24);
  
  if (strtotime($tgl_pengembalian) >= strtotime($tgl_now)) {
    $denda_telat = 0;
  } else {
    $denda_telat = $selisih_tgl * 10000;
  }

  $select_barang = mysqli_query($conn, "SELECT dp.qty, b.id_barang, b.nama_barang, b.foto_barang, b.harga_barang FROM detail_peminjaman dp
                                        INNER JOIN barang b ON dp.id_barang=b.id_barang
                                        WHERE dp.id_peminjaman='$id_peminjaman' ORDER BY id_barang ASC") or die(mysqli_error($conn));
  $jml_barang = mysqli_num_rows($select_barang);
  $barangs = [];
  while ($barang = mysqli_fetch_array($select_barang)) {
    $barangs[] = $barang;
  }

  $i = 1;

  if (!isset($_SESSION['barang_rusak'])) {
    $_SESSION['barang_rusak'] = [];
    $_SESSION['id_barang'] = [];
    // $denda_rusak = 0;
  }

  // jika barang rusak
  if (isset($_GET['rusak'])) 
  {
    $idbrg = $_GET['id_barang'];
    $ii = $_GET['i'];

    if (!isset($_SESSION['barang_rusak'][$idbrg])) {
      $_SESSION['barang_rusak'][$idbrg] = [];
    }

    switch ($_GET['rusak']) {
      case '3':
        array_push($_SESSION['barang_rusak'][$idbrg], $ii.'_'.$idbrg.'_3');
        array_push($_SESSION['id_barang'], $ii.'_'.$_GET['id_barang']);
        break;
      case '2':
        array_push($_SESSION['barang_rusak'][$idbrg], $ii.'_'.$idbrg.'_2');
        array_push($_SESSION['id_barang'], $ii.'_'.$_GET['id_barang']);
        break;
      case '1':
        array_push($_SESSION['barang_rusak'][$idbrg], $ii.'_'.$idbrg.'_1');
        array_push($_SESSION['id_barang'], $ii.'_'.$_GET['id_barang']);
        break;
      case '0':
        array_push($_SESSION['barang_rusak'][$idbrg], $ii.'_'.$idbrg.'_0');
        array_push($_SESSION['id_barang'], $ii.'_'.$_GET['id_barang']);
        break;
    }
  }

  $denda_rusak = 0; 
  $no = 1;
?>

<!-- heading content -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Detail Pengembalian</h1>
</div>

<?php 
  if (isset($_SESSION['notif'])) {
    if ($_SESSION['notif']=='kondisi-error') {
      echo '
      <div class="row"><div class="col-12">  
        <div class="alert alert-danger alert-dismissible fade show mx-3 text-left" role="alert">
          <strong>Gagal!</strong> Kondisi barang harus dipilih.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div></div>';
    }
    unset($_SESSION['notif']);
  }
?>

<div class="row mb-3">
  <div class="col-6"><a href="./pengembalian_index.php" class="btn btn-outline-secondary"><i class="fas fa-chevron-left"></i>&nbsp; Kembali</a></div>
  <div class="col-6 text-right">
    <button type="button" onclick="document.getElementById('formPengembalian').submit()" class="btn btn-primary">Selesai &nbsp;<i class="fas fa-angle-right"></i></button>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Peminjaman</h6>
      </div>
      <div class="card-body">
        <div class="form-group row">
          <label class="col-sm-4 col-form-label text-right">Id Peminjaman</label>
          <div class="col-sm-8">
            <input type="text" name="id_peminjaman" class="form-control-plaintext border pl-3" value="<?=$id_peminjaman?>" readonly>
          </div>
        </div>
        <div class="form-group row mt-4">
          <label class="col-sm-4 col-form-label text-right">Nama Member</label>
          <div class="col-sm-8">
            <input type="text" name="nama" class="form-control-plaintext border pl-3" value="<?=$p[0]['nama']?>" readonly>
          </div>
        </div>
        <div class="form-group row mt-5">
          <label class="col-sm-4 col-form-label text-right">Tanggal Sewa</label>
          <div class="col-sm-8">
            <input type="text" name="tgl_sewa" class="form-control-plaintext border pl-3" value="<?=date('d-m-Y',strtotime($tgl_sewa))?>" readonly>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-4 col-form-label text-right">Tanggal Kembali</label>
          <div class="col-sm-8">
            <input type="text" name="tgl_pengembalian" class="form-control-plaintext border pl-3" value="<?=date('d-m-Y',strtotime($tgl_pengembalian))?>" readonly>
          </div>
        </div>
        <div class="form-group row mt-4">
          <label class="col-sm-4 col-form-label text-right">Denda Telat</label>
          <div class="col-sm-8">
            <input type="text" name="denda_telat_rupiah" class="form-control" value="Rp. <?=$denda_telat?>" readonly>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-6">
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Barang</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-hover">
            <thead>
              <tr class="text-center">
                <th>No</th>
                <th>Nama Barang</th>
                <th>Harga Barang</th>
                <th>Kondisi</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($barangs as $b) : ?>
                <?php $id_barang = $b['id_barang'] ?>
                <?php if ($b['qty'] > 1) : ?>
                  <?php for ($a=0; $a<2; $a++) : ?>
                    <tr>
                      <td class="text-center"><?= $i ?></td>
                      <td><?= $b['nama_barang'] ?></td>
                      <td class="text-center">Rp. <?= $b['harga_barang'] ?></td>
                      <td class="text-center">
                        <?php if (in_array($i.'_'.$b['id_barang'], $_SESSION['id_barang'])) : ?>
                          <?php 
                            foreach ($_SESSION['barang_rusak'][$id_barang] as $br) {
                              if (substr($br, 0, -2) == $i.'_'.$b['id_barang']) {
                                if (substr($br, -1) == 3) {
                                  $denda_rusak = $denda_rusak + (1  * $b['harga_barang']);
                                  echo "Hilang";
                                } elseif (substr($br, -1) == 2) {
                                  $denda_rusak = $denda_rusak + (0.5  * $b['harga_barang']);
                                  echo "Rusak berat";
                                } elseif (substr($br, -1) == 1) {
                                  $denda_rusak = $denda_rusak + (0.25  * $b['harga_barang']);
                                  echo "Rusak ringan";
                                } elseif (substr($br, -1) == 0) {
                                  echo "Baik";
                                }
                              }
                            }
                          ?>
                        <?php else : ?>
                        <a href="?id=<?=$id_peminjaman?>&id_barang=<?=$b['id_barang']?>&i=<?=$i?>&rusak=0" data-toggle="tooltip" title="Baik" class="btn btn-sm btn-outline-primary">B</a>&nbsp;
                        <a href="?id=<?=$id_peminjaman?>&id_barang=<?=$b['id_barang']?>&i=<?=$i?>&rusak=1" data-toggle="tooltip" title="Rusak ringan" class="btn btn-sm btn-outline-danger">RR</a>&nbsp;
                        <a href="?id=<?=$id_peminjaman?>&id_barang=<?=$b['id_barang']?>&i=<?=$i?>&rusak=2" data-toggle="tooltip" title="Rusak berat" class="btn btn-sm btn-outline-danger">RB</a>&nbsp;
                        <a href="?id=<?=$id_peminjaman?>&id_barang=<?=$b['id_barang']?>&i=<?=$i?>&rusak=3" data-toggle="tooltip" title="Hilang" class="btn btn-sm btn-outline-danger">HL</a>&nbsp;
                        <?php endif ?>
                      </td>
                    </tr>
                  <?php $i++; endfor ?>
                <?php else : ?>
                  <tr>
                    <td class="text-center"><?= $i ?></td>
                    <td><?= $b['nama_barang'] ?></td>
                      <td class="text-center">Rp. <?= $b['harga_barang'] ?></td>
                    <td class="text-center">
                      <?php if (in_array($i.'_'.$b['id_barang'], $_SESSION['id_barang'])) : ?>
                        <?php 
                          foreach ($_SESSION['barang_rusak'][$id_barang] as $br) {
                            if (substr($br, 0, -2) == $i.'_'.$b['id_barang']) {
                              if (substr($br, -1) == 3) {
                                $denda_rusak = $denda_rusak + (1  * $b['harga_barang']);
                                echo "Hilang";
                              } elseif (substr($br, -1) == 2) {
                                $denda_rusak = $denda_rusak + (0.5  * $b['harga_barang']);
                                echo "Rusak berat";
                              } elseif (substr($br, -1) == 1) {
                                $denda_rusak = $denda_rusak + (0.25  * $b['harga_barang']);
                                echo "Rusak ringan";
                              } elseif (substr($br, -1) == 0) {
                                echo "Baik";
                              }
                            }
                          }
                        ?>
                      <?php else : ?>
                      <a href="?id=<?=$id_peminjaman?>&id_barang=<?=$b['id_barang']?>&i=<?=$i?>&rusak=0" data-toggle="tooltip" title="Baik" class="btn btn-sm btn-outline-primary">B</a>&nbsp;
                      <a href="?id=<?=$id_peminjaman?>&id_barang=<?=$b['id_barang']?>&i=<?=$i?>&rusak=1" data-toggle="tooltip" title="Rusak ringan" class="btn btn-sm btn-outline-danger">RR</a>&nbsp;
                      <a href="?id=<?=$id_peminjaman?>&id_barang=<?=$b['id_barang']?>&i=<?=$i?>&rusak=2" data-toggle="tooltip" title="Rusak berat" class="btn btn-sm btn-outline-danger">RB</a>&nbsp;
                      <a href="?id=<?=$id_peminjaman?>&id_barang=<?=$b['id_barang']?>&i=<?=$i?>&rusak=3" data-toggle="tooltip" title="Hilang" class="btn btn-sm btn-outline-danger">HL</a>&nbsp;
                      <?php endif ?>
                    </td>
                  </tr>
                <?php $i++; endif ?>
                <?php $no++ ?>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
        <div class="form-group row mt-3">
          <label class="col-sm-4 col-form-label text-right">Denda Kerusakan</label>
          <div class="col-sm-8">
            <input type="text" name="denda_rusak_rupiah" class="form-control" value="Rp. <?=$denda_rusak?>" readonly>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- form -->
<form id="formPengembalian" action="./pengembalian_submit.php" method="post">
  <input type="hidden" name="id_peminjaman" value="<?= $id_peminjaman ?>">
  <input type="hidden" name="jml_barang" value="<?= $jml_barang ?>">
  <input type="hidden" name="denda_telat" value="<?= $denda_telat; ?>">
  <input type="hidden" name="denda_rusak" value="<?= $denda_rusak; ?>">
</form>

<?php require('../layouts/footer_admin.php') ?>