$('#btn-plus-hari').click(function(){
  lamaSewa = $('#tagihan-lama-sewa').val();
  totalHarga = $('#tagihan-total-harga').val();

  if (lamaSewa < 7) {
    lamaSewa++;
  
    $('#tagihan-lama-sewa').val(lamaSewa);
    lamaSewaPlus = $('#tagihan-lama-sewa').val();
  
    awalHarga = totalHarga / (lamaSewaPlus - 1);
  
    totalHargaPlus = awalHarga * lamaSewaPlus;
  
    $('#tagihan-total-harga').val(totalHargaPlus);
  }
});

$('#btn-minus-hari').click(function(){
  lamaSewa = $('#tagihan-lama-sewa').val();
  totalHarga = $('#tagihan-total-harga').val();

  if (lamaSewa > 1) {
    awalHarga = totalHarga / lamaSewa;
  
    lamaSewa--;
  
    totalHargaMinus = awalHarga * lamaSewa; 
  
    $('#tagihan-lama-sewa').val(lamaSewa);
    $('#tagihan-total-harga').val(totalHargaMinus);
  }
});