$(document).ready(function() {
  $('#dataTable').DataTable({
    "scrollX": true
  });

  // table penyewaan (gudang)
  $('#table-gudang').DataTable({
    "scrollX": true,
    "aaSorting": [[7, "asc"]]
  });

  // table penyewaan (keuangan)
  $('#table-keuangan').DataTable({
    "scrollX": true,
    "aaSorting": [[12, "asc"]]
  });

  // table pengembalian (admin)
  $('#table-pengembalian').DataTable({
    "scrollX": true,
    "aaSorting": [[7, "asc"]]
  });

  $('#myTable').DataTable();

  $('#member-baru').change(function(){
    $('#data-member').toggle();
    $('#id_member').val('');
    $('#id_member').attr('disabled', this.checked);
  });

  // tooltip
  $('[data-toggle="tooltip"]').tooltip();

  // radiobutton download report
  $('input[type="radio"]').click(function () {
    if ($(this).attr("value") == "all") {
        $(".tanggalReport").hide('slow');
    }
    if ($(this).attr("value") == "date") {
        $(".tanggalReport").show('slow');
    }
  });
  $('input[type="radio"]').trigger('click');  // trigger the event
});

function deleteConfirm(ev){
  ev.preventDefault();
  var url = ev.currentTarget.getAttribute('href');
  console.log(url);
  Swal.fire({
    title: 'Hapus?',
    text: 'data akan terhapus!',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Hapus!'
  }).then((result) => {
    if (result.value){
      window.location = url;
    }
  });
}

function btnDoubleRedirect() {
  window.open('transaksi_invoice.php', '_blank');
  window.location = 'transaksi_submit.php';
}

$('#uang_bayar').on('change keyup', function () {
  let uangBayar = $('#uang_bayar').val();
  let totalHarga = $('#total_harga').val();
  let uangKembali = uangBayar - totalHarga;

  $('#uang_kembali').val(uangKembali);

  $('#transaksi_submit').prop('disabled', true);
  if (uangKembali >= 0) {
    $('#transaksi_submit').prop('disabled', false);
  }
})