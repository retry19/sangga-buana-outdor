<?php 
  session_start();

  // apakah sudah login
  if (!isset($_SESSION['id_member'])) {
    header('location:login_index.php?notif=error-login');die;
  }

  require('config/connection.php');

  $id_member = $_SESSION['id_member'];
  $jaminan = $_SESSION['foto_identitas'];
  $dateNow = date('Y-m-d');

  // apakah masih terdapat tagihan yang belum memiliki metode pembayaran
  $select_peminjaman = mysqli_query($conn, "SELECT * FROM peminjaman WHERE id_member='$id_member' AND metode_bayar IS NULL");
  if (mysqli_num_rows($select_peminjaman)) {
    $_SESSION['notif'] = 'tagihan-error';
    header('location:keranjang_index.php');die;
  }

  $select_tunai = mysqli_query($conn, "SELECT t.status_pembayaran FROM peminjaman p INNER JOIN transaksi t ON p.id_peminjaman=t.id_peminjaman WHERE p.id_member='$id_member' AND p.metode_bayar='Tunai' AND t.status_pembayaran=0");
  if (mysqli_num_rows($select_tunai)) {
    $_SESSION['notif'] = 'tunai-not-null';
    header('location:keranjang_index.php');die;
  }

  // apakah masih terdapat transaksi yang belum tf
  $select_tranfer = mysqli_query($conn, "SELECT t.status_pembayaran FROM peminjaman p INNER JOIN transaksi t ON p.id_peminjaman=t.id_peminjaman WHERE p.id_member='$id_member' AND p.metode_bayar='Transfer' AND t.status_pembayaran=0");
  if (mysqli_num_rows($select_tranfer)) {
    $_SESSION['notif'] = 'transfer-not-null';
    header('location:keranjang_index.php');die;
  }

  $total_harga = $_POST['total_harga'];
  $total_harga_sewa = $_POST['total_harga']*$_SESSION['date']['lama_sewa'];

  $select_id_p = mysqli_query($conn, "SELECT RIGHT(MAX(id_peminjaman),3) AS last_id FROM peminjaman");
  $last_id = mysqli_fetch_array($select_id_p);
  
  if (mysqli_num_rows($select_id_p)) {
    $id_peminjaman = 'P'.date('ym').sprintf("%03s",$last_id['last_id']+1);
  } else {
    $id_peminjaman = 'P'.date('ym').sprintf("%03s","001");
  }

  // memasukan data peminjaman ke db peminjaman
  $dateTglAwal = date('Y-m-d',strtotime($_SESSION['date']['tgl_awal']));
  $dateLamaSewa = $_SESSION['date']['lama_sewa'];
  $add_peminjaman = mysqli_query($conn, "INSERT INTO peminjaman VALUES('$id_peminjaman','$id_member',NULL,'','$total_harga_sewa','$dateTglAwal','$dateLamaSewa','$jaminan','',NULL)");

  // perulangan sejumlah barang di keranjang belanja
  foreach ($_SESSION['keranjang_belanja'] as $key => $value) {
    $id_barang = $_SESSION['keranjang_belanja'][$key]['id_barang'];
    $qty = $_SESSION['keranjang_belanja'][$key]['qty'];

    // memanggil detail barang sesuai id barang yang di sewa
    $query = mysqli_query($conn, "SELECT * FROM barang WHERE id_barang='$id_barang'");
    $barang = [];
    while ($b = mysqli_fetch_assoc($query)) {
      $barang[] = $b;
    }
    
    $harga_barang = $barang[0]['harga_sewa']*$_SESSION['keranjang_belanja'][$key]['qty'];
    $total_harga += $harga_barang;

    // memasukan setiap data barang yang berada di keranjang belanja ke detail_peminjaman
    try {
      $add_detail_peminjaman = mysqli_query($conn, "INSERT INTO detail_peminjaman VALUES('','$id_peminjaman','$id_barang','$qty','$harga_barang',NULL)");
      $up_barang = mysqli_query($conn, "UPDATE barang SET jmlh_diambil=jmlh_diambil+'$qty' WHERE id_barang='$id_barang'");
    } catch (\Exception $e) {
      echo $e;
    }
  }

  unset($_SESSION['keranjang_belanja']);
  header('location:tagihan_index.php');
?>