<?php 
  session_start();
  require('config/connection.php');

  if ($_POST['tgl_sewa'] == null) {
    $_SESSION['notif'] = 'tgl-empty';
    header('location:tagihan_index.php');die;
  }

  $id_peminjaman = $_POST['id_peminjaman'];
  $total_harga = $_POST['total_harga'];
  $tgl_sewa = date('Y-m-d', strtotime($_POST['tgl_sewa']));
  $lama_sewa = $_POST['lama_sewa'];

  $id_transaksi = 'T'.substr($id_peminjaman, 1, 7);

  if (isset($_POST['submit-tunai'])) {
    try {
      $up_peminjaman = mysqli_query($conn, "UPDATE peminjaman 
                                            SET metode_bayar='Tunai',total_harga='$total_harga',tgl_sewa='$tgl_sewa',lama_sewa='$lama_sewa' 
                                            WHERE id_peminjaman='$id_peminjaman'");

      $select_transaksi = mysqli_query($conn, "SELECT * FROM transaksi WHERE id_transaksi='$id_transaksi'");
      if (mysqli_num_rows($select_transaksi)) {
        $in_transaksi = mysqli_query($conn, "UPDATE transaksi SET metode_pembayaran='Tunai' WHERE id_transaksi='$id_transaksi'");
      } else {
        $in_transaksi = mysqli_query($conn, "INSERT INTO transaksi 
                                            VALUES ('$id_transaksi','$id_peminjaman',NULL,NULL,NULL,NULL,NULL,NULL,'Tunai',NULL,'')");
      }

      $_SESSION['notif'] = 'submit-tunai';
      header('location:peminjaman_index.php');
    } catch (\Exception $e) {
      echo $e;
    }
  } else if (isset($_POST['submit-tf'])) {
    try {
      $up_peminjaman = mysqli_query($conn, "UPDATE peminjaman 
                                            SET metode_bayar='Transfer',total_harga='$total_harga',tgl_sewa='$tgl_sewa',lama_sewa='$lama_sewa' 
                                            WHERE id_peminjaman='$id_peminjaman'");
                                            
      $select_transaksi = mysqli_query($conn, "SELECT * FROM transaksi WHERE id_transaksi='$id_transaksi'");
      if (mysqli_num_rows($select_transaksi)) {
        $in_transaksi = mysqli_query($conn, "UPDATE transaksi SET metode_pembayaran='Transfer' WHERE id_transaksi='$id_transaksi'");
      } else {
        $in_transaksi = mysqli_query($conn, "INSERT INTO transaksi 
                                            VALUES ('$id_transaksi','$id_peminjaman',NULL,NULL,NULL,NULL,NULL,NULL,'Transfer',NULL,'')");
      }
      
      $_SESSION['id_peminjaman'] = $id_peminjaman;
      unset($_SESSION['date']);
      header('location:pembayaran_index.php');
    } catch (\Exception $e) {
      echo $e;
    }
  }
?>