-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2020 at 01:29 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sangga_buana_outdor`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(11) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `nama_barang` varchar(128) NOT NULL,
  `foto_barang` varchar(255) NOT NULL,
  `jenis` varchar(64) NOT NULL,
  `stok_awal` int(11) NOT NULL,
  `tgl_barang_masuk` date NOT NULL,
  `deskripsi` text NOT NULL,
  `jmlh_rusak` int(11) DEFAULT NULL,
  `merek` varchar(64) NOT NULL,
  `harga_sewa` int(11) NOT NULL,
  `jmlh_diambil` int(11) DEFAULT NULL,
  `harga_barang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `id_supplier`, `nama_barang`, `foto_barang`, `jenis`, `stok_awal`, `tgl_barang_masuk`, `deskripsi`, `jmlh_rusak`, `merek`, `harga_sewa`, `jmlh_diambil`, `harga_barang`) VALUES
(1, 1, 'Tenda kapasitas 4-5 orang', '_Screenshot_15.jpg', 'Tenda', 15, '2019-04-04', 'Tenda dome kapasitas 4 orang, untuk kegiatan camping atau mendaki gunung.\r\n\r\nUkuran 210cm x 240cm x 130cm. Double layer. Alas terpal. Ada terasnya. Sudah ada frame, pasak dan tali. Mudah didirikan oleh 2 orang.', 2, 'monodom', 35000, 8, 250000),
(2, 4, 'Hammock Ulina', '_Screenshot_25.jpg', 'hammock', 10, '2019-07-31', 'Ulina hammock Made In Indonesia\r\nsize : 2,5x1,5m\r\ninclude tali webbing (4mtr @2pcs)\r\n#beban max 150 kg', 0, 'Ulina', 10000, 7, 120000),
(4, 2, 'Carier consina ', '_Screenshot_8.jpg', 'Tas gunung', 8, '2019-03-31', 'Carrier Consina Bering 60+10L\r\nBahan Soft Cordura Duratex Polyester\r\ninc cover bag ', 0, 'Consina', 60000, 0, 560000),
(5, 3, 'Sepatu Gunung Size 41', '_Screenshot_3.jpg', 'sepatu gunung', 5, '2019-03-03', 'Sepatu HIking Pria/wanita size 41 WATERPROOF SNTA ', 1, 'SNTA', 35000, 0, 320000),
(6, 3, 'Sepatu Gunung Size 42', '_Screenshot_2.jpg', 'sepatu gunung', 4, '2019-03-04', 'Sepatu Hiking Outdoor Pria \r\nsize 42\r\nBerat 1300gr\r\nwaterproof', 0, 'SNTA', 40000, 0, 340000),
(7, 6, 'Hammock ', '_Screenshot_9.jpg', 'hammock', 3, '2019-03-05', 'Hamoock kap 2 orang max 150kg', 0, 'lokal', 15000, 0, 120000),
(8, 2, 'tas selempang', '_Screenshot_6.jpg', 'Tas gunung', 9, '2019-05-01', 'Tas selempang consina Flexible to use ', 0, 'Consina', 25000, 0, 170000),
(9, 2, 'Carier consina 80 liter', '_Screenshot_7.jpg', 'Tas gunung', 7, '2019-03-05', 'CARRIER CONSINA GALAPAGOS 80 LITER incldue Raincover', 0, 'Consina', 50000, 0, 570000),
(10, 1, 'Cooking Set', '_Screenshot_14.jpg', 'Cooking Set', 15, '2019-03-06', 'Cooking set nesting dan panci', 0, 'lokal', 20000, 3, 150000),
(11, 1, 'Kompor portable', '_Screenshot_4.jpg', 'Cooking Set', 17, '2019-03-06', '\r\nKompor gas mini portable card stove camping praktis', 0, 'lokal', 15000, 0, 150000),
(12, 7, 'Tenda kapasitas 2 orang', '_Screenshot_27.jpg', 'Tenda', 17, '2019-07-02', 'Tenda REI Monodome. Kapasitas : 2-3 orang \r\nP: 210cm x L: 120cm x T: 100cm Fly : 190T polyester \r\ndouble layer\r\nberat 2kg', 0, 'Rei Adventure', 60000, 0, 670000),
(13, 5, 'Tenda eiger kapasitas 2 orang', '_Screenshot_29.jpg', 'Tenda', 8, '2019-03-01', 'Tenda Eiger kapasitas 2 orang\r\ndouble layer\r\nberat 2000gr\r\nRangka fiber\r\nPasak besi', 0, 'EIGER', 70000, 0, 970000),
(14, 6, 'Raincoat', '_Screenshot_24.jpg', 'Jas hujan', 9, '2019-03-05', 'Jas hujan Bahan taslan full coating dan seam seal pada jahitan\r\nwaterproof\r\n\r\nujung lengan puring karet\r\nhoodie\r\nlingkar pinggang karet\r\nno pocket\r\nsarung terpisah tdk menyatu', 0, 'monodom', 20000, 0, 170000),
(15, 6, 'FLYSHET', '_Screenshot_16.jpg', 'flysheet', 20, '2019-04-01', 'Tiang / Frame Flysheet (Tarp / emergency tent)\r\nBahan utama : Aluminium Alloy\r\nType : Light Frame\r\n\r\nSpesifikasi :\r\n- 4 ukuran ketinggian 70cm, 105cm, 140cm, max.200cm\r\n- OD tiang 19mm, ID 17mm, ketebalan 1mm full\r\n- OD socket 16.5mm,\r\n- Tutup Tiang karet 3/4\" atas bawah\r\n- Pasak tenda 20cm stainless steel 3mm\r\n- Tali Kur Cord Filamen 3mm\r\n- Stopper tali', 0, 'lokal', 15000, 0, 90000),
(16, 6, 'Sleeping Bag', '_Screenshot_20.jpg', 'sleeping Bag Panthera', 20, '2019-03-06', 'SLEEPING BAG PANTHERA / OUTDOOR / CAMPING \r\nBahan Polar Bulu, hangat untuk udara dingin', 0, 'lokal', 15000, 0, 140000),
(17, 1, 'Sleeping Bag', '_Screenshot_1.jpg', 'sleeping Bag ', 9, '2019-12-04', 'Sleeping Bag \r\nBahan Dalam Polar Berupa Bulu yg Tebal\r\n- Lapisan Paling Luar Ripstop\r\n- Lapisan Dalam Polar Bulu\r\nModel Tikar (Bukan Mummy)\r\nKalau Resleting Dibuka Total Bisa jadi Tikar\r\nUkuran Sleeping Bag\r\n-/+ 180+15 x 75 x 48 cm', 0, 'monodom', 20000, 0, 150000),
(18, 2, 'Tas Hydro Pack Consina', '_ss.jpg', 'Tas gunung', 9, '2019-02-05', 'Tas hydropack tas lari gunung, Tas sepeda dengen side mesh pocket\r\ndurable fabric\r\nfield tested\r\nhipbelt\r\n1 compartement water\r\n\r\nSpecification :\r\n\r\nAverage Weight : 0,70\r\n\r\nVolume : 2 Ltr\r\n\r\nDimension :6 x 23 x 41\r\n\r\ntersedia warna hitam abu\"', 0, 'Consina', 30000, 0, 300000),
(19, 5, 'arrier Eiger 1232 R Classic Dyno 75 Liter', '_images (2).jpg', 'Tas gunung', 7, '2019-03-01', 'eiger carrier\r\n*art\r\n1232\r\n*dension\r\n80x34x24 cm\r\n*capacity\r\n75 l max\r\n*weight\r\n3kg\r\n*fabric\r\nnylon cordura 420D\r\nnylon cordura 210D\r\n\r\nfeatures\r\n-tol loaded with 2 back top pocket with rain cover accessed\r\n-front pocket with zipper\r\n-2side pocket\r\n-sleeping bag compartment with zipper access to main compartment and additional front loaded compartment\r\n-side streps adjuster\r\n-2strong handles with 50mm nylon webbing\r\n-EIGER ergo dyno back system with U frame&amp;ergonomic flexy accessed shoulder straps', 0, 'EIGER', 60000, 0, 900000),
(20, 5, 'Tas Carrier Eiger Eliptic Solaris 55L', '_images (3).jpg', 'Tas gunung', 7, '2019-12-23', 'Carrier Eiger / Tas Gunung Eiger Eliptic Solaris 55L\r\n\r\n\r\n\r\nEiger Eliptic Solaris 55L - Black Orange : Kompartemen utama dengan zipper separator, kantong depan, straps samping yang bisa diatur, 2 kantong samping, top lid pocket, kompartemen water bladder, kantong hip belt, rain cover, trekking poles holder, teknologi backsystem aerovoid v system.\r\n\r\n\r\n\r\nCarrier ini merupakan edisi â€œCouple Seriesâ€ yang cocok untuk menemani aktivitas petualangannya kamu. Eliptic Solaris tersedia dalam beberapa kapasitas, antara lain : 45L, 55L dan 65L. Dengan kapasitas yang besar, tas ini memang diperuntukan untuk Eigerian yang senang melakukan perjalanan panjang dan ekstrim', 0, 'EIGER', 70000, 1, 800000);

-- --------------------------------------------------------

--
-- Table structure for table `detail_peminjaman`
--

CREATE TABLE `detail_peminjaman` (
  `id` int(11) NOT NULL,
  `id_peminjaman` varchar(16) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga_sewa` int(11) NOT NULL,
  `kondisi` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_peminjaman`
--

INSERT INTO `detail_peminjaman` (`id`, `id_peminjaman`, `id_barang`, `qty`, `harga_sewa`, `kondisi`) VALUES
(1, 'P1912001', 1, 1, 35000, '[\"RR\"]'),
(3, 'P1912003', 1, 1, 35000, '[\"B\"]'),
(4, 'P1912004', 2, 1, 10000, NULL),
(5, 'P1912005', 1, 1, 35000, '[\"B\"]'),
(6, 'P1912006', 1, 1, 35000, '[\"RR\"]'),
(7, 'P1912007', 1, 1, 35000, NULL),
(8, 'P1910008', 1, 1, 35000, '[\"B\"]'),
(9, 'P1910008', 5, 1, 35000, '[\"RR\"]'),
(10, 'P1910008', 1, 1, 35000, NULL),
(11, 'P1910008', 2, 1, 10000, NULL),
(12, 'P1910008', 1, 1, 35000, NULL),
(13, 'P1910008', 1, 1, 35000, NULL),
(14, 'P1811008', 1, 1, 35000, '[\"B\"]'),
(15, 'P1811008', 2, 1, 10000, NULL),
(16, 'P1811008', 2, 1, 10000, NULL),
(17, 'P1811008', 1, 1, 35000, NULL),
(18, 'P1811008', 2, 1, 10000, NULL),
(20, 'P2001008', 2, 1, 10000, NULL),
(21, 'P2001009', 2, 1, 10000, NULL),
(22, 'P2001010', 1, 1, 35000, NULL),
(23, 'P2001011', 1, 1, 35000, NULL),
(24, 'P2001012', 1, 1, 35000, NULL),
(25, 'P2001013', 10, 3, 60000, NULL),
(26, 'P2001013', 2, 2, 20000, '[\"B\",\"B\"]');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id_member` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `foto_identitas` varchar(255) DEFAULT NULL,
  `status` varchar(64) NOT NULL DEFAULT 'member',
  `no_hp` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `alamat` text NOT NULL,
  `password` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id_member`, `nama`, `foto_identitas`, `status`, `no_hp`, `email`, `alamat`, `password`) VALUES
(21, 'Edogawa', 'Edogawa_write-your-own-argumentative-essay-750x420.jpg', 'member', '0897267372', 'edogawa@gmail.com', 'Kuningan bandung lautan api', 'edogawa'),
(25, 'gilang media ', 'gilang media _images.jpg', 'member', '08797645817', 'gilang@gmail.com', 'tasikmalaya', 'gilang'),
(26, 'Lukman Hakim', 'Lukman Hakim_images.jpg', 'member', '083293281', 'lukman@yahoo.com', 'Puri Asri blok A no 127', 'lukman'),
(27, 'Arif prambudi', 'Arif prambudi_images.jpg', 'member', '0829329932', 'arif@gmail.com', 'Perumnas Ciporang jl Dahlia no 47', 'arif'),
(28, 'Fajria falahusifa', 'Fajria falahusifa_images.jpg', 'member', '0822331242', 'fajria@gmail.com', 'Ds.Babakan mulya RT 19', 'fajria'),
(29, 'Abdul Khoer', 'Abdul Khoer_images.jpg', 'member', '08392382388', 'abdul@gmail.com', 'Majalengka Talaga ', 'abdul'),
(30, 'Vera vebriana', 'Vera vebriana_images.jpg', 'member', '08394848483', 'vera@yahoo.com', 'Majalengka', 'vera'),
(31, 'Fathan asraf', 'Fathan asraf_images.jpg', 'member', '08937412733', 'fathan@gmail.com', 'Bekasi timur', 'fathan'),
(32, 'Rendi akbar', 'Rendi akbar_images.jpg', 'member', '08352532553', 'rendi@gmail.com', 'Tasikmalaya ', 'rendi'),
(33, 'irma', 'irma_images.jpg', 'member', '09746464', 'irma@gmai.com', 'selajambe', 'irma'),
(34, 'fazar ikhwan ', 'fazar ikhwan _images.jpg', 'member', '08932821010', 'fazar@gmail.com', 'Cigugur kuningan ', 'fazar'),
(35, 'fina fathatul', 'fina fathatul_images.jpg', 'member', '0837372828', 'fina@yahoo.com', 'Kedungarum kuningan', 'fina');

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id_peminjaman` varchar(16) NOT NULL,
  `id_member` int(11) NOT NULL,
  `metode_bayar` varchar(64) DEFAULT NULL,
  `status_kembali` tinyint(1) NOT NULL DEFAULT 0,
  `total_harga` int(11) NOT NULL,
  `tgl_sewa` date DEFAULT NULL,
  `lama_sewa` int(11) NOT NULL,
  `jaminan` varchar(128) NOT NULL,
  `status_barang` tinyint(1) NOT NULL DEFAULT 0,
  `denda_rusak` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peminjaman`
--

INSERT INTO `peminjaman` (`id_peminjaman`, `id_member`, `metode_bayar`, `status_kembali`, `total_harga`, `tgl_sewa`, `lama_sewa`, `jaminan`, `status_barang`, `denda_rusak`) VALUES
('P1811008', 31, 'Transfer', 1, 35000, '2019-11-03', 1, 'Fathan asraf_images.jpg', 1, 0),
('P1910008', 21, 'Transfer', 1, 70000, '2019-10-02', 1, 'Edogawa_write-your-own-argumentative-essay-750x420.jpg', 1, 80000),
('P1912001', 25, 'Transfer', 1, 35000, '2019-12-21', 1, 'gilang media _images.jpg', 1, 62500),
('P1912002', 30, 'Tunai', 1, 60000, '2019-12-21', 1, 'Vera vebriana_images.jpg', 1, 130000),
('P1912003', 25, 'Transfer', 1, 35000, '2019-12-20', 1, 'gilang media _images.jpg', 1, 140000),
('P1912004', 31, 'Transfer', 0, 10000, '2019-12-21', 1, 'Fathan asraf_images.jpg', 0, NULL),
('P1912005', 30, 'Transfer', 1, 35000, '2019-12-20', 1, 'Vera vebriana_images.jpg', 1, 140000),
('P1912006', 33, 'Tunai', 1, 35000, '2019-12-20', 1, 'irma_images.jpg', 1, 172500),
('P1912007', 25, 'Transfer', 0, 0, '1970-01-01', 0, 'gilang media _images.jpg', 0, NULL),
('P2001008', 33, NULL, 0, 10000, '2020-01-03', 1, 'irma_images.jpg', 0, NULL),
('P2001009', 29, 'Transfer', 0, 10000, '2020-01-03', 1, 'Abdul Khoer_images.jpg', 0, NULL),
('P2001010', 21, 'Transfer', 0, 35000, '2020-01-16', 1, 'Edogawa_write-your-own-argumentative-essay-750x420.jpg', 0, NULL),
('P2001011', 32, 'Transfer', 0, 35000, '2020-01-17', 1, 'Rendi akbar_images.jpg', 0, NULL),
('P2001012', 28, 'Transfer', 0, 35000, '2020-01-16', 1, 'Fajria falahusifa_images.jpg', 0, NULL),
('P2001013', 21, 'Transfer', 1, 80000, '2020-01-22', 1, 'Edogawa_write-your-own-argumentative-essay-750x420.jpg', 1, 37500);

-- --------------------------------------------------------

--
-- Table structure for table `pengembalian`
--

CREATE TABLE `pengembalian` (
  `id_pengembalian` int(11) NOT NULL,
  `id_peminjaman` varchar(16) NOT NULL,
  `denda_telat` int(11) NOT NULL,
  `denda_rusak` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengembalian`
--

INSERT INTO `pengembalian` (`id_pengembalian`, `id_peminjaman`, `denda_telat`, `denda_rusak`) VALUES
(1, 'P1912001', 0, 62500),
(2, 'P1912006', 110000, 62500),
(3, 'P1912003', 140000, 0),
(4, 'P1912002', 130000, 0),
(5, 'P1912005', 140000, 0),
(6, 'P1910008', 0, 80000),
(7, 'P1811008', 0, 0),
(8, 'P2001013', 0, 37500);

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id` int(11) NOT NULL,
  `detail_peminjaman_id` int(11) NOT NULL,
  `rate` enum('1','2','3','4','5') NOT NULL,
  `komentar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id`, `detail_peminjaman_id`, `rate`, `komentar`) VALUES
(1, 8, '1', 'Ada yg sobek'),
(2, 9, '3', 'Lumayan Kuat'),
(3, 10, '2', 'Ada yg sobek sedikit'),
(4, 11, '4', 'Tali Kuat'),
(5, 12, '2', 'Patok besi ada yg kurang'),
(6, 13, '5', 'Tenda nya bagus'),
(7, 25, '4', 'Kondisi nya bersih'),
(8, 26, '5', 'Tali Kuat');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id_supplier` int(11) NOT NULL,
  `nama_supplier` varchar(128) NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id_supplier`, `nama_supplier`, `alamat`, `no_hp`) VALUES
(1, 'Sari Jaya Tenda', ' Jalan.budhi gang.5 no.3 rt.6/rw.3 kel.suka raja kec.cicendo kota bandung', '087737262133'),
(2, 'Consina Stores Tasikmalaya', 'Tasikmalaya Jl.HZ muhamad', '087747501999'),
(3, 'SNTA store', 'Jakarta Barat', '08322372622'),
(4, 'Ulina Hammock', 'Bogor', '08283277172'),
(5, 'Eiger Adventure', 'Majalengka ', '0233282828'),
(6, 'Parka Outdor', 'jl.Dago Bandung', '0838838322'),
(7, 'REI adventure', 'Bandung indah Plaza 2 unit jl.merdeka', '02224223311'),
(8, 'Eiger Adventure Jakarta', 'jakarta barat', '03843784');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` varchar(16) NOT NULL,
  `id_peminjaman` varchar(16) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `transfer_an` varchar(128) DEFAULT NULL,
  `transfer_bank` varchar(128) DEFAULT NULL,
  `bukti_pembayaran` varchar(128) DEFAULT NULL,
  `tgl_pembayaran` date DEFAULT NULL,
  `total_pembayaran` int(11) DEFAULT NULL,
  `metode_pembayaran` varchar(64) NOT NULL,
  `jumlah_dp` int(11) DEFAULT NULL,
  `status_pembayaran` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_peminjaman`, `id_user`, `transfer_an`, `transfer_bank`, `bukti_pembayaran`, `tgl_pembayaran`, `total_pembayaran`, `metode_pembayaran`, `jumlah_dp`, `status_pembayaran`) VALUES
('T1811008', 'P1811008', 3, 'fathan asraf', 'BNI', 'fathan asraf_images.jpg', '2020-01-03', 35000, 'Transfer', NULL, 1),
('T1910008', 'P1910008', 3, 'edo gawa', 'BRI', 'edo gawa_images.jpg', '2019-10-02', 70000, 'Transfer', NULL, 1),
('T1912001', 'P1912001', 3, 'gilang', 'BRI', 'gilang_images.jpg', '2019-12-20', 35000, 'Transfer', NULL, 1),
('T1912002', 'P1912002', 3, NULL, NULL, NULL, '2019-12-21', 60000, 'Tunai', NULL, 1),
('T1912003', 'P1912003', 3, 'gilang', 'BRI', 'gilang_images.jpg', '2019-12-20', 350000, 'Transfer', NULL, 1),
('T1912005', 'P1912005', 3, 'Vera', 'BRi', 'Vera_images.jpg', '2019-12-20', 35000, 'Transfer', NULL, 1),
('T1912006', 'P1912006', 3, NULL, NULL, NULL, '2019-12-20', 35000, 'Tunai', NULL, 1),
('T2001009', 'P2001009', 3, 'abdul khoer', 'BNI', 'abdul khoer_images (2).jpg', '2020-01-16', 35000, 'Transfer', NULL, 1),
('T2001010', 'P2001010', 3, 'Edogawa', 'BANK BRI', 'Edogawa_Picture1.jpg', '2020-01-22', 80000, 'Transfer', NULL, 1),
('T2001011', 'P2001011', NULL, 'randi', 'BANK BCA', 'randi_activity Diagram.png', '2020-01-16', 35000, 'Transfer', NULL, 0),
('T2001012', 'P2001012', NULL, NULL, NULL, NULL, NULL, NULL, 'Transfer', NULL, 0),
('T2001013', 'P2001013', 3, 'Edo', 'BANK BRI', 'Edo_Picture1.jpg', '2020-01-23', 280000, 'Transfer', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama_user` varchar(128) NOT NULL,
  `level` varchar(16) NOT NULL,
  `alamat` text NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama_user`, `level`, `alamat`, `username`, `password`) VALUES
(1, 'Abdul Azis', 'admin', 'Jalan pramuka', 'admin', 'admin'),
(2, 'fathan', 'gudang', 'Ciawigebang, Kab. Kuningan 45591', 'gudang', 'gudang'),
(3, 'Ulfah SIti', 'keuangan', 'Desa puncak blok kulon RT 12', 'keuangan', 'keuangan'),
(5, 'vera vebriana', 'keuangan', 'Ciawigebang, Kuningan, Jawa Barat', 'keuangan2', 'keuangan2'),
(6, 'Muhamad Akbar', 'manager', 'Majalengka', 'manager', 'manager');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`),
  ADD KEY `id_supplier` (`id_supplier`);

--
-- Indexes for table `detail_peminjaman`
--
ALTER TABLE `detail_peminjaman`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_peminjaman` (`id_peminjaman`),
  ADD KEY `id_barang` (`id_barang`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id_member`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`),
  ADD KEY `id_member` (`id_member`);

--
-- Indexes for table `pengembalian`
--
ALTER TABLE `pengembalian`
  ADD PRIMARY KEY (`id_pengembalian`),
  ADD KEY `id_peminjaman` (`id_peminjaman`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detail_peminjaman_id` (`detail_peminjaman_id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_peminjaman` (`id_peminjaman`),
  ADD KEY `user_id` (`id_user`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `detail_peminjaman`
--
ALTER TABLE `detail_peminjaman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id_member` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `pengembalian`
--
ALTER TABLE `pengembalian`
  MODIFY `id_pengembalian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id_supplier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `barang`
--
ALTER TABLE `barang`
  ADD CONSTRAINT `barang_ibfk_1` FOREIGN KEY (`id_supplier`) REFERENCES `supplier` (`id_supplier`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `detail_peminjaman`
--
ALTER TABLE `detail_peminjaman`
  ADD CONSTRAINT `detail_peminjaman_ibfk_3` FOREIGN KEY (`id_peminjaman`) REFERENCES `peminjaman` (`id_peminjaman`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `detail_peminjaman_ibfk_4` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD CONSTRAINT `peminjaman_ibfk_1` FOREIGN KEY (`id_member`) REFERENCES `member` (`id_member`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pengembalian`
--
ALTER TABLE `pengembalian`
  ADD CONSTRAINT `pengembalian_ibfk_1` FOREIGN KEY (`id_peminjaman`) REFERENCES `peminjaman` (`id_peminjaman`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `review_ibfk_1` FOREIGN KEY (`detail_peminjaman_id`) REFERENCES `detail_peminjaman` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`id_peminjaman`) REFERENCES `peminjaman` (`id_peminjaman`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
