<?php 
    $conn = mysqli_connect('localhost', 'root', '', 'sangga_buana_outdor');
    date_default_timezone_set('Asia/Jakarta');
    
    while (true) {
        // untuk mengambil data peminjaman yang tidak melakukan pembayaran tunai atau transfer
        $select_peminjaman = mysqli_query($conn, "SELECT p.id_peminjaman, p.tgl_sewa FROM peminjaman p INNER JOIN transaksi t ON p.id_peminjaman=t.id_peminjaman 
                                                    WHERE t.total_pembayaran IS NULL");
        $peminjamans = [];
        while ($peminjaman = mysqli_fetch_array($select_peminjaman)) {
            $peminjamans[] = $peminjaman;
        }

        $dateThird = date('Y-m-d', strtotime(date('Y-m-d').'- 2 days'));

        foreach ($peminjamans as $p) {
            $id_peminjaman = $p['id_peminjaman'];
            
            if (strtotime($p['tgl_sewa']) <= strtotime($dateThird)) {
                $select_detail = mysqli_query($conn, "SELECT * FROM detail_peminjaman WHERE id_peminjaman='$id_peminjaman'");
                $details = [];
                while ($detail = mysqli_fetch_array($select_detail)) {
                    $details[] = $detail;
                }
                
                foreach ($details as $d) {
                    $id_barang = $d['id_barang'];
                    $jmlh_diambil = $d['qty'];
                    $up_barang = mysqli_query($conn, "UPDATE barang SET jmlh_diambil=jmlh_diambil-'$jmlh_diambil' WHERE id_barang='$id_barang'");
                }

                $del_peminjaman = mysqli_query($conn, "DELETE FROM peminjaman WHERE id_peminjaman='$id_peminjaman'");
            } 
        }

        // untuk mengambil data peminjaman yang belum memilih metode bayar
        $select_penminjaman_only = mysqli_query($conn, "SELECT id_peminjaman, tgl_sewa FROM peminjaman WHERE metode_bayar IS NULL");
        $peminjamans_only = [];
        while ($p_only = mysqli_fetch_array($select_penminjaman_only)) {
            $peminjamans_only[] = $p_only;
        }

        $dateThirdMetodeNull = date('Y-m-d', strtotime(date('Y-m-d').'- 3 days'));

        foreach ($peminjamans_only as $po) {
            $id_peminjaman_only = $po['id_peminjaman'];

            if (strtotime($po['tgl_sewa']) <= strtotime($dateThirdMetodeNull)) {
                $select_detail_metode_null = mysqli_query($conn, "SELECT * FROM detail_peminjaman WHERE id_peminjaman='$id_peminjaman_only'");
                $details_metode_null = [];
                while ($detail_metode_null = mysqli_fetch_array($select_detail_metode_null)) {
                    $details_metode_null[] = $detail_metode_null;
                }
                
                foreach ($details_metode_null as $dmn) {
                    $id_barang_metode_null = $dmn['id_barang'];
                    $jmlh_diambil_metode_null = $dmn['qty'];
                    $up_barang_metode_null = mysqli_query($conn, "UPDATE barang SET jmlh_diambil=jmlh_diambil-'$jmlh_diambil_metode_null' WHERE id_barang='$id_barang_metode_null'");
                }

                $del_peminjaman_metode_null = mysqli_query($conn, "DELETE FROM peminjaman WHERE id_peminjaman='$id_peminjaman_only'");
            } 
        }

        sleep(60);
    }
?>