<?php 
  require('layouts/header.php');
  require('config/connection.php');

  $id_member = $_SESSION['id_member'];

  $select_peminjaman = mysqli_query($conn, "SELECT * FROM peminjaman WHERE id_member='$id_member' AND metode_bayar IS NULL");
  $peminjaman = [];
  while ($row = mysqli_fetch_array($select_peminjaman)) {
    $peminjaman[] = $row;
  }

  $total_biaya = 0;
  $m = 0;
?>

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-5 text-center">
      <h2 class="my-4">Daftar Tagihan</h2>
      <?php 
        if (isset($_SESSION['notif'])) {
          switch ($_SESSION['notif']) {
            case 'tgl-empty': 
              echo '
              <div class="alert alert-danger alert-dismissible fade show text-left" role="alert">
                <strong>Tanggal sewa tidak boleh kosong!</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>'; break;
          }
          unset($_SESSION['notif']);
        }
      ?>
    </div>
  </div>

  <!-- tabel tagihan -->
  <div class="row">
    <div class="col-12">
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr class="text-center">
              <th>Id Peminjaman</th>
              <!-- <th>Tanggal Sewa</th> -->
              <th>Lama Sewa (hari)</th>
              <th>Total (Rp)</th>
              <th>Status</th>
              <th>Metode Pembayaran</th>
            </tr>
          </thead>
          <tbody>
            <?php if (!mysqli_num_rows($select_peminjaman)) : ?>
              <tr>
                <td colspan="6" class="text-center">Tidak ada tagihan!</td>
              </tr>
            <?php else : ?>
              <?php foreach ($peminjaman as $p) : ?>
                <?php 
                  $m++; 
                  $idpeminjaman = $p['id_peminjaman'];
                  $select_transaksi = mysqli_query($conn, "SELECT status_pembayaran FROM transaksi 
                                                            WHERE id_peminjaman='$idpeminjaman'");
                ?>
                <?php if (!mysqli_num_rows($select_transaksi)) : ?>
                  <form id="tagihan_form" action="pembayaran_konfirmasi.php" method="post">
                    <tr>
                      <td class="align-middle">
                        <input class="form-control-plaintext border-0 text-center" type="text" name="id_peminjaman" value="<?= $p['id_peminjaman'] ?>" readonly>
                      </td>
                      <input type="hidden" class="form-control text-center" name="tgl_sewa" autocomplete="off" value="<?= $p['tgl_sewa'] ?>">
                      <!-- <td class="align-middle text-center" width="20%"> -->
                      <!-- </td> -->
                      <td class="align-middle text-center">
                        <input type="text" name="lama_sewa" id="tagihan-lama-sewa" class="border-0 w-25 text-center mb-2" value="<?= $p['lama_sewa'] ?>" readonly><br>
                        <button type="button" id="btn-minus-hari" class="btn btn-sm btn-outline-primary btn-inc"><i class="fas fa-minus"></i></button>
                        <button type="button" id="btn-plus-hari" class="btn btn-sm btn-primary btn-inc"><i class="fas fa-plus"></i></button>
                      </td>
                      <td class="align-middle text-center" width="10%">
                        <input type="text" name="total_harga" id="tagihan-total-harga" class="form-control-plaintext border-0 text-center" value="<?= $p['total_harga'] ?>" readonly>
                      </td>
                      <td class="align-middle text-center">
                        <?php 
                          $total_biaya += $p['total_harga'];
                          if ($p['status_barang'] == 0) {
                            echo 'Belum bayar';
                          } else {
                            echo 'Sudah bayar';
                          }
                        ?>
                      </td>
                      <td class="align-middle text-center">
                        <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tunaiModal<?= $m ?>">Tunai</button> &nbsp; -->
                        <button name="submit-tf" onclick="form_submit()" class="btn btn-primary">Konfirmasi Pembayaran</button>
                        <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tfModal<?= $m ?>">Transfer</button> -->
                      </td>
                    </tr>
                    <!-- awal modal -->
                    <div class="modal fade" id="tunaiModal<?= $m ?>" tabindex="-1" role="dialog" aria-labelledby="tunaiModalTitle" aria-hidden="true">
                      <div class="modal-dialog modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="tunaiModalTitle">Syarat & Ketentuan</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            tunai 
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Batal</button>
                            <button name="submit-tunai" onclick="form_submit()" class="btn btn-primary">OK</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- akhir modal -->
                    <!-- awal modal -->
                    <div class="modal fade" id="tfModal<?= $m ?>" tabindex="-1" role="dialog" aria-labelledby="tfModalTitle" aria-hidden="true">
                      <div class="modal-dialog modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="tfModalTitle">Syarat & Ketentuan</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            tf kesini
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Batal</button>
                            <button name="submit-tf" onclick="form_submit()" class="btn btn-primary">OK</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- akhir modal -->
                  </form>
                <?php endif; ?>
              <?php endforeach; ?>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <!-- akhir tabel tagihan -->
   <div class="row justify-content-center mt-4">
    <div class="col-md-6 mb-5">
      <div class="card mb-4">
        <div class="card-body">
          <h4 class="text-center">Syarat & Ketentuan</h4>
          <ol>
            <li>Transfer pembayaran dilakukan via BRI atau BNI dengan No rekening : 
              <ul>
                <li>BNI. 836 297 7326528</li>
                <li>a.n. Abdul Azis</li>
              </ul>
			    <ul>
                <li>BRI. 0133 092 0393438</li>
                <li>a.n. Abdul Azis</li>
              </ul>
            </li>
            <li>Silahkan foto / scan bukti pembayaran, lalu kirimkan bukti pembayaran.</li>
			<li>Bukti pembayaran akan divalidasi oleh petugas dan notifikasi pembayaran akan di tampilkan pada daftar peminjaman </li>
            <li>Pembayaran yang sudah dilakukan tidak dapat dibatalkan.</li>
			<li>Jika penyewa telah melakukan pembayaran dan tidak melakukan pengambilan barang, pengembalian dana diberikan sebesar 90% menginggat stok barang yang sudah dipesan.</li> 
			<li>Dana pengembalian akan ditransfer Ke rekening yang telah mengirimkan bukti pembayaran.</li>
			<li>Untuk info lebih llanjut penyewa bisa menghbungi petugas melalu whatsapp atau media sosial lainnya.</li>
			<li>Dengan senang hati kami akan melayani setulus hati.</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require('layouts/footer.php') ?>