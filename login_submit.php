<?php 
  session_start();
  unset($_SESSION['keranjang_belanja']);
  unset($_SESSION['id_barang']);
  require('config/connection.php');
  extract($_POST);

  $data = mysqli_query($conn, "SELECT * FROM member WHERE email='$email' AND password='$password'");
  $account = mysqli_fetch_assoc($data);

  if (mysqli_num_rows($data) > 0) {
    $_SESSION['email'] = $email;
    $_SESSION['id_member'] = $account['id_member'];
    $id_member = $account['id_member'];
    $_SESSION['nama'] = $account['nama'];
    $_SESSION['foto_identitas'] = $account['foto_identitas'];

    $query_jml_review = mysqli_query($conn, "SELECT COUNT(*) AS jml_review FROM review r INNER JOIN detail_peminjaman d ON r.detail_peminjaman_id=d.id INNER JOIN peminjaman p ON d.id_peminjaman=p.id_peminjaman WHERE p.id_member='$id_member'");
    $jml_review = mysqli_fetch_assoc($query_jml_review);
    $_SESSION['jml_review'] = $jml_review['jml_review'];
    $query_jml_blm_review = mysqli_query($conn, "SELECT COUNT(*) AS jml_blm_review FROM detail_peminjaman d INNER JOIN peminjaman p ON d.id_peminjaman=p.id_peminjaman WHERE p.id_member='$id_member' AND p.status_kembali=1");
    $jml_blm_review = mysqli_fetch_assoc($query_jml_blm_review);
    $_SESSION['jml_blm_review'] = $jml_blm_review['jml_blm_review'];

    switch ($account['status']) {
      case 'member':
        $select_id_peminjaman = mysqli_query($conn, "SELECT p.id_peminjaman FROM peminjaman p INNER JOIN transaksi t ON p.id_peminjaman=t.id_peminjaman WHERE p.id_member='$id_member' AND p.metode_bayar='Transfer' AND t.status_pembayaran=0");
        if (mysqli_num_rows($select_id_peminjaman)) {
          $id_peminjaman = mysqli_fetch_assoc($select_id_peminjaman);
          $_SESSION['id_peminjaman'] = $id_peminjaman;
        }
        $_SESSION['status'] = 'member';
        header('location:index.php?notif=login');
        break;
      case 'admin':
        $_SESSION['status'] = 'admin';
        header('location:admin/index.php?notif=login');
        break;
      case 'gudang':
        $_SESSION['status'] = 'gudang';
        header('location:admin/index.php?notif=login');
        break;
      case 'keuangan':
        $_SESSION['status'] = 'keuangan';
        header('location:admin/index.php?notif=login');
        break;
    }
  } else {
    header('location:login_index.php?notif=error-account');
  }
?>