<?php require('layouts/header.php'); ?>

<?php 
  require('config/connection.php');
  
  // untuk pagination
  $halaman = 9;
  $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
  $mulai = ($page > 1) ? ($page * $halaman) - $halaman : 0;
  $query = mysqli_query($conn, "SELECT * FROM barang LIMIT $mulai, $halaman");
  $sql = mysqli_query($conn, "SELECT * FROM barang");
  $total = mysqli_num_rows($sql);
  $pages = ceil($total/$halaman);
  
  if (isset($_SESSION['date']['tgl_awal'])) {
    $query = mysqli_query($conn, "SELECT * FROM barang b LEFT JOIN (SELECT id_peminjaman, id_barang as id_brg, sum(qty) as jml_dipinjam, kondisi FROM detail_peminjaman WHERE kondisi IS NULL GROUP BY id_brg) dp ON dp.id_brg=b.id_barang LEFT JOIN peminjaman p ON dp.id_peminjaman=p.id_peminjaman");
  }

  // untuk search
  if (isset($_GET['search'])) {
    if (isset($_SESSION['date'])) {
      $query = mysqli_query($conn, "SELECT * FROM barang b LEFT JOIN (SELECT id_peminjaman, id_barang as id_brg, sum(qty) as jml_dipinjam, kondisi FROM detail_peminjaman WHERE kondisi IS NULL GROUP BY id_brg) dp ON dp.id_brg=b.id_barang LEFT JOIN peminjaman p ON dp.id_peminjaman=p.id_peminjaman WHERE b.nama_barang LIKE '%".$_GET['search']."%'");
    } else {
      $query = mysqli_query($conn, "SELECT * FROM barang WHERE nama_barang LIKE '%".$_GET['search']."%'");
    }
  }

  // untuk merek
  if (isset($_GET['merek'])) {
    $byMerek = $_GET['merek'];
    if (isset($_SESSION['date'])) {
      $query = mysqli_query($conn, "SELECT * FROM barang b LEFT JOIN (SELECT id_peminjaman, id_barang as id_brg, sum(qty) as jml_dipinjam, kondisi FROM detail_peminjaman WHERE kondisi IS NULL GROUP BY id_brg) dp ON dp.id_brg=b.id_barang LEFT JOIN peminjaman p ON dp.id_peminjaman=p.id_peminjaman WHERE b.merek='$byMerek'");
    } else {
      $query = mysqli_query($conn, "SELECT * FROM barang WHERE merek='$byMerek'");
    }
  }

  // jika set tgl_awal
  if (isset($_GET['tgl_awal'])) {
    $tgl_awal = $_GET['tgl_awal'];
    $lama_sewa = $_GET['lama_sewa'];

    $_SESSION['date'] = [];
    $_SESSION['date']['tgl_awal'] = $tgl_awal;
    $_SESSION['date']['lama_sewa'] = $lama_sewa;

    $query = mysqli_query($conn, "SELECT * FROM barang b LEFT JOIN (SELECT id_peminjaman, id_barang as id_brg, sum(qty) as jml_dipinjam, kondisi FROM detail_peminjaman WHERE kondisi IS NULL GROUP BY id_brg) dp ON dp.id_brg=b.id_barang LEFT JOIN peminjaman p ON dp.id_peminjaman=p.id_peminjaman");
  }

  // untuk menampilkan data barang
  $barangs = [];
  while ($barang = mysqli_fetch_assoc($query)) {
    $barangs[] = $barang;
  }

  $sql_merek = mysqli_query($conn, "SELECT COUNT(id_barang), merek FROM barang GROUP BY merek");
  $mereks = [];
  while ($merek = mysqli_fetch_assoc($sql_merek)) {
    $mereks[] = $merek;
  }

  if (isset($_GET['notif'])) {
    if ($_GET['notif']=='login') {
      echo '
        <div class="alert alert-warning alert-dismissible fade show mx-3 text-left" role="alert">
          <strong>Login berhasil!</strong> Selamat datang di Website Sangga Buana Outdor.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>';
    }
  }

  if (isset($_GET['date'])) {
    if ($_GET['date'] == 'null') {
      unset($_SESSION['date']);
      header('location:index.php'); die;
    }
  }
?>

  <div class="jumbotron jumbotron-fluid" style="background:url('images/background.jpg') no-repeat;background-size:cover;background-position:center;">
    <div class="container text-center text-white">
      <h2>Selamat datang di toko Sangga Buana Outdor Kuningan</h2>
      <p class="lead">Kami menyediakan alat outdor untuk anda.</p>

    </div>
  </div>

  <div class="container">

  <!-- awal search form -->
  <div class="row justify-content-end mb-4">
    <div class="col-md-3">
      <form method="GET" action="" class="form-inline">
        <input class="form-control mr-sm-2" name="search" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fas fa-search"></i></button>
      </form>
    </div>
  </div>
  <!-- akhir search form -->

  <div class="row">
    <!-- awal kategori barang -->
    <div class="col-md-3 mb-4">
      <div class="list-group">
        <?php foreach ($mereks as $m) : ?>
        <a href="./?merek=<?= $m['merek'] ?>" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
          <?= $m['merek'] ?>
          <span class="badge badge-primary badge-pill"><?= $m['COUNT(id_barang)'] ?></span>
        </a>
        <?php endforeach; ?>
      </div>
    </div>
    <!-- akhir kategori barang -->
    <!-- awal list barang -->
    <div class="col-md-9 mb-5">
      <div class="row mb-4">
        <div class="col-12">
          <?php 
            if (isset($_SESSION['notif'])) {
              if ($_SESSION['notif']=='date-null') {
                echo '
                  <div class="alert alert-danger alert-dismissible fade show mx-3 text-left" role="alert">
                    <strong>Gagal!</strong> Tanggal pinjam dan lama sewa harus diisi!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>';
              }
              unset($_SESSION['notif']);
            }
          ?>
          <div class="card bg-info">
            <div class="card-body">
              <?php if (!isset($_SESSION['date'])) : ?>
                <form action="" method="get">
                  <div class="row" style="margin-bottom: -15px;">
                    <div class="col-md-5 form-group">
                      <input class="form-control" id="date" name="tgl_awal" placeholder="   Pilih tanggal pinjam" required autocomplete="off">
                    </div>
                    <div class="col-md-5 form-group">
                      <input type="number" class="form-control" name="lama_sewa" min="1" placeholder="  Masukan lama sewa (hari)" required autocomplete="off">
                    </div>
                    <div class="col-md-2">
                      <button type="submit" class="btn btn-warning w-100">SET</button>
                    </div>
                  </div>
                </form>
              <?php else : ?>
                <div class="row" style="margin-bottom: -15px;">
                  <div class="col-md-5 form-group">
                    <input type="text" class="form-control" value="<?= $_SESSION['date']['tgl_awal'] ?>" readonly>
                  </div>
                  <div class="col-md-5 form-group">
                    <input type="text" class="form-control" value="<?= $_SESSION['date']['lama_sewa'] ?> hari" readonly>
                  </div>
                  <div class="col-md-2">
                    <a href="./index.php?date=null" class="btn btn-danger w-100">RESET</a>
                  </div>
                </div>
              <?php endif ?>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <?php foreach($barangs as $barang) : ?>
        <?php if (!isset($barang['jml_dipinjam'])) : ?>
          <div class="col-md-4 mb-3">
            <div class="card" style="width: 100%;">
              <div class="text-center">
                <img class="center-cropped" src="images/<?= $barang['foto_barang'] ?>" class="card-img-top" alt="<?= $barang['nama_barang'] ?>">
              </div>
              <div class="card-body">
                <h6 class="card-subtitle mb-2 text-muted"><?= $barang['nama_barang'] ?></h6>
                <h4 class="card-title mb-3">Rp. <?= $barang['harga_sewa'] ?></h4>
                <h6 class="card-subtitle mb-3 text-muted">Stok <?= $barang['stok_awal']-$barang['jmlh_rusak'] ?></h6>
                <div class="text-right">
                  <?php if (($barang['stok_awal']-$barang['jmlh_rusak']-$barang['jmlh_diambil']) == 0) : ?>
                    <a href="" class="btn btn-outline-secondary disabled">Stok Kosong</a>
                  <?php else : ?>
                    <a href="barang_index.php?id=<?= $barang['id_barang'] ?>" class="btn btn-primary">Sewa</a>
                  <?php endif ?>
                </div>
              </div>
            </div>
          </div>
        <?php else : ?>
          <?php if (strtotime($_SESSION['date']['tgl_awal']) >= strtotime($barang['tgl_sewa']) && strtotime($_SESSION['date']['tgl_awal']) <= strtotime(date('Y-m-d', strtotime($barang['tgl_sewa'].'+'.$barang['lama_sewa'].' day')))) : ?>
            <div class="col-md-4 mb-3">
              <div class="card" style="width: 100%;">
                <div class="text-center">
                  <img class="center-cropped" src="images/<?= $barang['foto_barang'] ?>" class="card-img-top" alt="<?= $barang['nama_barang'] ?>">
                </div>
                <div class="card-body">
                  <h6 class="card-subtitle mb-2 text-muted"><?= $barang['nama_barang'] ?></h6>
                  <h4 class="card-title mb-3">Rp. <?= $barang['harga_sewa'] ?></h4>
                  <h6 class="card-subtitle mb-3 text-muted">Stok <?= $barang['stok_awal']-$barang['jmlh_rusak']-$barang['jml_dipinjam'] ?></h6>
                  <div class="text-right">
                    <?php if (($barang['stok_awal']-$barang['jmlh_rusak']-$barang['jmlh_diambil']) == 0) : ?>
                      <a href="" class="btn btn-outline-secondary disabled">Stok Kosong</a>
                    <?php else : ?>
                      <a href="barang_index.php?id=<?= $barang['id_barang'] ?>" class="btn btn-primary">Sewa</a>
                    <?php endif ?>
                  </div>
                </div>
              </div>
            </div>
          <?php else : ?>
            <div class="col-md-4 mb-3">
              <div class="card" style="width: 100%;">
                <div class="text-center">
                  <img class="center-cropped" src="images/<?= $barang['foto_barang'] ?>" class="card-img-top" alt="<?= $barang['nama_barang'] ?>">
                </div>
                <div class="card-body">
                  <h6 class="card-subtitle mb-2 text-muted"><?= $barang['nama_barang'] ?></h6>
                  <h4 class="card-title mb-3">Rp. <?= $barang['harga_sewa'] ?></h4>
                  <h6 class="card-subtitle mb-3 text-muted">Stok <?= $barang['stok_awal']-$barang['jmlh_rusak'] ?></h6>
                  <div class="text-right">
                    <?php if (($barang['stok_awal']-$barang['jmlh_rusak']-$barang['jmlh_diambil']) == 0) : ?>
                      <a href="" class="btn btn-outline-secondary disabled">Stok Kosong</a>
                    <?php else : ?>
                      <a href="barang_index.php?id=<?= $barang['id_barang'] ?>" class="btn btn-primary">Sewa</a>
                    <?php endif ?>
                  </div>
                </div>
              </div>
            </div>
          <?php endif ?>
        <?php endif ?>
        <?php endforeach; ?>
      </div>
      <div class="row mb-5">
        <div class="col-md-12 mb-3">
          <ul class="pagination justify-content-center">
            <?php if (isset($_GET['page'])) : ?>
              <?php if ($_GET['page'] <= 1) : ?>
                <li class="page-item disabled"><a class="page-link" href="?page=<?= $_GET['page']-1 ?>">Previous</a></li>
              <?php else : ?>
                <li class="page-item"><a class="page-link" href="?page=<?= $_GET['page']-1 ?>">Previous</a></li>
              <?php endif; ?>
            <?php endif; ?>
            <?php for ($i=1; $i <= $pages; $i++) : ?>
              <li class="page-item"><a class="page-link" href="?page=<?= $i ?>"><?= $i ?></a></li>
            <?php endfor; ?>
            <?php if (isset($_GET['page'])) : ?>
              <?php if ($_GET['page'] >= $pages) : ?>
                <li class="page-item disabled"><a class="page-link" href="?page=<?= $_GET['page']+1 ?>">Next</a></li>
              <?php else : ?>
                <li class="page-item"><a class="page-link" href="?page=<?= $_GET['page']+1 ?>">Next</a></li>
              <?php endif; ?>
            <?php endif; ?>
          </ul>
        </div>
      </div>
    </div>
    <!-- akhir list barang -->
  </div>

  </div>

  <!-- Modal ask rating -->
  <div class="modal fade" id="askRating" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body text-center p-4">
          <h3>Bantu kami menjadi lebih baik</h3><br>
          <button class="btn btn-lg btn-outline-secondary" type="button" data-dismiss="modal">Lain kali</button>&nbsp; 
          <a class="btn btn-lg btn-primary" href="./peminjaman_index.php">Berikan Review</a>
        </div>
      </div>
    </div>
  </div>

<?php require('layouts/footer.php'); ?>