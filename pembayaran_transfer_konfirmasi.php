<?php 
  session_start();
  require('config/connection.php');

  extract($_POST);

  // upload bukti pembayaran
  $ekstensi = ['png', 'jpeg', 'jpg', 'bmp'];
  $nama_file = $_FILES['bukti_pembayaran']['name'];
  $x = explode('.', $nama_file);
  $ekstensi_file = strtolower(end($x));
  $ukuran_file = $_FILES['bukti_pembayaran']['size'];
  $temp_file = $_FILES['bukti_pembayaran']['tmp_name'];

  $namaFile = $transfer_an.'_'.$nama_file;
  $tgl_bayar = date('Y-m-d', strtotime($tgl_pembayaran));

  if (in_array($ekstensi_file, $ekstensi) === true) {
    if ($ukuran_file <= 5000000) {
      move_uploaded_file($temp_file, 'file/bukti-pembayaran/'.$namaFile);
      try {
        $query = "UPDATE transaksi 
                  SET 
                  transfer_an='$transfer_an', transfer_bank='$transfer_bank', total_pembayaran='$total_pembayaran', bukti_pembayaran='$namaFile', tgl_pembayaran='$tgl_bayar'
                  WHERE id_peminjaman='$id_peminjaman'";
        mysqli_query($conn, $query);
      } catch (\Exception $e) {
        echo $e;
      }
      unset($_SESSION['id_peminjaman']);
      $_SESSION['notif'] = 'submit-tf';
      header('location:peminjaman_index.php');
    } else {
      $_SESSION['notif'] = 'size-file';
      header('location:pembayaran_index.php');
    }
  } else {
    $_SESSION['notif'] = 'ekstensi-file';
    header('location:pembayaran_index.php');
  }
?>