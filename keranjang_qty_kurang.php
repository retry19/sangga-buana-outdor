<?php 
  session_start();

  $id_barang = $_GET['id'];

  foreach ($_SESSION['keranjang_belanja'] as $key => $value) {
    if ($_SESSION['keranjang_belanja'][$key]['id_barang'] == $id_barang) {
      if ($_SESSION['keranjang_belanja'][$key]['qty'] != 1) {
        $_SESSION['keranjang_belanja'][$key]['qty']--; 
      }
    }

    header('location:keranjang_index.php');
  }
?>