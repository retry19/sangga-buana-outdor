<!-- awal footer -->
<div class="row"  style="background-color: #2d3d5a; color:#fff;margin-bottom:8px;padding:20px;">
	<div class="col-sm-4" >
		<p>Jl. Pramuka No.130, Purwawinangun<br>
	Kuningan Jawa Barat<br>
	Telefon: (0232) 874538<br>
	Opens at 8AM<br>
	Kode POS: Kuningan 45512<br>
	</p>
	</div>
	<div class="col-sm-4">
		Ikuti Kami di media sosial:<br>
		<a href="https://www.facebook.com/sanggabuanaoutdoor" style="color:#fff;font-size:36px;margin-right:10px;"><i class="fab fa-facebook-f"></i></a>
		<a href="https://www.instagram.com/sanggabuanaoutdoor_kuningan/" style="color:#fff;font-size:36px;margin-right:10px;"><i class="fab fa-instagram"></i></a>
	</div>
</div>
  <div class="jumbotron footer mt-4 mb-0" style="background-color: #2d3d5a; color:#fff;">
    <div class="container text-right">
      <p>2019 &copy; Sangga Buana Outdor</p>
    </div>
  </div>
  <!-- akhir footer -->

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Yakin akan keluar?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Pilih "Logout" untuk keluar dari akun.</div>
        <div class="modal-footer">
          <button class="btn btn-white" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="./login_logout.php">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/star-rating.js"></script>
  <script src="js/btn-tagihan.js"></script>
  <script src="js/script.js"></script>
  <?php if (isset($_SESSION['jml_review'])) { ?>
    <?php if ($_SESSION['jml_blm_review'] > $_SESSION['jml_review']) { ?>
      <script>
        $(document).ready(function(){
          $('#askRating').modal('show');
        });
      </script>
    <?php } unset($_SESSION['jml_blm_review']); unset($_SESSION['jml_review']); ?>
  <?php } ?>
  <script>
    var date = new Date();
    var lastDate = new Date();
    date.setDate(date.getDate());
    lastDate.setDate(lastDate.getDate() + 7);

    $('#date').datepicker({ 
      startDate: date,
      format: "dd-mm-yyyy",
      todayHighlight: true,
    });

    $('.date').datepicker({ 
      startDate: date,
      endDate: lastDate,
      // endDate: new Date('2019-11-20'),
      format: "dd-mm-yyyy",
      todayHighlight: true,
    });

    $('.datepicker').datepicker({ 
      format: "dd-mm-yyyy",
      todayHighlight: true,
    });

    $('#rating-system').change(function(){
      if ($(this).val() > 0) {
        $('#btn-rating').prop('disabled', false)
      } else {
        $('#btn-rating').prop('disabled', true)
      }
    });

    $(document).ready(function(){
      $(document).on('click', '.button-review', function(){
        let id_peminjaman = $(this).attr('data-id');
        $.ajax({
          url: "get_detail_peminjaman.php",
          method: "GET",
          data: { id: id_peminjaman },
          dataType: "JSON",
          success: function(data){
            $('#modalRating').modal('show');
            let html = '';
            let i;
            for (i = 0; i < data.length; i++) {
              html += '<div class="form-group">' +
                        '<label>' + data[i].nama_barang + '</label><br>' +
                        '<input type="hidden" name="iddp_' + data[i].detail_peminjaman_id +'" id="iddp_'+ data[i].detail_peminjaman_id +'" value="'+ data[i].detail_peminjaman_id +'">' +
                        '<span class="star-rating star-5">' +
                          '<input type="radio" name="rating_' + data[i].detail_peminjaman_id +'" value="1"><i></i>'+
                          '<input type="radio" name="rating_' + data[i].detail_peminjaman_id +'" value="2"><i></i>'+
                          '<input type="radio" name="rating_' + data[i].detail_peminjaman_id +'" value="3"><i></i>'+
                          '<input type="radio" name="rating_' + data[i].detail_peminjaman_id +'" value="4"><i></i>'+
                          '<input type="radio" name="rating_' + data[i].detail_peminjaman_id +'" value="5"><i></i>'+
                        '</span><br>'+
                        '<textarea class="form-control" name="komentar_' + data[i].detail_peminjaman_id +'" rows="3" maxlength="250"></textarea>' +
                      '</div>';
            }
            html += '<input type="hidden" name="count" value="'+ data.length +'">';
            $('#body-rating').html(html);
          }
        })
      })
    });

    $(document).ready(function(){
      $(document).on('click', '.button-hasil-review', function(){
        let id_peminjaman = $(this).attr('data-id');
        $.ajax({
          url: "get_hasil_review.php",
          method: "GET",
          data: { id: id_peminjaman },
          dataType: "JSON",
          success: function(data){
            $('#modalHasilRating').modal('show');
            let html = '';
            let i;
            for (i = 0; i < data.length; i++) {
              html += '<div class="row justify-content-center mb-3">'+
                        '<div class="col-6">'+ data[i].nama_barang +'</div>' +
                        '<div class="col-2">'+ data[i].rate +'&nbsp; <i class="fas fa-star text-warning"></i></div>' +
                      '</div>';
            }
            $('#body-hasil-rating').html(html);
          }
        })
      })
    });
  </script>
</body>
</html>