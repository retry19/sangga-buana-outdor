<?php 
  session_start();

  function active($current_page) {
    $url_array = explode('/', $_SERVER['REQUEST_URI']);
    $url = end($url_array);
    if ($current_page == $url) {
      echo 'active';
    }
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Sangga Buana Outdor</title>
  <link rel="stylesheet" href="vendor/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-datepicker.css">
  <link rel="stylesheet" href="css/rating-radio.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<body id="body">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
      <a class="navbar-brand" href="./">Sangga Buana Outdor</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav mr-0 ml-auto">
        <?php if (isset($_SESSION['id_member'])) : ?>
          <a class="nav-item nav-link mx-2 my-1 <?= active('keranjang_index.php') ?>" href="./keranjang_index.php"><i class="fas fa-shopping-cart"></i> Keranjang Sewa</a>
          <a class="nav-item nav-link mx-2 my-1 <?= active('tagihan_index.php') ?>" href="./tagihan_index.php"><i class="fas fa-wallet"></i> Daftar Tagihan</a>
          <a class="nav-item nav-link mx-2 my-1 <?= active('peminjaman_index.php') ?>" href="./peminjaman_index.php"><i class="fas fa-clipboard-list"></i> Daftar Peminjaman</a>
          <div class="dropdown mr-3">
            <a href="#" class="nav-item nav-link mx-2 my-1 dropdown-toggle" role="button" id="account" data-toggle="dropdown">
              <i class="fas fa-user"></i>
            </a>
            <div class="dropdown-menu" aria-labelledby="account">
              <a href="#" class="dropdown-item disabled"><?= $_SESSION['nama'] ?></a>
              <?php if (isset($_SESSION['id_peminjaman'])) : ?>
                <a href="./pembayaran_index.php" class="dropdown-item">Konfirmasi Pembayaran</a>
              <?php endif ?>
              <a href="./login_logout.php" class="dropdown-item" data-toggle="modal" data-target="#logoutModal">Logout</a>
            </div>
          </div>
        <?php else : ?>
            <a class="nav-item nav-link mx-2 my-1" href="./keranjang_index.php"><i class="fas fa-shopping-cart"></i> Keranjang Sewa</a>
            <a href="./daftar_index.php" class="nav-item btn btn-primary mx-2 my-1">DAFTAR</a>
            <a href="./login_index.php" class="nav-item btn btn-outline-secondary mx-2 my-1">LOGIN</a>
        <?php endif; ?>
        </div>
      </div>
    </div>
  </nav>

  <a href="https://api.whatsapp.com/send?phone=089661938540&text=Halo%21%20Saya%20ingin%20bertanya%20mengenai%20Sangga%20Buana%20Outdor" class="float" target="_blank">
    <i class="fab fa-whatsapp my-float"></i>
  </a>