<?php 
  session_start();
  if ($_SESSION['level'] == 'member') {
    header('location:../index.php');die;
  }

  function active($current_page) {
    $url_array = explode('/', $_SERVER['REQUEST_URI']);
    $url = end($url_array);
    if ($current_page == $url) {
      echo 'active';
    }
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Sangga Buana Outdor</title>

  <!-- Custom fonts for this template-->
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Custom styles for this page -->
  <link href="../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

  <!-- sweetalert -->
  <script src="../vendor/sweetalert2/dist/sweetalert2.all.js"></script>

  <!-- Custom styles for this template-->
  <link href="../css/sb-admin-2.min.css" rel="stylesheet">
  <link href="../css/style.css" rel="stylesheet">
  <style>
    .center-cropped {
      object-fit: cover;
      object-position: center;
      height: 80px;
      width: 80px;
    }

    .center-cropped-detail {
      object-fit: cover;
      object-position: center;
      height: 80px;
      width: 80px; 
    }
  </style>
  

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <span class="sidebar-brand d-flex align-items-center justify-content-center">
        <div class="sidebar-brand-text mx-3">Sangga Buana Outdor</div>
      </span>

      <!-- Divider -->
      <!-- <hr class="sidebar-divider my-0">

      Nav Item - Dashboard
      

      Divider -->
      <hr class="sidebar-divider">

      <?php if ($_SESSION['level'] == 'admin') : ?>
        <div class="sidebar-heading">Data</div>
        <li class="nav-item <?php active('peminjaman_index.php'); active('peminjaman_tambah.php') ?>">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePeminjaman" aria-expanded="true" aria-controls="collapsePeminjaman">
            <i class="fas fa-fw fa-shopping-cart"></i>
            <span>Peminjaman</span>
          </a>
          <div id="collapsePeminjaman" class="collapse" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="./peminjaman_tambah.php">Buat Peminjaman</a>
              <a class="collapse-item" href="./peminjaman_index.php">Daftar Peminjaman</a>
            </div>
          </div>
        </li>
        <li class="nav-item <?php active('pengembalian_index.php') ?>">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePengembalian" aria-expanded="true" aria-controls="collapsePengembalian">
            <i class="fas fa-fw fa-cart-arrow-down"></i>
            <span>Pengembalian</span>
          </a>
          <div id="collapsePengembalian" class="collapse" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="./pengembalian_index.php">Daftar Pengembalian</a>
            </div>
          </div>
        </li>
        <li class="nav-item <?php active('member_index.php');active('member_tambah.php') ?>">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapeMember" aria-expanded="true" aria-controls="collapeMember">
            <i class="fas fa-fw fa-user"></i>
            <span>Member</span>
          </a>
          <div id="collapeMember" class="collapse" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="./member_index.php">Daftar Member</a>
              <a class="collapse-item" href="./member_tambah.php">Tambah Member</a>
            </div>
          </div>
        </li>
        <li class="nav-item <?php active('user_index.php');active('user_tambah.php') ?>">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapeUser" aria-expanded="true" aria-controls="collapeUser">
            <i class="fas fa-fw fa-user"></i>
            <span>User</span>
          </a>
          <div id="collapeUser" class="collapse" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="./user_index.php">Daftar User</a>
              <a class="collapse-item" href="./user_tambah.php">Tambah User</a>
            </div>
          </div>
        </li>
      <?php endif ?>
      <?php if ($_SESSION['level'] == 'keuangan') : ?>
        <div class="sidebar-heading">Data</div>
        <li class="nav-item <?php active('transaksi_index.php') ?>">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsetransaksi" aria-expanded="true" aria-controls="collapsetransaksi">
            <i class="fas fa-fw fa-money-check-alt"></i>
            <span>Transaksi</span>
          </a>
          <div id="collapsetransaksi" class="collapse" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="./transaksi_index.php">Daftar Transaksi</a>
            </div>
          </div>
        </li>
        <li class="nav-item <?php active('denda_index.php') ?>">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsedenda" aria-expanded="true" aria-controls="collapsedenda">
            <i class="fas fa-fw fa-receipt"></i>
            <span>Denda</span>
          </a>
          <div id="collapsedenda" class="collapse" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="./denda_index.php">Daftar Denda</a>
            </div>
          </div>
        </li>
      <?php endif ?>
      <?php if ($_SESSION['level'] == 'gudang') : ?>
        <div class="sidebar-heading">Sewa</div>
        <li class="nav-item <?php active('penyewaan_index.php') ?>">
          <a class="nav-link collapsed" href="./penyewaan_index.php">
            <i class="fas fa-fw fa-shopping-cart"></i>
            <span>Daftar Penyewaan</span>
          </a>
        </li>
        <hr class="sidebar-divider">
        <div class="sidebar-heading">Data</div>
        <li class="nav-item <?php active('supplier_index.php'); active('supplier_tambah.php') ?>">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSuplier" aria-expanded="true" aria-controls="collapseSuplier">
            <i class="fas fa-fw fa-user"></i>
            <span>Suplier</span>
          </a>
          <div id="collapseSuplier" class="collapse" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="./supplier_index.php">Daftar Suplier</a>
              <a class="collapse-item" href="./supplier_tambah.php">Tambah Suplier</a>
            </div>
          </div>
        </li>
        <li class="nav-item <?php active('barang_index.php'); active('barang_tambah.php') ?>">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBarangOutdor" aria-expanded="true" aria-controls="collapseBarangOutdor">
            <i class="fas fa-fw fa-box"></i>
            <span>Barang Outdor</span>
          </a>
          <div id="collapseBarangOutdor" class="collapse" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="./barang_index.php">Daftar Barang</a>
              <a class="collapse-item" href="./barang_tambah.php">Tambah Barang</a>
            </div>
          </div>
        </li>
      <?php endif ?>
      <?php if ($_SESSION['level'] == 'manager') : ?>
        <li class="nav-item <?php active('dashboard_index.php') ?>">
          <a class="nav-link" href="./dashboard_index.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
        </li>
        <hr class="sidebar-divider">
        <div class="sidebar-heading">Data</div>
        <li class="nav-item <?php active('manager_peminjaman.php') ?>">
          <a class="nav-link collapsed" href="./manager_peminjaman.php">
            <i class="fas fa-fw fa-shopping-cart"></i>
            <span>Daftar Peminjaman</span>
          </a>
        </li>
        <li class="nav-item <?php active('manager_pengembalian.php') ?>">
          <a class="nav-link collapsed" href="./manager_pengembalian.php">
            <i class="fas fa-fw fa-cart-arrow-down"></i>
            <span>Daftar Pengembalian</span>
          </a>
        </li>
        <li class="nav-item <?php active('manager_transaksi.php') ?>">
          <a class="nav-link collapsed" href="./manager_transaksi.php">
            <i class="fas fa-fw fa-money-check-alt"></i>
            <span>Daftar Transaksi</span>
          </a>
        </li>
        <li class="nav-item <?php active('manager_barang.php') ?>">
          <a class="nav-link collapsed" href="./manager_barang.php">
            <i class="fas fa-fw fa-box"></i>
            <span>Daftar Barang</span>
          </a>
        </li>
        <li class="nav-item <?php active('manager_denda.php') ?>">
          <a class="nav-link collapsed" href="./manager_denda.php">
            <i class="fas fa-fw fa-receipt"></i>
            <span>Daftar Denda</span>
          </a>
        </li>
        <li class="nav-item <?php active('manager_supplier.php') ?>">
          <a class="nav-link collapsed" href="./manager_supplier.php">
            <i class="fas fa-fw fa-user"></i>
            <span>Daftar Supplier</span>
          </a>
        </li>
        <li class="nav-item <?php active('manager_member.php') ?>">
          <a class="nav-link collapsed" href="./manager_member.php">
            <i class="fas fa-fw fa-user"></i>
            <span>Daftar Member</span>
          </a>
        </li>
        <li class="nav-item <?php active('manager_user.php') ?>">
          <a class="nav-link collapsed" href="./manager_user.php">
            <i class="fas fa-fw fa-user"></i>
            <span>Daftar User</span>
          </a>
        </li>
      <?php endif ?>
      

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-3 d-none d-lg-inline text-gray-600 small"><?= $_SESSION['nama_user'] ?></span>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">