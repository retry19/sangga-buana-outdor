  <!-- awal footer -->
  <div class="jumbotron footer mt-4 mb-0">
    <div class="container text-center">
      <p>2019 &copy; Sangga Buana Outdor</p>
    </div>
  </div>
  <!-- akhir footer -->

  <script src="js/jquery.slim.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>

  <script src="js/jquery191.js"></script>
  <script src="js/jquery-ui-1.9.2.min.js"></script>
  <script>
    $(function() {
      $("#input_tgl_sewa").datepicker({
        dateFormat: 'dd-mm-yy',
        minDate: 0,
        maxDate: '+1Y+6M',
        onSelect: function (dateStr) {
            var min = $(this).datepicker('getDate'); // Get selected date
            $("#input_tgl_kembali").datepicker('option', 'minDate', min || '0'); // Set other min, default to today
          }
      });

      $("#input_tgl_kembali").datepicker({
        dateFormat: 'dd-mm-yy',
        minDate: '0',
        maxDate: '+1Y+6M',
        onSelect: function (dateStr) {
          var max = $(this).datepicker('getDate'); // Get selected date
          $('#datepicker').datepicker('option', 'maxDate', max || '+1Y+6M'); // Set other max, default to +18 months
          var start = $("#input_tgl_sewa").datepicker("getDate");
          var end = $("#input_tgl_kembali").datepicker("getDate");
          var days = (end - start) / (1000 * 60 * 60 * 24);
          $("#output_lama_sewa").val(days+' hari');
          $("#lama_sewa").val(days);
        }
			});
    });
  </script>
</body>
</html>