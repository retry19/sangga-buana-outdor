<?php require('layouts/header.php') ?>

  <div class="container" style="min-height: 500px;">
    <div class="row">
      <div class="col-12 text-center">
        <h2 class="mt-4 mb-4">Keranjang Sewa</h2>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col">
      <?php 
        if (isset($_SESSION['notif'])) {
          if ($_SESSION['notif'] == 'qty-max'){
            echo '
              <div class="alert alert-danger alert-dismissible fade show text-left" role="alert">
                <strong>Stok barang tidak tersedia!</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>';
          }
          elseif ($_SESSION['notif'] == 'barang-hapus'){
            echo '
              <div class="alert alert-warning alert-dismissible fade show text-left" role="alert">
                <strong>Barang berhasil dihapus!</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>';
          }
          elseif ($_SESSION['notif'] == 'tagihan-error'){
            echo '
              <div class="alert alert-danger alert-dismissible fade show text-left" role="alert">
                <strong>Checkout gagal!</strong> harap selesaikan terlebih dahulu tagihan anda sebelumnya!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>';
          }
          elseif ($_SESSION['notif'] == 'tunai-not-null'){
            echo '
              <div class="alert alert-danger alert-dismissible fade show text-left" role="alert">
                <strong>Checkout gagal!</strong> harap selesaikan terlebih dahulu proses Pembayaran anda sebelumnya!
                silahkan lakukan pembayaran ditempat.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>';
          }
          elseif ($_SESSION['notif'] == 'transfer-not-null'){
            echo '
              <div class="alert alert-danger alert-dismissible fade show text-left" role="alert">
                <strong>Checkout gagal!</strong> harap selesaikan terlebih dahulu proses Transfer tagihan anda sebelumnya!
                <a href="./pembayaran_index.php">klik disini</a> untuk konfirmasi bukti transfer anda
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>';
          }
          unset($_SESSION['notif']);
        }
      ?>
      </div>
    </div>
    <!-- awal table keranjang -->
    <div class="row">
      <div class="col-12">
        <table class="table table-bordered" cellpadding="10" width="100%">
          <tr align="center">
            <th>Produk</th>
            <th>Jumlah</th>
            <th>Harga/Unit</th>
            <th>Harga</th>
            <th>Batal</th>
          </tr>
          <?php if (isset($_SESSION['keranjang_belanja'])) : ?>
            <?php
              $total_harga = 0; 
              foreach ($_SESSION['keranjang_belanja'] as $kb) : 
            ?>
              <?php 
                require('config/connection.php');
                $id = $kb['id_barang'];
                $query = mysqli_query($conn, "SELECT * FROM barang WHERE id_barang='$id'");

                $rows = [];
                while ($row = mysqli_fetch_assoc($query)){
                  $rows[] = $row;
                }
              ?>
              <tr align="center">
                <td class="align-middle"><img src="images/<?= $rows[0]['foto_barang'] ?>" alt="<?= $rows[0]['nama_barang'] ?>" width="100px"></td>
                <td class="align-middle" width="15%">
                  <input type="text" name="jmlh_barang" class="border-0 w-25 text-center" value="<?= $kb['qty'] ?>" readonly><br>
                  <a href="./keranjang_qty_kurang.php?id=<?= $kb['id_barang'] ?>" class="btn btn-sm btn-outline-primary btn-inc"><i class="fas fa-minus"></i></a>
                  <a href="./keranjang_qty_tambah.php?id=<?= $kb['id_barang'] ?>&max=<?= $rows[0]['stok_awal']-$rows[0]['jmlh_rusak']-$rows[0]['jmlh_diambil'] ?>" class="btn btn-sm btn-primary btn-inc"><i class="fas fa-plus"></i></a>
                </td>
                <td class="align-middle">Rp. <?= $rows[0]['harga_sewa'] ?></td>
                <td class="align-middle">Rp. <?= $rows[0]['harga_sewa']*$kb['qty'] ?></td>
                <td class="align-middle"><button class="btn btn-danger" data-toggle="modal" data-target="#hapusBarangModal<?= $kb['id_barang'] ?>"><i class="fas fa-trash-alt"></i></button></td>
              </tr>
              <?php $total_harga += $rows[0]['harga_sewa']*$kb['qty']; ?>

              <!-- awal modal -->
              <div class="modal fade" id="hapusBarangModal<?= $kb['id_barang'] ?>" tabindex="-1" role="dialog" aria-labelledby="hapusBarangModalTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="hapusBarangModalTitle">Hapus barang?</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <p>Apakah anda yakin akan menghapus <?= $rows[0]['nama_barang'] ?></p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-white" data-dismiss="modal">Batal</button>
                      <a href="./keranjang_hapus.php?id=<?= $rows[0]['id_barang'] ?>" class="btn btn-primary">Hapus</a>
                    </div>
                  </div>
                </div>
              </div>
              <!-- akhir modal -->
              
            <?php endforeach; ?>
            <tr>
              <th colspan="6" class="align-middle">
                <div class="row">
                  <div class="col-6">Total</div>
                  <div class="col-6 text-right">Rp. <?= $total_harga ?></div>
                </div>
              </th>
            </tr>
          <?php else : ?>
            <tr>
              <td colspan="6" class="align-middle text-center">Tidak ada barang</td>
            </tr>
          <?php endif; ?>
        </table>
      </div>
    </div>
    <!-- akhir table keranjang -->
    <!-- awal utilities -->
    <div class="row mt-3">
      <div class="col-6" style="margin-bottom:100px;">
        <a href="./" class="btn btn-outline-primary"><i class="fas fa-plus"></i>&nbsp; Tambah Sewa</a>
      </div>
      <div class="col-6 text-right">
        <form action="keranjang_checkout.php" method="post">
          <input type="hidden" name="total_harga" value="<?= $total_harga ?>">
          <?php if (isset($_SESSION['keranjang_belanja'])) : ?>
            <button class="btn btn-primary"><i class="fas fa-check"></i>&nbsp; Checkout</button>
          <?php endif ?>
        </form>
      </div>
    </div>
    <!-- akhir utilities -->
  </div>

<?php require('layouts/footer.php') ?>