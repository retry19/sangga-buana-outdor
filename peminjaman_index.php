<?php
  require('layouts/header.php');
  require('config/connection.php');

  $id_member = $_SESSION['id_member'];
  
  $select_peminjaman = mysqli_query($conn, "SELECT p.id_peminjaman, p.id_member, m.nama, p.tgl_sewa, p.lama_sewa, p.total_harga, p.status_barang, p.status_kembali, t.status_pembayaran, t.id_user, t.total_pembayaran 
                                            FROM peminjaman p, transaksi t, member m
                                            WHERE p.id_member='$id_member' AND (p.id_member=m.id_member AND p.id_peminjaman=t.id_peminjaman)");
  $peminjamans = [];
  while ($peminjaman = mysqli_fetch_array($select_peminjaman)) {
    $peminjamans[] = $peminjaman;
  }
?>

<div class="container">
  <div class="row">
    <div class="col-12 text-center">
      <h2 class="my-4">Daftar Peminjaman</h2>
      <?php 
        if (isset($_SESSION['notif'])) {
          switch ($_SESSION['notif']) {
            case 'submit-tunai':
              echo '
              <div class="alert alert-warning alert-dismissible fade show text-left" role="alert">
                <strong>Silahkan segera lakukan pembayaran di tempat!</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>'; break;
            case 'submit-tf':
              echo '
              <div class="alert alert-warning alert-dismissible fade show text-left" role="alert">
                <strong>Silahkan segera ambil barang di tempat!</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>'; break;
          }
          unset($_SESSION['notif']);
        }
      ?>
    </div>
  </div>

  <!-- tabel peminjaman -->
  <div class="row mb-5">
    <div class="col-12 mb-4">
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr class="text-center">
              <th>Id Peminjaman</th>
              <th>Tanggal Sewa</th>
              <th>Lama Sewa (hari)</th>
              <th>Total</th>
              <th>Status</th>
              <th>Validate by</th>
              <th>Opsi</th>
              <th>Review</th>
            </tr>
          </thead>
          <tbody id="data-peminjaman">
            <?php if (mysqli_num_rows($select_peminjaman)) : ?>
              <?php $no = 0; foreach ($peminjamans as $p) : ?>
                <?php $no++ ?>
              <tr class="text-center">
                <td class="align-middle"><?= $p['id_peminjaman'] ?></td>
                <td class="align-middle"><?= $p['tgl_sewa'] ?></td>
                <td class="align-middle"><?= $p['lama_sewa'] ?></td>
                <td class="align-middle"><?= $p['total_harga'] ?></td>
                <td class="align-middle">
                  <?php 
                    if ($p['status_pembayaran'] == 0 && $p['total_pembayaran'] != null) : 
                      echo "Menunggu validasi";
                    elseif ($p['status_pembayaran'] == 0 && $p['total_pembayaran'] == null):
                      echo "Belum bayar";
                    elseif ($p['status_pembayaran'] == 1 && $p['status_barang'] == 0):
                      echo "Belum diambil";
                    elseif ($p['status_pembayaran'] == 1 && $p['status_barang'] == 1 && $p['status_kembali'] == 1):
                      echo "Barang sudah dikembalikan";
                    elseif ($p['status_pembayaran'] == 1 && $p['status_barang'] == 1):
                      echo "Sudah diambil";
                    endif
                  ?>
                </td>
                <?php 
                  $id_user = $p['id_user'];
                  $select_user = mysqli_query($conn, "SELECT nama_user FROM user WHERE id_user='$id_user'");
                  $user = mysqli_fetch_array($select_user);
                  
                  $idpnjmn = $p['id_peminjaman'];
                  $review = mysqli_query($conn, "SELECT r.rate FROM review r 
                                                  INNER JOIN detail_peminjaman d ON r.detail_peminjaman_id=d.id WHERE d.id_peminjaman='$idpnjmn'");
                ?>
                <td class="align-middle"><?= $user['nama_user'] ?></td>
                <td class="align-middle">
                  <a href="javascript:void(0)" class="btn btn-sm btn-outline-primary" onclick="window.open('./admin/report/invoice.php?id=<?= $p[0] ?>', '_blank')">Cetak Invoice</a>
                </td>
                <td>
                  <?php if ($p['status_kembali'] == 1) : ?>
                    <?php if (mysqli_num_rows($review) > 0) : ?>
                      <button class="btn btn-sm btn-warning button-hasil-review" data-id="<?= $p['id_peminjaman'] ?>">Lihat hasil Review</button>
                    <?php else : ?>
                      <button class="btn btn-sm btn-primary button-review" data-id="<?= $p['id_peminjaman'] ?>">Review</button>
                    <?php endif ?>
                  <?php else : ?>
                    -
                  <?php endif ?>
                </td>
              </tr>
              <?php endforeach ?>
            <?php else : ?>
              <tr>
                <td colspan="7" class="text-center">Tidak ada peminjaman!</td>
              </tr>
            <?php endif ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- Modal Rating -->
<div class="modal fade" id="modalRating" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Review Peminjaman</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="./review_submit.php" method="POST">
        <div class="modal-body" id="body-rating">
        </div>
        <div class="modal-footer">
          <button class="btn btn-outline-secondary" type="button" data-dismiss="modal">Batal</button>&nbsp; 
          <button class="btn btn-primary" id="btn-rating" type="submit">Kirim</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Hasil Rating -->
<div class="modal fade" id="modalHasilRating" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Hasil Review</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="body-hasil-rating">
      </div>
    </div>
  </div>
</div>


<?php require('layouts/footer.php') ?>